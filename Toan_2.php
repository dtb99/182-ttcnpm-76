<!DOCTYPE html>
<?php
include 'UserID.php';
$user = new UserID();
if(!$user->isLogin()) header('Location:login.php');
?>
<html>
	<head>
		<meta charset="UTF-8" />
		<title>Thi thử-Toán 2</title>
		<link rel="stylesheet" type="text/css" href="examstyle.css" />
		
		
	</head>
	<body>
		<h1>Đề thi thử môn toán- Đề số 2</h1>
		<p id="time">Thời gian: 90p</p>
		<script src='examform.js'></script>
		<div id="content">
			<div id="noi_dung_de" style='display:none;min-height:3000px'>
				Tính nguyên hàm <img src="https://latex.codecogs.com/gif.latex?F(x)=\int&space;\frac{1}{e^x&plus;1}dx" title="F(x)=\int \frac{1}{e^x+1}dx" />.
					@<img src="https://latex.codecogs.com/gif.latex?F(x)=1-\ln(1&plus;e^x)&plus;c(c\in&space;\mathbb{R})" title="F(x)=1-\ln(1+e^x)+c(c\in \mathbb{R})" />
					@<img src="https://latex.codecogs.com/gif.latex?F(x)=\ln(1&plus;e^x)-x&plus;c(c\in&space;\mathbb{R})" title="F(x)=\ln(1+e^x)-x+c(c\in \mathbb{R})" />
					@~<img src="https://latex.codecogs.com/gif.latex?F(x)=x-\ln(1&plus;e^x)&plus;c(c\in&space;\mathbb{R})" title="F(x)=x-\ln(1+e^x)+c(c\in \mathbb{R})" />
					@<img src="https://latex.codecogs.com/gif.latex?F(x)=x-\ln(1&plus;e^x)-1&plus;c(c\in&space;\mathbb{R})" title="F(x)=x-\ln(1+e^x)-1+c(c\in \mathbb{R})" />
				Xác định số mặt phẳng đối xứng của tứ diện đều
					@4
					@~9
					@5
					@7`
				Cho tứ diên <i>ABCD</i> có <img src="https://latex.codecogs.com/gif.latex?AB=AC=AD=2a" title="AB=AC=AD=2a" />. Biết tam giác <i>BCD</i> có <img src="https://latex.codecogs.com/gif.latex?BC=2a,BD=a,\widehat{CBD}=120^{\circ}" title="BC=2a,BD=a,\widehat{CBD}=120^{\circ}" />. Tính thể tích tứ diện <i>ABCD</i> theo <i>a</i>.
					@<img src="https://latex.codecogs.com/gif.latex?\frac{\sqrt{5}}{3}a^3" title="\frac{\sqrt{5}}{3}a^3" />
					@<img src="https://latex.codecogs.com/gif.latex?\frac{\sqrt{5}}{2}a^3" title="\frac{\sqrt{5}}{2}a^3" />
					@<img src="https://latex.codecogs.com/gif.latex?\sqrt{5}a^3" title="\sqrt{5}a^3" />
					@~<img src="https://latex.codecogs.com/gif.latex?\frac{\sqrt{5}}{6}a^3" title="\frac{\sqrt{5}}{6}a^3" />
				Cho hình phẳng (<i>H</i>) được giới hạn bởi elip cps phương trình <img src="https://latex.codecogs.com/gif.latex?\frac{x^2}{25}&plus;\frac{y^2}{16}=1" title="\frac{x^2}{25}+\frac{y^2}{16}=1" />. Tính thể tích của khối tròn xoay thu được khi quay hình phẳng trên quanh trục <i>Ox</i>.
					@<img src="https://latex.codecogs.com/gif.latex?\frac{160\pi}{3}" title="\frac{160\pi}{3}" />
					@~<img src="https://latex.codecogs.com/gif.latex?\frac{320\pi}{3}" title="\frac{320\pi}{3}" />
					@<img src="https://latex.codecogs.com/gif.latex?\frac{160}{3}" title="\frac{160}{3}" />
					@<img src="https://latex.codecogs.com/gif.latex?\frac{320}{3}" title="\frac{320}{3}" />
				Tìm số phát biểu đúng trong các phát biểu sau:<br>(1) Đồ thị hàm số <img src="https://latex.codecogs.com/gif.latex?y=x^a(a<0)" title="y=x^a(a<0)" /> nhận trục <i>Ox</i> làm tiệm cận ngang và nhận <i>Oy</i> làm tiệm cận đứng.<br>(2) Đồ thị hàm số <img src="https://latex.codecogs.com/gif.latex?y=x^a(a>0)" title="y=x^a(a>0)" /> không có tiệm cận.<br>(3) Đồ thị hàm số <a href="https://www.codecogs.com/eqnedit.php?latex=y=\log_{a}x(0<a\neq1)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?y=\log_{a}x(0<a\neq1)" title="y=\log_{a}x(0<a\neq1)" /></a> nhận trục <i>Oy</i> làm tiệm cận đứng và không có tiệm cận ngang.<br>(4) Đồ thị hàm số <a href="https://www.codecogs.com/eqnedit.php?latex=y=a^x(0<a\neq1)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?y=a^x(0<a\neq1)" title="y=a^x(0<a\neq1)" /></a> nhận trục <i>Ox</i> 					làm tiệm cận ngang và không có tiệm cận đứng.
					@2
					@1
					@~4
					@3`
				Trong không gian toạ độ <i>Oxyz</i>, cho đường thẳng &Delta; có phương trình <img src="https://latex.codecogs.com/gif.latex?\frac{x-1}{2}=\frac{y&plus;1}{-1}=\frac{z}{2}" /> và mặt phẳng (&alpha;) có phương trình <img src="https://latex.codecogs.com/gif.latex?x&plus;y-2=0" />. Tính côsin của góc tạo bởi đường thẳng &Delta; và mặt phẳng (&alpha;).
					@<img src="https://latex.codecogs.com/gif.latex?\frac{\sqrt{3}}{9}" />
					@<img src="https://latex.codecogs.com/gif.latex?-\frac{\sqrt{3}}{9}" />
					@~<img src="https://latex.codecogs.com/gif.latex?\frac{\sqrt{78}}{9}" />
					@<img src="https://latex.codecogs.com/gif.latex?-\frac{\sqrt{78}}{9}" />`
				Cho hàm số <img src="https://latex.codecogs.com/gif.latex?y=ax^3&plus;bx^2&plus;cx&plus;d(a\neq0)" /> có đồ thị (C). Tìm phát biểu sai
					@Đồ thị (C) có tâm đối xứng là điểm I(<i>x<sub>0</sub></i>;<i>f(x<sub>0</sub>)</i>) với <i>f"(x<sub>0</sub>)</i> = 0.
					@Số điểm cực trị của (C) là số chẵn.
					@Đồ thị (C) luôn cắt trục hoành.
					@~Đồ thị (C) luôn có hai điểm cực trị.`
				Gọi S là tập hợp các ước số nguyên dương của 121500. Chọn ngẫu nhiên một số từ S. Tính xác xuất để số được chọn chia hết cho 5.
					@1/2
					@1/3
					@5/6
					@~1/4`
				Tìm phần ảo của số phức <i>z</i> thoả mãn <img src="https://latex.codecogs.com/gif.latex?\frac{(1-3i)\bar{z}}{\left|z\right|^2}-5i=\frac{2&plus;i}{z}" />
					@<img src="https://latex.codecogs.com/gif.latex?-\frac{4}{5}" />
					@<img src="https://latex.codecogs.com/gif.latex?-\frac{4}{5}i" />
					@~<img src="https://latex.codecogs.com/gif.latex?\frac{1}{5}" />
					@<img src="https://latex.codecogs.com/gif.latex?\frac{1}{5}i" />`
				Trong không gian toạ độ <i>Oxyz</i>, cho hình hộp <i>ABCD.A'B'C'D'</i> với các điểm A(-1;1;2), B(-3;2;1), D(0;-1;2) và A'(2;1;2). Tìm toạ độ đỉnh C'.
					@~C'(1;0;1)
					@C'(-3,1,3)
					@C'(0;1;0)
					@C'(-1;3;1)`
				
				Cho hình chóp <i>S.ABCD</i> có đáy là hình bình hành và có thể tích bằng V. Điểm P là trung điểm của SC, một mặt phẳng qua AP cắt hai cạnh SB và SD lần lượt tại M và N. Gọi V<sub>1</sub> là thể tích của khối chóp <i>S.AMPN</i>. Giá trị nhỏ nhất của tỉ số V<sub>1</sub>/V bằng:
					@~1/3
					@1/8
					@2/3
					@3/8`
				Xét phương trình bậc hai <a href="https://www.codecogs.com/eqnedit.php?latex=az^2&plus;bz&plus;c=0" target="_blank"><img src="https://latex.codecogs.com/gif.latex?az^2&plus;bz&plus;c=0" title="az^2+bz+c=0" /></a> trên tập <a href="https://www.codecogs.com/eqnedit.php?latex=\mathbb{C}(a\neq0,a,b,c\in&space;\mathbb{R})" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\mathbb{C}(a\neq0,a,b,c\in&space;\mathbb{R})" title="\mathbb{C}(a\neq0,a,b,c\in \mathbb{R})" /></a>. Tìm điều kiện cần và đủ để phương trình có hai nghiệm <a href="https://www.codecogs.com/eqnedit.php?latex=z_1,z_2" target="_blank"><img src="https://latex.codecogs.com/gif.latex?z_1,z_2" title="z_1,z_2" /></a> là hai số phức liên hợp.
					@<a href="https://www.codecogs.com/eqnedit.php?latex=b^2-4ac\geq&space;0" target="_blank"><img src="https://latex.codecogs.com/gif.latex?b^2-4ac\geq&space;0" title="b^2-4ac\geq 0" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=b^2-4ac>&space;0" target="_blank"><img src="https://latex.codecogs.com/gif.latex?b^2-4ac>&space;0" title="b^2-4ac> 0" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=b^2-4ac<&space;0" target="_blank"><img src="https://latex.codecogs.com/gif.latex?b^2-4ac<&space;0" title="b^2-4ac< 0" /></a>
					@~<a href="https://www.codecogs.com/eqnedit.php?latex=b^2-4ac\leq&space;0" target="_blank"><img src="https://latex.codecogs.com/gif.latex?b^2-4ac\leq&space;0" title="b^2-4ac\leq 0" /></a>`
				Tìm tổng các giá trị nguyên của tham số <i>m</i> để hàm số <a href="https://www.codecogs.com/eqnedit.php?latex=y=mx^4&plus;(m^2-25)x^2&plus;2" target="_blank"><img src="https://latex.codecogs.com/gif.latex?y=mx^4&plus;(m^2-25)x^2&plus;2" title="y=mx^4+(m^2-25)x^2+2" /></a> có một điểm cực đại và hai điểm cực tiểu.
					@~10
					@-10
					@0
					@15`
				Cho số phức <a href="https://www.codecogs.com/eqnedit.php?latex=z=1&plus;\sqrt{3}i" target="_blank"><img src="https://latex.codecogs.com/gif.latex?z=1&plus;\sqrt{3}i" title="z=1+\sqrt{3}i" /></a>. Gọi A, B lần lượt là điểm biểu diễn của các số phức <a href="https://www.codecogs.com/eqnedit.php?latex=(1&plus;i)z" target="_blank"><img src="https://latex.codecogs.com/gif.latex?(1&plus;i)z" title="(1+i)z" /></a> và <a href="https://www.codecogs.com/eqnedit.php?latex=(3-i)z" target="_blank"><img src="https://latex.codecogs.com/gif.latex?(3-i)z" title="(3-i)z" /></a> trong mặt phẳng toạ độ. Tính độ dài AB
					@8
					@~<a href="https://www.codecogs.com/eqnedit.php?latex=4\sqrt{2}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?4\sqrt{2}" title="4\sqrt{2}" /></a>
					@4
					@<a href="https://www.codecogs.com/eqnedit.php?latex=2\sqrt{2}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?2\sqrt{2}" title="2\sqrt{2}" /></a>`
				Tính đạo hàm của hàm số <a href="https://www.codecogs.com/eqnedit.php?latex=f(x)=\ln\left|x\right|" target="_blank"><img src="https://latex.codecogs.com/gif.latex?f(x)=\ln\left|x\right|" title="f(x)=\ln\left|x\right|" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=-\frac{1}{\left|x\right|}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?-\frac{1}{\left|x\right|}" title="-\frac{1}{\left|x\right|}" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=\frac{1}{\left|x\right|}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{1}{\left|x\right|}" title="\frac{1}{\left|x\right|}" /></a>
					@~<a href="https://www.codecogs.com/eqnedit.php?latex=\frac{1}{x}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{1}{x}" title="\frac{1}{x}" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=-\frac{1}{x}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?-\frac{1}{x}" title="-\frac{1}{x}" /></a>`
				Tìm tập nghiệm của bất phương trình <a href="https://www.codecogs.com/eqnedit.php?latex=\log_3(2x-1)>\log_9x^2" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\log_3(2x-1)>\log_9x^2" title="\log_3(2x-1)>\log_9x^2" /></a>
					@~<a href="https://www.codecogs.com/eqnedit.php?latex=(1;&plus;\infty)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?(1;&plus;\infty)" title="(1;+\infty)" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=(0;&plus;\infty)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?(0;&plus;\infty)" title="(0;+\infty)" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=(0;1)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?(0;1)" title="(0;1)" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=\left(\frac{1}{4};&plus;\infty\right)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\left(\frac{1}{4};&plus;\infty\right)" title="\left(\frac{1}{4};+\infty\right)" /></a>`
				Cho hai số phức <a href="https://www.codecogs.com/eqnedit.php?latex=z_1;z_2" target="_blank"><img src="https://latex.codecogs.com/gif.latex?z_1;z_2" title="z_1;z_2" /></a> thoả mãn <a href="https://www.codecogs.com/eqnedit.php?latex=\left|z_1-2-i\right|=2\sqrt{2}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\left|z_1-2-i\right|=2\sqrt{2}" title="\left|z_1-2-i\right|=2\sqrt{2}" /></a> và <a href="https://www.codecogs.com/eqnedit.php?latex=\left|z_2-5&plus;i\right|=\left|\bar{z_2}-7&plus;i\right|" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\left|z_2-5&plus;i\right|=\left|\bar{z_2}-7&plus;i\right|" title="\left|z_2-5+i\right|=\left|\bar{z_2}-7+i\right|" /></a>. Tìm giá trị nhỏ nhất của <a href="https://www.codecogs.com/eqnedit.php?latex=\left|z_1-iz_2\right|" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\left|z_1-iz_2\right|" title="\left|z_1-iz_2\right|" /></a>.
					@<a href="https://www.codecogs.com/eqnedit.php?latex=\frac{11\sqrt{2}}{2}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{11\sqrt{2}}{2}" title="\frac{11\sqrt{2}}{2}" /></a>
					@~<a href="https://www.codecogs.com/eqnedit.php?latex=\frac{3\sqrt{2}}{2}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{3\sqrt{2}}{2}" title="\frac{3\sqrt{2}}{2}" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=2\sqrt{2}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?2\sqrt{2}" title="2\sqrt{2}" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=\frac{7\sqrt{2}}{2}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{7\sqrt{2}}{2}" title="\frac{7\sqrt{2}}{2}" /></a>`
				cho hàm số <a href="https://www.codecogs.com/eqnedit.php?latex=y=\frac{\sqrt{x-1}}{x^2-x-6}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?y=\frac{\sqrt{x-1}}{x^2-x-6}" title="y=\frac{\sqrt{x-1}}{x^2-x-6}" /></a>. Chọn phát biểu đúng
					@~Đồ thị hàm số có đúng một đường tiệm cận đứng và một đường tiệm cận ngang.
					@Đồ thị hàm số có đúng hai đường tiệm cận đứng và hai đường tiệm cận ngang.
					@Đồ thị hàm số có đúng một đường tiệm cận đứng và hai đường tiệm cận ngang.
					@Đồ thị hàm số có đúng hai đường tiệm cận đứng và một đường tiệm cận ngang.`
				Cho hình hộp <i>ABCD.A'B'C'D'</i> Tính tỉ số thể tích của khối tứ diện <i>BDA'C'</i> và khối hộp <i>ABCD.A'B'C'D'</i>.
					@1/5
					@2/3
					@~1/3
					@2/5`
				Gọi n là các số phức <i>z</i> đồng thời thoả mãn <a href="https://www.codecogs.com/eqnedit.php?latex=|iz&plus;1&plus;2i|=3" target="_blank"><img src="https://latex.codecogs.com/gif.latex?|iz&plus;1&plus;2i|=3" title="|iz+1+2i|=3" /></a> và biểu thức <a href="https://www.codecogs.com/eqnedit.php?latex=T=2|z&plus;5&plus;2i|&plus;3|z-3i|" target="_blank"><img src="https://latex.codecogs.com/gif.latex?T=2|z&plus;5&plus;2i|&plus;3|z-3i|" title="T=2|z+5+2i|+3|z-3i|" /></a> đạt giá trị lớn nhất. Gọi M là giá trị lớn nhất của <i>T</i>. Tính tích Mn.
					@<a href="https://www.codecogs.com/eqnedit.php?latex=2\sqrt{3}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?2\sqrt{3}" title="2\sqrt{3}" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=6\sqrt{13}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?6\sqrt{13}" title="6\sqrt{13}" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=5\sqrt{13}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?5\sqrt{13}" title="5\sqrt{13}" /></a>
					@~<a href="https://www.codecogs.com/eqnedit.php?latex=10\sqrt{21}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?10\sqrt{21}" title="10\sqrt{21}" /></a>`
				Cho hàm số y = f(x) thoả mãn <a href="https://www.codecogs.com/eqnedit.php?latex=\int_{0}^{1}f(x)dx=2" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\int_{0}^{1}f(x)dx=2" title="\int_{0}^{1}f(x)dx=2" /></a> và <a href="https://www.codecogs.com/eqnedit.php?latex=\int_{1}^{5}f(x)dx=-8" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\int_{1}^{5}f(x)dx=-8" title="\int_{1}^{5}f(x)dx=-8" /></a>. Tính tích phân <a href="https://www.codecogs.com/eqnedit.php?latex=I=\int_{-1}^{2}f(|2x-3|)dx" target="_blank"><img src="https://latex.codecogs.com/gif.latex?I=\int_{-1}^{2}f(|2x-3|)dx" title="I=\int_{-1}^{2}f(|2x-3|)dx" /></a>.
					@-8
					@~-2
					@-4
					@-6`
				Cho các số thực <i>a,b,c</i> thoả mãn <a href="https://www.codecogs.com/eqnedit.php?latex=\left&space;(&space;\frac{5}{4}&space;\right&space;)^a<\left&space;(&space;\frac{4}{5}&space;\right&space;)^a,\log_b\frac{5}{4}>\log_b\frac{4}{5},c^\frac{5}{4}<c^\frac{4}{5}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\left&space;(&space;\frac{5}{4}&space;\right&space;)^a<\left&space;(&space;\frac{4}{5}&space;\right&space;)^a,\log_b\frac{5}{4}>\log_b\frac{4}{5},c^\frac{5}{4}<c^\frac{4}{5}" title="\left ( \frac{5}{4} \right )^a<\left ( \frac{4}{5} \right )^a,\log_b\frac{5}{4}>\log_b\frac{4}{5},c^\frac{5}{4}<c^\frac{4}{5}" /></a>. Tìm phát biểu đúng.
					@<a href="https://www.codecogs.com/eqnedit.php?latex=b<0<c<1<a" target="_blank"><img src="https://latex.codecogs.com/gif.latex?b<0<c<1<a" title="b<0<c<1<a" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=a<0<b<1<c" target="_blank"><img src="https://latex.codecogs.com/gif.latex?a<0<b<1<c" title="a<0<b<1<c" /></a>
					@~<a href="https://www.codecogs.com/eqnedit.php?latex=a<0<c<1<b" target="_blank"><img src="https://latex.codecogs.com/gif.latex?a<0<c<1<b" title="a<0<c<1<b" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=c<0<b<1<a" target="_blank"><img src="https://latex.codecogs.com/gif.latex?c<0<b<1<a" title="c<0<b<1<a" /></a>`
				Cho hàm số <i>y = f(x)</i> xác định và liên tục trên tập số thực, có đồ thị hàm <i>y = f'(x)</i> như hình vẽ.<br><img src="https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.15752-9/62516770_2327637807330758_1578007916974178304_n.png?_nc_cat=111&_nc_oc=AQkC9VjuQvPYbB22Vd9phdK9b9vot5EtvuygsMJWl3-xSRfv0MZTRYgxZP0LpW4PRMI&_nc_ht=scontent.fsgn2-1.fna&oh=a66223d74f77c963d0d1936c1b887e0a&oe=5D95C539" /><br> Hàm số <a href="https://www.codecogs.com/eqnedit.php?latex=g(x)=f(x)-\frac{1}{2}x^2&plus;x-8" target="_blank"><img src="https://latex.codecogs.com/gif.latex?g(x)=f(x)-\frac{1}{2}x^2&plus;x-8" title="g(x)=f(x)-\frac{1}{2}x^2+x-8" /></a> có bao nhiêu cực tiểu?
					@3
					@~2
					@1
					@4`
				Cho hình nón có thể tích bằng 12&pi; và diện tích xung quanh bằng 15&pi;. Tính bán kính đáy của hình nón biết bán kính là số nguyên dương.
					@4
					@~3
					@6
					@5`
				Cho hàm số <a href="https://www.codecogs.com/eqnedit.php?latex=y=x^4-2mx^2&plus;\frac{7}{2}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?y=x^4-2mx^2&plus;\frac{7}{2}" title="y=x^4-2mx^2+\frac{7}{2}" /></a> có đồ thị (C). Biết (C) có ba điểm cực trị lập thành một tam giác nhận gốc toạ độ O(0;0) làm trực tâm. Khẳng định nào là đúng?
					@<a href="https://www.codecogs.com/eqnedit.php?latex=m\in(2;4]" target="_blank"><img src="https://latex.codecogs.com/gif.latex?m\in(2;4]" title="m\in(2;4]" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=m\in(6;8]" target="_blank"><img src="https://latex.codecogs.com/gif.latex?m\in(6;8]" title="m\in(6;8]" /></a>
					@~<a href="https://www.codecogs.com/eqnedit.php?latex=m\in(0;2]" target="_blank"><img src="https://latex.codecogs.com/gif.latex?m\in(0;2]" title="m\in(0;2]" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=m\in(4;6]" target="_blank"><img src="https://latex.codecogs.com/gif.latex?m\in(4;6]" title="m\in(4;6]" /></a>`
				Cho hàm số <i>y = f(x)</i> có đồ thị như hình vẽ.<br><img src="https://scontent.fsgn2-2.fna.fbcdn.net/v/t1.15752-9/62360668_382065429092170_8911160135280230400_n.png?_nc_cat=100&_nc_oc=AQkklDtxVfi7HUssjEBcG6XsujRKROafoLrJW8qZLuVtlDojWwu-NdvGzVESysLUCmw&_nc_ht=scontent.fsgn2-2.fna&oh=7060170cbb0931bc13e481c21ebf1928&oe=5D9EF95D" /><br>Có bao nhiêu giá trị nguyên của tham số <i>m</i> để phương trình <i>f</i>(3sin<i>x</i>+4cos<i>x</i>) = <i>f</i>(<i>m</i>) có nghiệm?
					@10
					@~14
					@9
					@11`
				Một người được trả lương qua tài khoản ATM của Vietcombank. Người đó dùng 35 triệu tiền mặt để mở thêm tài khoản tiết kiệm tự động, kỳ hạn 1 tháng với hình thức cứ sau mỗi tháng thì ngân hàng tự động chuyển từ tài khoản ATM qua tài khoản tiết kiệm 3 triệu đồng. Hỏi sau 5 năm, người đó rút bao nhiêu tiền trong tài khoản tiết kiệm, biết rằng trong suốt 5 năm, người đó không rút tiền, lãi suất không đổi là 5%/năm và nếu đến kỳ hạn mà người đó rút hết tài khoản tiết kiệm thì ngân hàng sẽ không chuyển tiền từ tài khoản ATM sang tài khoản tiết kiệm nữa.
					@~248,9358023 triệu đồng
					@245,1017017 triệu đồng
					@249,7858783 triệu đồng
					@245,9358023 triệu đồng`
				Trong không gian toạ độ <i>Oxyz</i> cho hai đường thẳng d<sub>1</sub> và d<sub>2</sub> lần lượt có phương trình <a href="https://www.codecogs.com/eqnedit.php?latex=\left\{\begin{matrix}x=9&plus;2t&space;\\&space;y=-1-t&space;\\&space;z=3-t&space;\end{matrix}\right.&space;\left\{\begin{matrix}x=1-2t'&space;\\&space;y=4&plus;t'&space;\\&space;z=2&plus;t'&space;\end{matrix}\right." target="_blank"><img src="https://latex.codecogs.com/gif.latex?\left\{\begin{matrix}x=9&plus;2t&space;\\&space;y=-1-t&space;\\&space;z=3-t&space;\end{matrix}\right.&space;\left\{\begin{matrix}x=1-2t'&space;\\&space;y=4&plus;t'&space;\\&space;z=2&plus;t'&space;\end{matrix}\right." title="\left\{\begin{matrix}x=9+2t \\ y=-1-t \\ z=3-t \end{matrix}\right. \left\{\begin{matrix}x=1-2t' \\ y=4+t' \\ z=2+t' \end{matrix}\right." /></a>. Viết phương trình mặt phẳng chứa hai đường thẳng d<sub>1</sub> và d<sub>2</sub>.
					@<a href="https://www.codecogs.com/eqnedit.php?latex=3x-5y&plus;z&plus;25=0" target="_blank"><img src="https://latex.codecogs.com/gif.latex?3x-5y&plus;z&plus;25=0" title="3x-5y+z+25=0" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=3x-5y&plus;z-25=0" target="_blank"><img src="https://latex.codecogs.com/gif.latex?3x-5y&plus;z-25=0" title="3x-5y+z-25=0" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=3x&plus;5y&plus;z&plus;25=0" target="_blank"><img src="https://latex.codecogs.com/gif.latex?3x&plus;5y&plus;z&plus;25=0" title="3x+5y+z+25=0" /></a>
					@~<a href="https://www.codecogs.com/eqnedit.php?latex=3x&plus;5y&plus;z-25=0" target="_blank"><img src="https://latex.codecogs.com/gif.latex?3x&plus;5y&plus;z-25=0" title="3x+5y+z-25=0" /></a>`
				Cho hàm số <i>y = f(x)</i> xác định và có đạo hàm trên tập số thực. Đồ thị hàm số <i>y = f'(x)</i> được cho như hình vẽ. Hỏi <i>f(x)</i> có bao nhiêu điểm cực đại?<br><img src="https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.15752-9/62511559_2422661981337720_6916285767752876032_n.png?_nc_cat=104&_nc_oc=AQk5wB7yK5KL4nCJfjoHrjuGdJPb2oTv732B0JSQpUIHXJz-9FdKS3HC-_EvZVGXRCA&_nc_ht=scontent.fsgn2-1.fna&oh=6079bacbf2e4b45e92d0f407ab5888ca&oe=5D50CD2C" />
					@3
					@2
					@4
					@~1`
				Cho hàm số <i>f(x)</i> xác định và liên tục trên khoảng <a href="https://www.codecogs.com/eqnedit.php?latex=(0;&plus;\infty)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?(0;&plus;\infty)" title="(0;+\infty)" /></a> sao cho <a href="https://www.codecogs.com/eqnedit.php?latex=x^2&plus;xf(e^x)&plus;f(e^x)=1(x\in(0;&plus;\infty))" target="_blank"><img src="https://latex.codecogs.com/gif.latex?x^2&plus;xf(e^x)&plus;f(e^x)=1(x\in(0;&plus;\infty))" title="x^2+xf(e^x)+f(e^x)=1(x\in(0;+\infty))" /></a>. Tính tích phân <a href="https://www.codecogs.com/eqnedit.php?latex=I=\int_{\sqrt{e}}^{e}\frac{\ln{xf(x)}}{x}dx" target="_blank"><img src="https://latex.codecogs.com/gif.latex?I=\int_{\sqrt{e}}^{e}\frac{\ln{xf(x)}}{x}dx" title="I=\int_{\sqrt{e}}^{e}\frac{\ln{xf(x)}}{x}dx" /></a>
					@-1/8
					@-2/3
					@~1/12
					@3/8`
				Trong không gian toạ độ <i>Oxyz</i> cho mặt cầu (S) có phương trình <a href="https://www.codecogs.com/eqnedit.php?latex=(x-2)^2&plus;(y&plus;1)^2&plus;(z-3)^2=20" target="_blank"><img src="https://latex.codecogs.com/gif.latex?(x-2)^2&plus;(y&plus;1)^2&plus;(z-3)^2=20" title="(x-2)^2+(y+1)^2+(z-3)^2=20" /></a>. Mặt phẳng (&alpha;) có phương trình <a href="https://www.codecogs.com/eqnedit.php?latex=x-2y&plus;2z-1=0" target="_blank"><img src="https://latex.codecogs.com/gif.latex?x-2y&plus;2z-1=0" title="x-2y+2z-1=0" /></a> và đường thẳng &Delta; có phương trình <a href="https://www.codecogs.com/eqnedit.php?latex=\frac{x}{1}=\frac{y&plus;2}{2}=\frac{z&plus;4}{-3}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{x}{1}=\frac{y&plus;2}{2}=\frac{z&plus;4}{-3}" title="\frac{x}{1}=\frac{y+2}{2}=\frac{z+4}{-3}" /></a>. Viết phương trình đường thẳng &Delta;' nằm trong mặt phẳng (&alpha;), vuông góc với &Delta; đồng thời cắt (S) theo một dây cung có độ dài lớn nhất.
					@<a href="https://www.codecogs.com/eqnedit.php?latex=\left\{\begin{matrix}x=3t&space;\\&space;y=-2&space;\\&space;z=-4&plus;t&space;\end{matrix}\right." target="_blank"><img src="https://latex.codecogs.com/gif.latex?\left\{\begin{matrix}x=3t&space;\\&space;y=-2&space;\\&space;z=-4&plus;t&space;\end{matrix}\right." title="\left\{\begin{matrix}x=3t \\ y=-2 \\ z=-4+t \end{matrix}\right." /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=\left\{\begin{matrix}x=1&plus;3t&space;\\&space;y=1&space;\\&space;z=1&plus;t&space;\end{matrix}\right." target="_blank"><img src="https://latex.codecogs.com/gif.latex?\left\{\begin{matrix}x=1&plus;3t&space;\\&space;y=1&space;\\&space;z=1&plus;t&space;\end{matrix}\right." title="\left\{\begin{matrix}x=1+3t \\ y=1 \\ z=1+t \end{matrix}\right." /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=\left\{\begin{matrix}x=2&plus;2t&space;\\&space;y=-1&plus;5t&space;\\&space;z=3&plus;4t&space;\end{matrix}\right." target="_blank"><img src="https://latex.codecogs.com/gif.latex?\left\{\begin{matrix}x=2&plus;2t&space;\\&space;y=-1&plus;5t&space;\\&space;z=3&plus;4t&space;\end{matrix}\right." title="\left\{\begin{matrix}x=2+2t \\ y=-1+5t \\ z=3+4t \end{matrix}\right." /></a>
					@~<a href="https://www.codecogs.com/eqnedit.php?latex=\left\{\begin{matrix}x=1-2t&space;\\&space;y=1-5t&space;\\&space;z=1-4t&space;\end{matrix}\right." target="_blank"><img src="https://latex.codecogs.com/gif.latex?\left\{\begin{matrix}x=1-2t&space;\\&space;y=1-5t&space;\\&space;z=1-4t&space;\end{matrix}\right." title="\left\{\begin{matrix}x=1-2t \\ y=1-5t \\ z=1-4t \end{matrix}\right." /></a>`
				Cho một vật chuyển động với gia tốc <a href="https://www.codecogs.com/eqnedit.php?latex=a(t)=-20\cos&space;\left&space;(&space;2t&plus;\frac{\pi}{4}&space;\right&space;)(m/s^2)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?a(t)=-20\cos&space;\left&space;(&space;2t&plus;\frac{\pi}{4}&space;\right&space;)(m/s^2)" title="a(t)=-20\cos \left ( 2t+\frac{\pi}{4} \right )(m/s^2)" /></a>. Biết vận tốc của vật vào thời điểm <a href="https://www.codecogs.com/eqnedit.php?latex=t=\frac{\pi}{2}(s)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?t=\frac{\pi}{2}(s)" title="t=\frac{\pi}{2}(s)" /></a> là <a href="https://www.codecogs.com/eqnedit.php?latex=15\sqrt{2}(m/s)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?15\sqrt{2}(m/s)" title="15\sqrt{2}(m/s)" /></a>. Tính vận tốc ban đầu của vật.
					@~<a href="https://www.codecogs.com/eqnedit.php?latex=5\sqrt{2}(m/s)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?5\sqrt{2}(m/s)" title="5\sqrt{2}(m/s)" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=\sqrt{2}(m/s)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\sqrt{2}(m/s)" title="\sqrt{2}(m/s)" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=0(m/s)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?0(m/s)" title="0(m/s)" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=10\sqrt{2}(m/s)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?10\sqrt{2}(m/s)" title="10\sqrt{2}(m/s)" /></a>`
				Trong không gian <i>Oxyz</i> cho mặt cầu (S) có tâm I(1;-2;3) và đường thẳng d có phương trình <a href="https://www.codecogs.com/eqnedit.php?latex=\left\{\begin{matrix}x=1&plus;2t&space;\\&space;y=-1-t&space;\\&space;z=1&plus;2t&space;\end{matrix}\right." target="_blank"><img src="https://latex.codecogs.com/gif.latex?\left\{\begin{matrix}x=1&plus;2t&space;\\&space;y=-1-t&space;\\&space;z=1&plus;2t&space;\end{matrix}\right." title="\left\{\begin{matrix}x=1+2t \\ y=-1-t \\ z=1+2t \end{matrix}\right." /></a>. Biết mặt cầu (S) tiếp xúc với d. Viết phương trình mặt cầu (S).
					@~<a href="https://www.codecogs.com/eqnedit.php?latex=(x-1)^2&plus;(x&plus;2)^2&plus;(x-3)^2=\frac{20}{9}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?(x-1)^2&plus;(x&plus;2)^2&plus;(x-3)^2=\frac{20}{9}" title="(x-1)^2+(x+2)^2+(x-3)^2=\frac{20}{9}" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=(x&plus;1)^2&plus;(x-2)^2&plus;(x&plus;3)^2=\frac{20}{9}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?(x&plus;1)^2&plus;(x-2)^2&plus;(x&plus;3)^2=\frac{20}{9}" title="(x+1)^2+(x-2)^2+(x+3)^2=\frac{20}{9}" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=(x-1)^2&plus;(x&plus;2)^2&plus;(x-3)^2=\frac{25}{9}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?(x-1)^2&plus;(x&plus;2)^2&plus;(x-3)^2=\frac{25}{9}" title="(x-1)^2+(x+2)^2+(x-3)^2=\frac{25}{9}" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=(x&plus;1)^2&plus;(x-2)^2&plus;(x&plus;3)^2=\frac{25}{9}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?(x&plus;1)^2&plus;(x-2)^2&plus;(x&plus;3)^2=\frac{25}{9}" title="(x+1)^2+(x-2)^2+(x+3)^2=\frac{25}{9}" /></a>`
				Một khối trụ có bán kính đáy bằng 2 và khối cầu ngoại tiếp hình trụ có thể tích bằng 125&pi;/6. Tính thể tích khối trụ.
					@<a href="https://www.codecogs.com/eqnedit.php?latex=2\sqrt{41}\pi" target="_blank"><img src="https://latex.codecogs.com/gif.latex?2\sqrt{41}\pi" title="2\sqrt{41}\pi" /></a>
					@6&pi;
					@~12&pi;
					@<a href="https://www.codecogs.com/eqnedit.php?latex=\sqrt{41}\pi" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\sqrt{41}\pi" title="\sqrt{41}\pi" /></a>`
				Xác định tập nghiệm S của phương trình <a href="https://www.codecogs.com/eqnedit.php?latex=(3&plus;2\sqrt{2})^x-2(1&plus;\sqrt{2})^x-1=0" target="_blank"><img src="https://latex.codecogs.com/gif.latex?(3&plus;2\sqrt{2})^x-2(1&plus;\sqrt{2})^x-1=0" title="(3+2\sqrt{2})^x-2(1+\sqrt{2})^x-1=0" /></a>.
					@<a href="https://www.codecogs.com/eqnedit.php?latex=S=\{1&plus;\sqrt{2}\}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?S=\{1&plus;\sqrt{2}\}" title="S=\{1+\sqrt{2}\}" /></a>
					@~<a href="https://www.codecogs.com/eqnedit.php?latex=S=\{1\}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?S=\{1\}" title="S=\{1\}" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=S=\{1-\sqrt{2};1&plus;\sqrt{2}\}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?S=\{1-\sqrt{2};1&plus;\sqrt{2}\}" title="S=\{1-\sqrt{2};1+\sqrt{2}\}" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=S=\{-1;1\}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?S=\{-1;1\}" title="S=\{-1;1\}" /></a>`
				Cho lăng trụ tam giác đều <i>ABC.A'B'C'</i> có tất cả các cạnh đều bằng <i>a</i>. Gọi M,N lần lượt là trung điểm A'B' và AA'. Tính khoảng cách từ điểm M đến mặt phẳng (<i>NBC</i>) theo <i>a</i>.
					@<a href="https://www.codecogs.com/eqnedit.php?latex=\frac{a\sqrt{3}}{10}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{a\sqrt{3}}{10}" title="\frac{a\sqrt{3}}{10}" /></a>
					@~<a href="https://www.codecogs.com/eqnedit.php?latex=\frac{3a\sqrt{3}}{8}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{3a\sqrt{3}}{8}" title="\frac{3a\sqrt{3}}{8}" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=\frac{3a\sqrt{3}}{20}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{3a\sqrt{3}}{20}" title="\frac{3a\sqrt{3}}{20}" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=\frac{a\sqrt{3}}{6}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{a\sqrt{3}}{6}" title="\frac{a\sqrt{3}}{6}" /></a>`
				Tìm giá trị nhỏ nhất của hàm số <a href="https://www.codecogs.com/eqnedit.php?latex=y=x^3-3x^2-9x&plus;5" target="_blank"><img src="https://latex.codecogs.com/gif.latex?y=x^3-3x^2-9x&plus;5" title="y=x^3-3x^2-9x+5" /></a> trên đoạn [-2;2].
					@3
					@-22
					@-1
					@~-17`
				Một hộp đựng 10 tấm thẻ phân biệt gồm 6 tấm thẻ ghi số 1 và 4 tấm ghi số 0. Một trò chơi được thực hiện bằng cách rút ngẫu nhiên một thẻ từ hộp rồi hoàn lại. Sau một số lần rút, trò chơi sẽ kết thúc khi có đúng 3 lần rút được thẻ ghi số 1 hoặc đúng 3 lần rút được thẻ ghi số 0. Tính xác suất để trò chơi kết thúc khi có đúng 3 lần rút được thẻ ghi số 1.
					@0,9072
					@0,33696
					@0,456
					@~0,68256`
				Tính theo <i>a</i> thể tích khối nón nội tiếp tứ diện đều cạnh <i>a</i>.
					@<a href="https://www.codecogs.com/eqnedit.php?latex=\frac{\sqrt{6}}{27}\pi&space;a^3" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{\sqrt{6}}{27}\pi&space;a^3" title="\frac{\sqrt{6}}{27}\pi a^3" /></a>
					@~<a href="https://www.codecogs.com/eqnedit.php?latex=\frac{\sqrt{6}}{108}a^3" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{\sqrt{6}}{108}a^3" title="\frac{\sqrt{6}}{108}a^3" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=\frac{\sqrt{6}}{27}a^3" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{\sqrt{6}}{27}a^3" title="\frac{\sqrt{6}}{27}a^3" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=\frac{\sqrt{6}}{108}\pi&space;a^3" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{\sqrt{6}}{108}\pi&space;a^3" title="\frac{\sqrt{6}}{108}\pi a^3" /></a>`
				Cho phương trình <a href="https://www.codecogs.com/eqnedit.php?latex=m(\sqrt{x^2-2x&plus;2}&plus;1)-x^2&plus;2x=0" target="_blank"><img src="https://latex.codecogs.com/gif.latex?m(\sqrt{x^2-2x&plus;2}&plus;1)-x^2&plus;2x=0" title="m(\sqrt{x^2-2x+2}+1)-x^2+2x=0" /></a> (<i>m</i> là tham số). Biết rằng tập hợp tất cả các giá trị của tham số m để phương trình trên có nghiệm thuộc đoạn <a href="https://www.codecogs.com/eqnedit.php?latex=[0;1&plus;2\sqrt{2}]" target="_blank"><img src="https://latex.codecogs.com/gif.latex?[0;1&plus;2\sqrt{2}]" title="[0;1+2\sqrt{2}]" /></a> là đoạn <a href="https://www.codecogs.com/eqnedit.php?latex=[a;b]" target="_blank"><img src="https://latex.codecogs.com/gif.latex?[a;b]" title="[a;b]" /></a>. Tính giá trị biểu thức <a href="https://www.codecogs.com/eqnedit.php?latex=T=2b-a" target="_blank"><img src="https://latex.codecogs.com/gif.latex?T=2b-a" title="T=2b-a" /></a>.
					@~4
					@7/2
					@3
					@1/2`
				Cho cấp số cộng <a href="https://www.codecogs.com/eqnedit.php?latex=(u_n)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?(u_n)" title="(u_n)" /></a> có công thức tổng quát là <a href="https://www.codecogs.com/eqnedit.php?latex=u_n=5-2n,n\in\mathbb{N^*}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?u_n=5-2n,n\in\mathbb{N^*}" title="u_n=5-2n,n\in\mathbb{N^*}" /></a>. Tính tổng 20 số hạng đầu tiên của cấp số cộng.
					@-350
					@440
					@~-320
					@-340`
				Cho hình chóp <i>S.ABCD</i> có đáy <i>ABCD</i> là hình vuông cạnh <i>a</i>, đường thẳng SA vuông góc với mặt phẳng (<i>ABCD</i>). Gọi M là trung điểm SD, N là điểm trên cạnh BC sao cho CN = 2BN. Biết rằng <a href="https://www.codecogs.com/eqnedit.php?latex=MN=\frac{a\sqrt{10}}{3}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?MN=\frac{a\sqrt{10}}{3}" title="MN=\frac{a\sqrt{10}}{3}" /></a>, tính khoảng cách từ A đến mặt phẳng (<i>SBD</i>) theo <i>a</i>.
					@<a href="https://www.codecogs.com/eqnedit.php?latex=\frac{a\sqrt{14}}{7}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{a\sqrt{14}}{7}" title="\frac{a\sqrt{14}}{7}" /></a>
					@~<a href="https://www.codecogs.com/eqnedit.php?latex=\frac{a\sqrt{5}}{5}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{a\sqrt{5}}{5}" title="\frac{a\sqrt{5}}{5}" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=\frac{a\sqrt{14}}{14}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{a\sqrt{14}}{14}" title="\frac{a\sqrt{14}}{14}" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=\frac{a\sqrt{30}}{10}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{a\sqrt{30}}{10}" title="\frac{a\sqrt{30}}{10}" /></a>`
				Viết phương trình tiếp tuyến của đồ thị hàm số <a href="https://www.codecogs.com/eqnedit.php?latex=y=\frac{x-1}{x&plus;2}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?y=\frac{x-1}{x&plus;2}" title="y=\frac{x-1}{x+2}" /></a> tại điểm M(1;0).
					@~<a href="https://www.codecogs.com/eqnedit.php?latex=y=\frac{1}{3}x-\frac{1}{3}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?y=\frac{1}{3}x-\frac{1}{3}" title="y=\frac{1}{3}x-\frac{1}{3}" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=y=\frac{1}{3}x&plus;1" target="_blank"><img src="https://latex.codecogs.com/gif.latex?y=\frac{1}{3}x&plus;1" title="y=\frac{1}{3}x+1" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=y=x-1" target="_blank"><img src="https://latex.codecogs.com/gif.latex?y=x-1" title="y=x-1" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=y=\frac{1}{9}x-\frac{1}{9}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?y=\frac{1}{9}x-\frac{1}{9}" title="y=\frac{1}{9}x-\frac{1}{9}" /></a>`
				Trong không gian <i>Oxyz</i>, cho mặt cầu (S) có phương trình <a href="https://www.codecogs.com/eqnedit.php?latex=x^2&plus;y^2&plus;z^2-2x&plus;4y-4=0" target="_blank"><img src="https://latex.codecogs.com/gif.latex?x^2&plus;y^2&plus;z^2-2x&plus;4y-4=0" title="x^2+y^2+z^2-2x+4y-4=0" /></a>. Tìm mệnh đề sai trong các mệnh đề sau:
					@Mặt cầu (S) có diện tích là 36&pi;
					@Mặt cầu đi qua điểm M(1;1;0)
					@~Mặt cầu có tâm I(-1;2;0)
					@Mặt cầu có bán kính R = 3`
				Cho phương trình <a href="https://www.codecogs.com/eqnedit.php?latex=m.3^{2x^2-3x-2}-3^{x^2-3x&plus;2}=m.3^{x^2-4}-1" target="_blank"><img src="https://latex.codecogs.com/gif.latex?m.3^{2x^2-3x-2}-3^{x^2-3x&plus;2}=m.3^{x^2-4}-1" title="m.3^{2x^2-3x-2}-3^{x^2-3x+2}=m.3^{x^2-4}-1" /></a> (<i>m</i> là tham số). Tính tổng tất cả các giá trị của <i>m</i> để phương trình có đúng ba nghiệm phân biệt.
					@-7
					@85/81
					@81
					@~109`
				Cho <a href="https://www.codecogs.com/eqnedit.php?latex=\log_2a=x;\log_2b=y;a>0,b>0,b^3\neq&space;a^2" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\log_2a=x;\log_2b=y;a>0,b>0,b^3\neq&space;a^2" title="\log_2a=x;\log_2b=y;a>0,b>0,b^3\neq a^2" /></a>. Tìm biểu diễn của <a href="https://www.codecogs.com/eqnedit.php?latex=\log_{a^{-2}b^3}(a^4b)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\log_{a^{-2}b^3}(a^4b)" title="\log_{a^{-2}b^3}(a^4b)" /></a> theo <i>x</i> và <i>y</i>.
					@<a href="https://www.codecogs.com/eqnedit.php?latex=\frac{x-4y}{3y&plus;2x}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{x-4y}{3y&plus;2x}" title="\frac{x-4y}{3y+2x}" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=\frac{4x&plus;y}{-2y&plus;3x}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{4x&plus;y}{-2y&plus;3x}" title="\frac{4x+y}{-2y+3x}" /></a>
					@<a href="https://www.codecogs.com/eqnedit.php?latex=\frac{4x&plus;y}{3y&plus;2x}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{4x&plus;y}{3y&plus;2x}" title="\frac{4x+y}{3y+2x}" /></a>
					@~<a href="https://www.codecogs.com/eqnedit.php?latex=\frac{4x&plus;y}{3y-2x}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{4x&plus;y}{3y-2x}" title="\frac{4x+y}{3y-2x}" /></a>`
				Cho hàm số <a href="https://www.codecogs.com/eqnedit.php?latex=y=f(x)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?y=f(x)" title="y=f(x)" /></a> xác định và có đạo hàm trên đoạn [0;2]. Biết rằng <a href="https://www.codecogs.com/eqnedit.php?latex=f(2)=-3;\int_0^2xf'(x)dx&space;=&space;-4" target="_blank"><img src="https://latex.codecogs.com/gif.latex?f(2)=-3;\int_0^2xf'(x)dx&space;=&space;-4" title="f(2)=-3;\int_0^2xf'(x)dx = -4" /></a>. Tính tích phân <a href="https://www.codecogs.com/eqnedit.php?latex=I=\int_{0}^{2}f(x)dx" target="_blank"><img src="https://latex.codecogs.com/gif.latex?I=\int_{0}^{2}f(x)dx" title="I=\int_{0}^{2}f(x)dx" /></a>.
					@-1
					@0
					@-7
					@~-2`
				Trong không gian toạ độ <i>Oxyz</i>, cho đường thẳng <a href="https://www.codecogs.com/eqnedit.php?latex=d:\left\{\begin{matrix}x=-1&plus;t&space;\\&space;y=1&plus;t&space;\\&space;z=1-t&space;\end{matrix}\right.,(t\in\mathbb{R})" target="_blank"><img src="https://latex.codecogs.com/gif.latex?d:\left\{\begin{matrix}x=-1&plus;t&space;\\&space;y=1&plus;t&space;\\&space;z=1-t&space;\end{matrix}\right.,(t\in\mathbb{R})" title="d:\left\{\begin{matrix}x=-1+t \\ y=1+t \\ z=1-t \end{matrix}\right.,(t\in\mathbb{R})" /></a> và mặt phẳng <a href="https://www.codecogs.com/eqnedit.php?latex=(\alpha):m^2x-3y&plus;z&plus;3m=0" target="_blank"><img src="https://latex.codecogs.com/gif.latex?(\alpha):m^2x-3y&plus;z&plus;3m=0" title="(\alpha):m^2x-3y+z+3m=0" /></a> (<i>m</i> là tham số). Tìm tất cả các giá trị của <i>m</i> để đường thẳng <i>d</i> song song với mặt phẳng (&alpha;).
					@~<i>m</i> = -2
					@<i>m</i> = 2 hoặc <i>m</i> = -2
					@<i>m</i> = 2
					@<i>m</i> = 1 hoặc <i>m</i> = 2`
				Biết rằng đồ thị hàm bậc bốn <a href="https://www.codecogs.com/eqnedit.php?latex=y=f(x)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?y=f(x)" title="y=f(x)" /></a> được cho bởi hình vẽ dưới. Tìm số giao điểm của đồ thị hàm số <a href="https://www.codecogs.com/eqnedit.php?latex=y=g(x)=\left&space;[&space;f'(x)&space;\right&space;]^2-f(x).f''(x)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?y=g(x)=\left&space;[&space;f'(x)&space;\right&space;]^2-f(x).f''(x)" title="y=g(x)=\left [ f'(x) \right ]^2-f(x).f''(x)" /></a> và trục hoành.<br><img src="https://scontent.fsgn2-4.fna.fbcdn.net/v/t1.15752-9/62419942_405001670106015_731340837820039168_n.png?_nc_cat=101&_nc_eui2=AeHqCCOiDvPl9kV2zK9YAyzMk276e24RkSVqT-YxfOJW-wxQPquDQlORZi4MUXshTIpzimz5jy0tU--3VGy-Qoj_G1lWu2D_nc628shJJOmBpA&_nc_oc=AQmyFXG8y3c2ZIIVldcpsbVMtvOmJFL3iT1CODE7Kt6bWWzTDAbc_3AY7cH02qOU0ZY&_nc_ht=scontent.fsgn2-4.fna&oh=f96e594e113dfeb6d78fe2313c4e9647&oe=5D9C193A"/>.
					@4
					@~0
					@6
					@2`
				Tính tổng hoành độ các giao điểm của đồ thị hàm số <a href="https://www.codecogs.com/eqnedit.php?latex=y=\frac{5x&plus;6}{x&plus;2}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?y=\frac{5x&plus;6}{x&plus;2}" title="y=\frac{5x+6}{x+2}" /></a> và đường thẳng <a href="https://www.codecogs.com/eqnedit.php?latex=y=-x" target="_blank"><img src="https://latex.codecogs.com/gif.latex?y=-x" title="y=-x" /></a>.
					@-7
					@-5
					@~5
					@7
			</div>
			<div id="countDown">	
				<p>Thời gian còn lại:</p>
				<p class="minute">Phút</p>
				<p class="second">Giây<p>
				
			<div>
			<script type="text/javascript">
				function getCookie(name){
					var cname = name + "=";
					var dc = document.cookie;
					if (dc.length > 0) {
						begin = dc.indexOf(cname);
						if (begin != -1) {
							begin += cname.length;
							end = dc.indexOf(";", begin);
						if (end == -1) end = dc.length;
						return unescape(dc.substring(begin, end));
						}
					}
					return null;
				}
				if(document.cookie && document.cookie.match('myClock_Toan2')){
				  // get deadline value from cookie
				  var deadline = getCookie('myClock_Toan2');
				}
				 
				
				else{
				  // create deadline 90 minutes from now
					var currentTime = Date.parse(new Date());
					var deadline = Date.parse(new Date(currentTime + 90*60*1000));
				 
				  // store deadline in cookie for future reference
				  document.cookie = 'myClock_Toan2=' + deadline + '; path=/';
				}
							
			
			
				
				var currentTime = Date.parse(new Date());
				
				var t=deadline-currentTime;
				var second = Math.floor((t / 1000) % 60);
				var minute = Math.floor(t / 1000 / 60);
				
				
				
			
				var nd = $("#noi_dung_de").html();
				var cau_hoi = nd.split('`');
				var html = ""; var tra_loi_dung = 0;
				for(var i = 0;i<cau_hoi.length;i++)
				{
					var chi_tiet = cau_hoi[i].split('@');
					//alert(chi_tiet[i]);
					for(var j=0;j<chi_tiet.length;j++)
					{
			 
						if (j==0) html += "<tr><td><b>Câu " + (i+1) + ":</b></td><td> <b>" + chi_tiet[j].trim() + "</b></td></tr>";
						else 
						{
							html += "<tr><td></td><td id='" + (i + 1 + String.fromCharCode(64 + j))+"'><input type='radio' name='" + (i + 1 )+ "' id='" + (i + 1 + String.fromCharCode(64 + j))+"' value='" + chi_tiet[j].trim() +"'> " + String.fromCharCode(64 + j) + ". " + chi_tiet[j].replace("~","").trim() + "</td></tr>";
						}
					}
				}

				$("#noi_dung_de").empty().append("<table>" + html + "</table><input type='submit' value='Nộp bài' id='tra_loi_xong' style='margin-left:585px;'>").fadeIn();
				$("#noi_dung_de input").click(function(){   
				//Lấy id của radio
				var id = $(this).attr("id");
				for (var j=1;j<5;j++){
					var newid=id.substr(0,1)+String.fromCharCode(64 + j);
					
					$("td#" + newid).css("background-color","");
				}
				$("td#" + id).css("background-color","yellow");
				
				});   
				
2				
				
3				
4				
			
				
				/*jQuery(document).ready(function($) {
					
					setTimeout(function(){
						
						$('#tra_loi_xong').trigger( "click" ) ;
						}, time);
				}); */
				
				
				
			
			
				$("#tra_loi_xong").click(function(){
					$('#noi_dung_de input').each(function () {
					var id = $(this).attr("id");
					var ctl = $(this).val();
				 
					//Hiển thị câu đúng với nền là màu đỏ
					if (ctl[0] == '~') 
					{
						//alert($("td#" + id).css("background-color"));
						if ($("td#" + id).css("background-color") == "rgb(255, 255, 0)")
						{
							tra_loi_dung++;
						}
						else $("td#" + id).css("background-color","red");     
					}
				});
				$("#noi_dung_de").append("<p style='text-align:center'>Số câu đúng :" + (tra_loi_dung < 0 ? "0" : tra_loi_dung) + " câu. Điểm : " + tra_loi_dung*0.2+ "</p>");
				$("#tra_loi_xong").fadeOut();
				
				});
				
				var timer = setInterval(function() {
					
				   $('.minute').text(minute +" Phút")
				   $('.second').text(second-- +" Giây");
				   if (second == -1) {
						minute--;
						second=60;
				   }
				   if (minute ==-1)  {
						 alert("Đã hết giờ làm bài, kiểm tra kết quả");
					  $('#tra_loi_xong').trigger( "click" ) ;
					  $('.second').fadeOut("slow");
					  $('.minute').fadeOut("slow");
					  clearInterval(timer);
				   }
				   
				}, 1000);
				
			</script>
			
		</div>
	</body>
</html>