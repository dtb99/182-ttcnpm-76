<!DOCTYPE html>
<?php
include 'UserID.php';
$user = new UserID();
if(!$user->isLogin()) header('Location:login.php');
?>
<html>
	<head>
		<meta charset="UTF-8" />
		<title>Đề THPTQG môn Hóa học</title>
		<link rel="stylesheet" type="text/css" href="examstyle.css" />
		
		
	</head>
	<body>
		<h1>Đề thi thử THPTQG 2019</h1>
		<p align="center"> <b> Môn thi: Hóa học</b> </p>
		<p id="time">Thời gian làm bài: 50 phút</p>	
		<p align="center"> Cho biết nguyên tử khối của các nguyên tố: H = 1; He = 4; Li = 7; Be = 9; C = 12; N = 14; O = 16; <br>
Na = 23; Mg = 24; Al = 27; P = 31; S = 32; Cl = 35,5;K = 39; Ca = 40; Cr = 52; Mn = 55; Fe = 56; <br>
Cu = 64; Zn = 65; Br = 80; Rb = 85; Sr = 88; Ag = 108; Sn = 119; Cs = 133; Ba = 137; Pb = 207. </p>
		<p align="center"> <b>(Thí sinh không sử dụng bảng hệ thống tuần hoàn) </b></p>
		<script src='examform.js'></script>
		<div id="content">
			<div id="noi_dung_de" style='display:none;'>
				
			<!--Câu 1-->Hiện tượng xảy ra khi cho giấy quỳ khô vào bình đựng khí amoniac là:$
				Giấy quỳ mất màu$
				Giấy quỳ chuyển sang màu xanh$
				Giấy quỳ chuyển sang màu đỏ$
				~Giấy quỳ không chuyển màu^
			<!--Câu 2-->Xà phòng hóa chất nào sau đây thu được glixerol?$
				Metyl axetat$
				~Tristearin$
				Metyl fomat$
				Benzyl axetat^					
				
			<!--Câu 3-->Tiến hành thí nghiệm với các dung dịch X, Y, Z và T. Kết quả được ghi ở bảng sau:
			<br> <img src="https://scontent.fsgn5-3.fna.fbcdn.net/v/t1.15752-9/62468305_911180155912927_3786232302439235584_n.png?_nc_cat=111&_nc_oc=AQnsBNiSQQbzkk7PKAa7PTb2IwfWK8UTqblmB1X-23QsjAoiW-6aG0-4bV2JZ2l1xcA&_nc_ht=scontent.fsgn5-3.fna&oh=3359c5512d94b3978b44658bfc9dad81&oe=5D862EA8"/>
			<br> X, Y, Z, T lần lượt là:$
				Glucozơ, lysin, etyl fomat, anilin$
				~Etyl fomat, lysin, glucozơ, phenol$
				Etyl fomat, lysin, glucozơ, axit acrylic$
				Lysin, etyl fomat, glucozơ, anilin^
			<!--Câu 4-->Phát biểu nào sau đây là <b>đúng</b>?$
				Amophot là hỗn hợp các muối (NH<sub>4</sub>)<sub>2</sub>HPO<sub>4</sub> và KNO<sub>3</sub>$
				~Phân hỗn hợp chứa nito,phot pho,kali được gọi chung là phân NPK$
				Phân lân cung cấp nitơ hoá hợp cho cây dưới dạng ion nitrat (NO<sub>3</sub><sup>-</sup>) và ion amoni (NH<sub>4</sub><sup>+</sup>).$
				Phân urê có công thức là (NH<sub>4</sub>)<sub>2</sub>CO<sub>3</sub>^
			<!--Câu 5-->Tổng số đồn phân cấu tạo của hợp chất hữu cơ no, đơn chức, mạch hở, có cùng công thức phân tử
C<sub>5</sub>H<sub>10</sub>O<sub>2</sub>, phản ứng được với dung dịch NaOH nhưng không có phản ứng tráng bạc là:$
				4$
				~9$
				8$
				5^
			<!--Câu 6-->Hỗn hợp khí E gồm một amin bậc III no, đơn chức, mạch hở và hai ankin X, Y (M<sub>X</sub> &lt; M<sub>Y</sub>). Đốt cháy
hoàn toàn 0,15 mol hỗn hợp E cần dùng 11,2 lít O<sub>2</sub> (đktc), thu được hỗn hợp F gồm CO<sub>2</sub>, H<sub>2</sub>O và N<sub>2</sub>. Dẫn toàn
bộ F qua bình đựng dung dịch KOH đặc, dư đến phản ứng hoàn toàn thấy khối lượng bình bazơ nặng thêm 20,8
gam. Số cặp công thức cấu tạo ankin X, Y thỏa mãn là:$
				1$
				2$
				~3$
				4^
			<!--Câu 7-->Este Z đơn chức, mạch hở được tạo ra thành từ axit X và ancol Y. Đốt cháy hoàn toàn 2,15 gam Z, thu
được 0,1 mol CO<sub>2</sub> và 0,075 mol H<sub>2</sub>O. Mặt khác, cho 2,15 gam Z tác dụng vừa đủ với dung dịch KOH, thu được
2,75 gam muối. Công thức của X và Y lần lượt là:$
				~C<sub>2</sub>H<sub>2</sub>COOH và CH<sub>3</sub>OH$
				CH<sub>3</sub>COOH và C<sub>3</sub>H<sub>5</sub>OH$
				HCOOH và C<sub>3</sub>H<sub>7</sub>OH$
				 HCOOH và C<sub>3</sub>H<sub>5</sub>OH^
			<!--Câu 8-->Thực hiện các thí nghiệm sau:
			<br>(1) Cho hỗn hợp gồm 2a mol Na và a mol Al vào lượng nước dư.
			<br>(2) Cho a mol bột Cu vào dung dịch chứa a mol Fe<sub>2</sub>(SO<sub>4</sub>)<sub>3</sub>.
			<br>(3) Cho dung dịch chứa a mol KHSO<sub>3</sub> vào dung dịch chứa a mol KHCO<sub>2</sub>.
			<br>(4) Cho dung dịch chứa a mol BaCl<sub>2</sub> vào dung dịch chứa a mol CuSO<sub>4</sub>.
			<br>(5) Cho dung dịch chứa a mol Fe(NO<sub>3</sub>)<sub>2</sub> vào dung dịch chứa a mol AgNO<sub>3</sub>.
			<br>(6) Cho a mol Na<sub>2</sub>O vào dung dịch chứa a mol CuSO<sub>4</sub>.
			<br>(7) Cho hỗn hợp Fe<sub>2</sub>O<sub>3</sub> và Cu (tỉ lệ mol tương ứng 2:1) vào dung dịch HCl dư.
			<br>Sau khi kết thúc thí nghiệm, số trường hợp thu được dung dịch chứa hai muối là:$
				4$
				~1$
				3$
				2^
			<!--Câu 9-->Hòa tan hoàn toàn m gam hỗn hợp gồm Na, Na<sub>2</sub>O, NaOH và Na<sub>2</sub>CO<sub>3</sub> trong dung dịch axít H<sub>2</sub>SO<sub>4</sub> 40%
(vừa đủ) thu được 8,96 lít hỗn hợp khí (ở đktc) có tỷ khối đối với H<sub>2</sub> bằng 16,75 và dung dịch Y có nồng độ
51,449%. Cô cạn Y thu được 170,4 gam muối. Giá trị của m là:$
				50,4$
				50,8$
				50,2$
				~50,6^
			<!--Câu 10-->Thủy phân hoàn toàn 14,6 gam Gly-Ala trong dung dịch NaOH dư, thu được m gam muối. Giá trị của
m là:$
				16,8$
				~20,8$
				18$
				22,6^
			<!--Câu 11-->Cho 6,72 gam hỗn hợp gồm Fe, FeO, Fe<sub>2</sub>O<sub>3</sub>, Fe<sub>3</sub>O<sub>4</sub> phản ứng hết với 500 mL dung dịch HNO<sub>3</sub> a (M)
loãng dư thu được 0,448 lít khí NO (đktc, là sản phẩm khử duy nhất) và dung dịch X. Dung dịch X có thể hòa
tan tối đa 8,4 gam Fe. Giá trị của a là:$
				1,50$
				0,88$
				~1,00$
				0,58^
			<!--Câu 12-->Etanol là chất có tác động đến thần kinh trung ương. Khi hàm lượng etanol trong máu tăng cao sẽ có
hiện tượng nôn, mất tỉnh táo và có thể dẫn đến tử vong. Tên gọi khác của etanol là:$
				~ancol etylic$
				axit fomic$
				etanal$
				phenol^
			<!--Câu 13-->Chất X (có M = 60 và chứa C, H, O). Chất X phản ứng được với Na, NaOH và NaHCO<sub>3</sub>. Tên gọi của
X là:$
				ancol propylic$
				metyl fomat$
				axit fomic$
				~axit axetic^
			<!--Câu 14-->Hòa tan hoàn toàn hỗn hợp X gồm Fe(NO<sub>3</sub>)<sub>2</sub>, Fe<sub>3</sub>O<sub>4</sub>, MgO và Mg trong dung dịch chứa 9,22 mol HCl
loãng. Sau khi các phản ứng xảy ra hoàn toàn thu được dung dịch Y chỉ chứa 463,15 gam muối clorua và 29,12 lít
(đktc) khí Z gồm NO và H<sub>2</sub> có tỉ khối so với H<sub>2</sub> là 69/13. Thêm NaOH dư vào dung dịch Y, sau phản ứng thấy
xuất hiện kết tủa T. Nung T trong không khí đến khối lượng không đổi được 204,4 gam chất rắn M. Biết trong X,
oxi chiếm 29,68% theo khối lượng. Phần trăm khối lượng MgO trong X gần nhất với giá trị nào dưới đây?$
				~13,33%$
				33,33%$
				20,00%$
				6,80%^
			<!--Câu 15--> X là axit no, đơn chức, Y là axit không no, có một liên kết đôi C=C, có đồng phân hình học và Z là
este hai chức tạo X, Y và một ancol no (tất cả các chất đều thuần chức, mạch hở). Đốt cháy hoàn toàn 9,52
gam E chứa X, Y và Z thu được 5,76 gam H<sub>2</sub>O. Mặt khác, 9,52 gam E có thể phản ứng tối đa với dung dịch
chứa 0,12 mol NaOH sản phẩm sau phản ứng có chứa 12,52 hỗn hợp các chất hữu cơ. Cho các phát biểu liên
quan tới bài toán gồm:
			<br>(1) Phần trăm khối lượng của X trong E là 72,76%            
			<br>(2) Số mol của Y trong E là 0,08 mol
			<br>(3) Khối lượng của Z trong E là 1,72 gam.                   
			<br>(4) Tổng số nguyên tử (C, H, O) trong Y là 12
			<br>(5) X không tham gia phản ứng tráng bạc
			<br>Số phát biểu đúng là?$
				4$
				2$
				~3$
				5^
			<!--Câu 16--> Cho từ từ 300 ml dung dịch NaHCO<sub>3</sub> 0,1M, K<sub>2</sub>CO3 0,2M vào 100 ml dung dịch HCl 0,2M; NaHSO<sub>4</sub>
0,6M thu được V lít CO<sub>2</sub> thoát ra ở đktc và dung dịch X. Thêm vào dung dịch X 100 ml dung dịch KOH 0,6M;
BaCl<sub>2</sub> 1,5M thu được m gam kết tủa. Giá trị của V và m là:$
				~1,0752 và 22,254g$
				1,0752 và 23,436g$
				0,448 và 25,8g$
				0,448 và 11,82g^
			<!--Câu 17-->Thể tích N<sub>2</sub> thu được khi nhiệt phân hoàn toàn 16 gam NH<sub>4</sub>NO<sub>2</sub> là:$
				1,12 lít$
				11,2 lít$
				0,56 lít$
				~5,6 lít^
			<!--Câu 18-->Cho dung dịch X chứa 0,05 mol Al<sup>3+</sup>; 0,1 mol Mg<sup>2+</sup>; 0,1 mol NO<sub>3</sub><sup>–</sup>; x mol Cl<sup>–</sup> ; y mol Cu<sup>2+</sup>.
			<br>– Nếu cho dung dịch X tác dụng với dung dịch AgNO3 dư thì thu được 43,05 gam kết tủa
			<br>– Nếu cho 450 ml dung dịch NaOH 1,0M vào dung dịch X thì khối lượng kết tủa thu được là:
			<br>(Biết các phản ứng đều xảy ra hoàn toàn)$
				12,65 gam$
				10,25 gam$
				12,15 gam$
				~8,25 gam^
			<!--Câu 19-->PVC là chất rắn vô định hình, cách điện tốt, bền với axit, được dùng làm vật liệu cách điện, ống dẫn
nước, vải che mưa,... PVC được tổng hợp trực tiếp từ monome nào sau đây?$
				Acrilonitrin$
				Propilen$
				Vinyl axetat$
				~Vinyl clorua^
			<!--Câu 20-->Đốt cháy hoàn toàn m gam hỗn hợp gồm xenlulozơ, tinh bột, glucozơ và saccarozơ cần 2,52 lít O<sub>2</sub>
(đktc), thu được 1,8 gam nước. Giá trị của m là:$
				~3,15$
				6,20$
				3,60$
				 5,25^
			<!--Câu 21-->Chất nào sau đây thuộc loại amin bậc ba?$
				~(CH<sub>3</sub>)<sub>3</sub>N$
				 CH<sub>3</sub>–NH–CH<sub>3</sub>$
				C<sub>2</sub>H<sub>5</sub>–NH<sub>2</sub>$
				CH<sub>3</sub>–NH<sub>2</sub>^
			<!--Câu 22-->Thủy phân m gam saccarozơ trong môi trường axit với hiệu suất 90%, thu được sản phẩm chứa 10,8
gam glucozơ. Giá trị của m là:$
				18,5$
				20,5$
				17,1$
				~22,8^
			<!--Câu 23-->Trước những năm 50 của thế kỷ XX, công nghiệp tổng hợp hữu cơ dựa trên nguyên liệu chính là
axetilen. Ngày nay, nhờ sự phát triển vượt bậc của công nghệ khai thác và chế biến dầu mỏ, etilen trở thành
nguyên liệu rẻ tiền, tiện lợi hơn nhiều so với axetilen. Công thức phân tử của etilen là:$
				C<sub>2</sub>H<sub>2</sub>$
				~C<sub>2</sub>H<sub>4</sub>$
				CH<sub>4</sub>$
				C<sub>2</sub>H<sub>6</sub>^
			<!--Câu 24-->Thủy phân hoàn toàn 4,34 gam tripeptit mạch hở X (được tạo nên từ hai α-amino axit có công thức
dạng H<sub>2</sub>NC<sub>x</sub>H<sub>y</sub>COOH) bằng dung dịch NaOH dư, thu được 6,38 gam muối. Mặt khác thủy phân hoàn toàn 4,34
gam X bằng dung dịch HCl dư, thu được m gam muối. Giá trị của m là:$
				6,53$
				8,25$
				~7,25$
				7,52^
			<!--Câu 25-->Hiệu ứng nhà kính là hiện tượng Trái Đất đang ấm dần lên do các bức xạ có bước sóng dài trong vùng
hồng ngoại bị giữ lại mà không bức xạ ra ngoài vũ trụ. Khí nào dưới đây là nguyên nhân chính gây ra
hiệu ứng nhà kính?$
				SO<sub>2</sub>$
				N<sub>2</sub>$
				~CO<sub>2</sub>$
				O<sub>2</sub>^
			<!--Câu 26-->Hỗn hợp E gồm muối vô cơ X (CH<sub>8</sub>N<sub>2</sub>O<sub>3</sub>) và đipeptit Y (C4H8N<sub>2</sub>O<sub>3</sub>). Cho E tác dụng với dung dịch
NaOH đun nóng, thu được khí Z. Cho E tác dụng với dung dịch HCl dư, thu được khí T và chất hữu cơ Q. Nhận
định nào sau đây <b>sai</b>?$
				Chất X là (NH<sub>4</sub>)<sub>2</sub>CO<sub>3</sub>$
				Chất Z là NH<sub>3</sub> và chất T là CO<sub>2</sub>$
				~Chất Q là H<sub>2</sub>NCH<sub>2</sub>COOH$
				Chất Y là H<sub>2</sub>NCH<sub>2</sub>CONHCH<sub>2</sub>COOH^
			<!--Câu 27-->Nung nóng hỗn hợp chứa các chất có cùng số mol gồm Al(NO<sub>3</sub>)<sub>3</sub>, NaHCO<sub>3</sub>, Fe(NO<sub>3</sub>)<sub>3</sub>, CaCO<sub>3</sub> đến khi
khối lượng không đổi, thu được chất rắn X. Hòa tan X vào nước dư, thu được dung dịch Y và chất rắn Z. Thổi
luồng khí CO (dùng dư) qua chất rắn Z, nung nóng thu được chất rắn T. Các phản ứng xảy ra hoàn toàn. Nhận
định nào sau đây là đúng?$
				Nhỏ dung dịch HCl vào dung dịch Y, thấy khí không màu thoát ra$
				Nhỏ dung dịch HCl vào dung dịch Y, thấy xuất hiện ngay kết tủa$
				~Chất rắn T chứa một đơn chất và một hợp chất$
				Chất rắn T chứa một đơn chất và hai hợp chất^
			<!--Câu 28--> Hấp thụ hoàn toàn 0,56 lít CO<sub>2</sub> (đktc) vào 50 mL dung dịch gồm K<sub>2</sub>CO<sub>3</sub> 1,0M và KOH xM, sau khi
các phản ứng xảy ra hoàn toàn thu được dung dịch Y. Cho toàn bộ Y tác dụng với dung dịch BaCl<sub>2</sub> dư, thu
được 9,85 gam kết tủa. Giá trị của x là:$
				1,0$
				~0,5$
				1,2$
				1,5^
			<!--Câu 29-->Thủy phân hoàn toàn m gam hỗn hợp hai este đơn chức, mạch hở E, F (ME < MF) trong 700 ml dung
dịch KOH 1M thu được dung dịch X và hỗn hợp Y gồm 2 ancol là đồng đẳng liên tiếp. Thực hiện tách nước Y
trong H<sub>2</sub>SO<sub>4</sub> đặc ở 140oC thu được hỗn hợp Z. Trong Z tổng khối lượng của các ete là 8,04 gam (hiệu suất ete
hóa của các ancol đều là 60%). Cô cạn dung dịch X được 53,0 gam chất rắn. Nung chất rắn này với CaO cho
đến khi phản ứng xảy ra hoàn toàn, thu được 6,72 lít hỗn hợp khí T (đktc). Cho các phát biểu sau:
			<br>(1) Chất F tham gia phản ứng tráng bạc            (2) Khối lượng của E trong hỗn hợp là 8,6 gam
			<br>(3) Khối lượng khí T là 2,55 gam                      (4) Tổng số nguyên tử trong F là 12
			<br>(5) Trong Z có chứa ancol propylic
			<br>Số phát biểu đúng là:$
				4$
				5$
				3$
				~2^
			<!--Câu 30-->Cho 4,48 lít khí CO<sub>2</sub> (ở đktc) hấp thụ hết vào 100 ml dung dịch chứa hỗn hợp NaOH 1M và Ba(OH)<sub>2</sub>
1M, thu được m gam kết tủa. Giá trị của m là:$
				39,4$
				7,88$
				3,94$
				~19,70^
			<!--Câu 31--> Cho m gam Mg vào dung dịch chứa 0,1 mol AgNO3 và 0,25 mol Cu(NO3)<sub>2</sub>, sau một thời gian thu
được 19,44 gam kết tủa và dung dịch X chứa 2 muối. Tách lấy kết tủa, thêm tiếp 8,4 gam bột sắt vào dung dịch
X, sau khi các phản ứng hoàn toàn thu được 9,36 gam kết tủa. Giá trị của m là:$
				4,8$
				~4,64$
				5,28$
				4,32^
			<!--Câu 32-->Cho 2,81 gam hỗn hợp A gồm 3 oxit Fe<sub>2</sub>O3, MgO, ZnO tan vừa đủ trong 300 ml dung dịch H<sub>2</sub>SO<sub>4</sub>
0,1M thì khối lượng hỗn hợp các muối sunfat khan tạo ra là:$
				~5,21 gam$
				4,81 gam$
				4,8 gam$
				3,81gam^
			<!--Câu 33-->Cho các phát biểu sau:
			<br>(a) Đipeptit Gly-Ala có phản ứng màu biure.
			<br>(b) Dung dịch axit glutamic đổi màu quỳ tím thành xanh
			<br>(c) Metyl fomat và glucozơ có cùng công thức đơn giản nhất
			<br>(d) Metylamin có lực bazơ mạnh hơn amoniac
			<br>(e) Saccarozơ có phản ứng thủy phân trong môi trường axit.
			<br>(g) Metyl metacrylat làm mất màu dung dịch brom
			<br>Số phát biểu đúng là:$
				~4$
				6$
				5$
				3^
			<!--Câu 34-->Hai chất nào sau đây đều là lưỡng tính?$
				Ca(OH)<sub>2</sub> và Cr(OH)<sub>3</sub>$
				~Zn (OH)<sub>2</sub> và Al(OH)<sub>3</sub>$
				Ba(OH)<sub>2</sub> và Fe(OH)<sub>3</sub>$
				NaOH và Al(OH)<sub>3</sub>^
			<!--Câu 35-->Dẫn luồng khí CO dư qua hỗn hợp CuO, Al<sub>2</sub>O<sub>3</sub>, CaO, MgO có số mol bằng nhau (nung nóng ở nhiệt
độ cao) thu được chất rắn A. Hòa tan A vào nước dư còn lại chất rắn X. X gồm:$
				Cu, Mg.$
				Cu, Mg, Al<sub>2</sub>O<sub>3</sub>.$
				Cu, Al<sub>2</sub>O<sub>3</sub>, MgO.$
				~Cu, MgO.^
			<!--Câu 36-->Thủy phân hoàn toàn m gam hỗn hợp gồm peptit X và peptit Y (đều mạch hở) bằng dung dịch NaOH
vừa đủ thu được 151,2 gam hỗn hợp A gồm các muối natri của Gly, Ala và Val. Mặt khác, để đốt cháy hoàn
toàn m gam hỗn hợp X, Y ở trên cần 107,52 lít khí O<sub>2</sub> (đktc) và thu được 64,8 gam H<sub>2</sub>O.Tổng số mol của 3 muối
trong hỗn hợp A gần nhất:$
				~1,5$
				1,2$
				0,5$
				2,1^
			<!--Câu 37--> Cho các cặp chất sau:
			<br>(1). Khí Br<sub>2</sub> và khí O<sub>2</sub>
			<br>(2). Khí H<sub>2</sub>S và dung dịch FeCl<sub>3</sub>.
			<br>(3). Khí H<sub>2</sub>S và dung dịch Pb(NO<sub>3</sub>)<sub>2</sub>.
			<br>(4). CuS và dung dịch HCl.
			<br>(5) Si và dung dịch NaOH loãng
			<br>(6). Dung dịch KMnO<sub>4</sub> và khí SO<sub>2</sub>.
			<br>(7). Hg và S.
			<br>(8). Khí Cl<sub>2</sub> và dung dịch NaOH.
			<br>Số cặp chất xảy ra phản ứng hóa học ở nhiệt độ thường là:$
				6$
				8$
				~5$
				7^
			<!--Câu 38-->Chất X có công thức cấu tạo CH<sub>3</sub>CH<sub>2</sub>COOCH<sub>3</sub>. Tên gọi của X là:$
				~metyl propionat$
				metyl axetat$
				propyl axetat$
				etyl axetat^
			<!--Câu 39-->Cho hỗn hợp gồm 1,68 gam Fe và 2,88 gam Cu vào 400 mL dung dịch chứa hỗn hợp gồm
H<sub>2</sub>SO<sub>4</sub> 0,75M và NaNO<sub>3</sub> 0,3M. Sau khi các phản ứng xảy ra hoàn toàn, thu được dung dịch X và khí NO (sản
phẩm khử duy nhất). Cho V (mL) dung dịch NaOH 1,0M vào dung dịch X thì lượng kết tủa thu được là lớn
nhất. Giá trị tối thiểu của V là:$
				~540$
				240$
				420$
				360^
			<!--Câu 40--> Cho 10,41 gam hỗn hợp gồm Cu, Al tác dụng hoàn toàn với dung dịch HNO<sub>3</sub> dư, thu được dung dịch
Y và 2,912 lít khí NO (đktc) là sản phẩm khử duy nhất. Khối lượng muối trong Y là:$
				11,52$
				10,67$
				~34,59$
				37,59
			</div>
			<div id="countDown">	
				<p>Thời gian còn lại:</p>
				<p class="minute">Phút</p>
				<p class="second">Giây<p>
				
			<div>
			<script type="text/javascript">
				function getCookie(name){
					var cname = name + "=";
					var dc = document.cookie;
					if (dc.length > 0) {
						begin = dc.indexOf(cname);
						if (begin != -1) {
							begin += cname.length;
							end = dc.indexOf(";", begin);
						if (end == -1) end = dc.length;
						return unescape(dc.substring(begin, end));
						}
					}
					return null;
				}
				if(document.cookie && document.cookie.match('myClock_Hoa1')){
				  // get deadline value from cookie
				  var deadline = getCookie('myClock_Hoa1');
				}
				 
				
				else{
				  // create deadline 50 minutes from now
					var currentTime = Date.parse(new Date());
					var deadline = Date.parse(new Date(currentTime + 50*60*1000));
				 
				  // store deadline in cookie for future reference
				  document.cookie = 'myClock_Hoa1=' + deadline + '; path=/';
				}
							
			
			
				
				var currentTime = Date.parse(new Date());
				
				var t=deadline-currentTime;
				var second = Math.floor((t / 1000) % 60);
				var minute = Math.floor(t / 1000 / 60);
				
				
				
			
				var nd = $("#noi_dung_de").html();
				var cau_hoi = nd.split('^');
				var html = ""; var tra_loi_dung = 0;
				for(var i = 0;i<cau_hoi.length;i++)
				{
					var chi_tiet = cau_hoi[i].split('$');
					//alert(chi_tiet[i]);
					for(var j=0;j<chi_tiet.length;j++)
					{
			 
						if (j==0) html += "<tr><td><b>Câu " + (i+1) + ":</b></td><td> <b>" + chi_tiet[j].trim() + "</b></td></tr>";
						else 
						{
							html += "<tr><td></td><td id='" + (i + 1 + String.fromCharCode(64 + j))+"'><input type='radio' name='" + (i + 1 )+ "' id='" + (i + 1 + String.fromCharCode(64 + j))+"' value='" + chi_tiet[j].trim() +"'> " + String.fromCharCode(64 + j) + ". " + chi_tiet[j].replace("~","").trim() + "</td></tr>";
						}
					}
				}

				$("#noi_dung_de").empty().append("<table>" + html + "</table><input type='submit' value='Nộp bài' id='tra_loi_xong' style='margin-left:585px;'>").fadeIn();
				$("#noi_dung_de input").click(function(){   
				//Lấy id của radio
				var id = $(this).attr("id");
				for (var j=1;j<5;j++){
					var newid=id.substr(0,1)+String.fromCharCode(64 + j);
					
					$("td#" + newid).css("background-color","");
				}
				$("td#" + id).css("background-color","yellow");
				
				});   
				
2				
				
3				
4				
			
				
				/*jQuery(document).ready(function($) {
					
					setTimeout(function(){
						
						$('#tra_loi_xong').trigger( "click" ) ;
						}, time);
				}); */
				
				
				
			
			
				$("#tra_loi_xong").click(function(){
					$('#noi_dung_de input').each(function () {
					var id = $(this).attr("id");
					var ctl = $(this).val();
				 
					//Hiển thị câu đúng với nền là màu đỏ
					if (ctl[0] == '~') 
					{
						//alert($("td#" + id).css("background-color"));
						if ($("td#" + id).css("background-color") == "rgb(255, 255, 0)")
						{
							tra_loi_dung++;
						}
						else $("td#" + id).css("background-color","red");     
					}
				});
				$("#noi_dung_de").append("<p style='text-align:center'>Số câu đúng :" + (tra_loi_dung < 0 ? "0" : tra_loi_dung) + " câu. Điểm : " + tra_loi_dung*0.25+ "</p>");
				$("#tra_loi_xong").fadeOut();
				
				});
				
				var timer = setInterval(function() {
					
				   $('.minute').text(minute +" Phút")
				   $('.second').text(second-- +" Giây");
				   if (second == -1) {
						minute--;
						second=60;
				   }
				   if (minute ==-1)  {
						 alert("Đã hết giờ làm bài, kiểm tra kết quả");
					  $('#tra_loi_xong').trigger( "click" ) ;
					  $('.second').fadeOut("slow");
					  $('.minute').fadeOut("slow");
					  clearInterval(timer);
				   }
				   
				}, 1000);
				
			</script>
			
		</div>
	</body>
</html>