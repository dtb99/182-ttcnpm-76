<?php
include 'UserID.php';
$user = new UserID();
?>
<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8"/>
    <title>Luyện Thi Đại Học</title>
    <link rel="stylesheet" type="text/css" href="style.css"/>

</head>
<body>
<div id="page">
    <div id="header">
        <div id="section">
            <?php if($user->isLogin()){ ?>
                <p> Hello <?= $user->getSESSION("firstname")." ".$user->getSESSION("name") ?> <a href="logout.php">Logout</a></p>

            <?php } ?>
        </div>
        <?php if(!$user->isLogin()){ ?>
            <div id="section">
                <a id="login" href="login.php"><button>ĐĂNG NHẬP</button></a>
                <a id="creat" href="register.php"><button>ĐĂNG KÍ</button></a>
            </div>
        <?php } ?>
        <ul>
            <li><a href="home.php">Trang chủ</a></li>
            <li><a href="testExam.php">THI THỬ</a></li>
            <li><a href="documents.php">TÀI LIỆU</a></li>
            <li class="current"><a href="news.php">TIN TỨC</a></li>
            <?php if ($user->isLogin()) {
                if ($user->getSESSION("username") == "admin") {
                    ?>
                    <li><a href="Search.php">TÌM KIẾM USERS</a></li>
                    <li><a href="upFile.php">TẢI LÊN DỮ LIỆU</a></li>
                <?php } } ?>
        </ul>
    </div>
    <div id="content">
        <div class="container">

            <div class="tab">
                <button class="tablinks active" style="margin-left:290px">Lịch thi các môn</button>
                <button class="tablinks">Cách tính điểm tốt nghiệp</button>
                <button class="tablinks">5 điểm mới</button>
            </div>

            <div id="Lịch thi các môn" class="tabcontent">
                <h1>Lịch thi THPT Quốc gia năm 2019 chính thức từ Bộ Giáo Dục</h1>
                <strong>Ngày 28/3, Bộ GD&ĐT vừa công bố hướng dẫn kỳ thi và xét công nhận tốt nghiệp THPT quốc gia
                    2019.</strong></br>
                <p>
                    Theo đó, học sinh thi THPT quốc gia làm 3 bài thi độc lập gồm Toán, Ngữ Văn, Ngoại ngữ; 2 bài thi tổ
                    hợp là Khoa học Tự nhiên (các môn Vật Lý, Hóa học, Sinh học) và Khoa học Xã hội (Lịch sử, Địa Lý,
                    Giáo dục Công dân).
                </p>
                <img src="images/lichthi.jpg">
                <p>
                    Như vậy, kỳ thi THPT quốc gia 2019 diễn ra trong 3 ngày, từ 25 đến 27/6, như năm 2018. Độ phân hóa
                    của đề thi nằm chủ yếu lớp 12. Thí sinh có thể yên tâm, đề thi tham khảo vừa công bố có giá trị tham
                    khảo, định hướng rất tốt cho việc dạy và học của giáo viên, học sinh trong nhà trường.
                </p>
                <p>
                    Kỳ thi THPT Quốc gia năm 2018, tổ chức thi 5 bài thi, gồm 3 bài thi độc lập là: Toán, Ngữ văn, Ngoại
                    ngữ và 2 bài thi tổ hợp là Khoa học Tự nhiên (tổ hợp các môn Vật lí, Hóa học, Sinh học; viết tắt là
                    KHTN), Khoa học Xã hội (tổ hợp các môn Lịch sử, Địa lí, Giáo dục công dân đối với thí sinh học
                    chương trình Giáo dục THPT; tổ hợp các môn Lịch sử, Địa lí đối với thí sinh học chương trình GDTX
                    cấp THPT; viết tắt là KHXH).
                </p>
                <p>
                    Để xét công nhận tốt nghiệp THPT, thí sinh học chương trình Giáo dục THPT (gọi tắt là thí sinh Giáo
                    dục THPT) phải dự thi 4 bài thi, gồm 3 bài thi độc là Toán, Ngữ văn, Ngoại ngữ và 1 bài thi do thí
                    sinh tự chọn trong số 2 bài thi tổ hợp; thí sinh học chương trình GDTX cấp THPT (gọi tắt là thí sinh
                    GDTX) phải dự thi 3 bài thi, gồm 2 bài thi độc lập là Toán, Ngữ văn và 1 bài thi do thí sinh tự chọn
                    trong số 2 bài thi tổ hợp. Để tăng cơ hội xét tuyển sinh ĐH, CĐ theo quy định của Quy chế tuyển sinh
                    ĐH, CĐ hệ chính quy hiện hành, thí sinh được chọn dự thi cả 2 bài thi tổ hợp, điểm bài thi tổ hợp
                    nào cao hơn sẽ được chọn để tính điểm xét công nhận tốt nghiệp THPT.
                </p>
                <p id="nguon">Nguồn: news.zing.vn</p>
            </div>

            <div id="Cách tính điểm tốt nghiệp" class="tabcontent">
                <h1>Cách tính điểm xét tốt nghiệp THPT quốc gia 2019</h1>
                <strong>Dự thảo Bộ GD&ĐT công bố có những điều chỉnh mới, trong đó, cách tính điểm xét tốt nghiệp có
                    nhiều thay đổi so với năm 2018.</strong>
                <p>
                    Điểm xét tốt nghiệp THPT 2019 được tính theo công thức sau:
                </p>
                <img src="images/tinhdiem.jpg">
                <p>
                    Giấy chứng nhận nghề loại giỏi được cộng 2,0 điểm.
                </p>
                <p>
                    Theo dự thảo, học sinh giáo dục THPT, học viên giáo dục thường xuyên (GDTX) trong diện xếp loại hạnh
                    kiểm, học viên GDTX tham gia học đồng thời chương trình trung cấp kết hợp với chương trình văn hóa
                    theo chương trình GDTX cấp THPT có Giấy chứng nhận nghề, Bằng tốt nghiệp trung cấp do Sở GD&ĐT, các
                    cơ sở giáo dục đào tạo và dạy nghề, các cơ sở giáo dục nghề nghiệp cấp trong thời gian học THPT được
                    cộng điểm khuyến khích căn cứ xếp loại ghi trong giấy chứng nhận nghề, bằng tốt nghiệp trung cấp như
                    sau:
                </p>
                <p>
                    - Loại giỏi đối với giấy chứng nhận nghề, loại xuất sắc và giỏi đối với bằng trung cấp: Cộng 2,0
                    điểm.
                </p>
                <p>
                    - Loại khá đối với giấy chứng nhận nghề, loại khá và trung bình khá đối với bằng trung cấp: Cộng 1,5
                    điểm.
                </p>
                <p>
                    - Loại trung bình: Cộng 1,0 điểm.
                </p>
                <p>
                    Kỳ thi THPT quốc gia 2019 tổ chức thi 5 bài, gồm 3 bài thi độc lập là: Toán, Ngữ văn, Ngoại ngữ và 2
                    bài thi tổ hợp là Khoa học Tự nhiên (tổ hợp các môn Vật lý, Hóa học, Sinh học), Khoa học Xã hội (tổ
                    hợp các môn Lịch sử, Địa lý, Giáo dục Công dân đối với thí sinh học chương trình giáo dục THPT; tổ
                    hợp các môn Lịch sử, Địa lý đối với thí sinh học chương trình GDTX cấp THPT).
                </p>
                <p id="nguon">
                    Nguồn: news.zing.vn
                </p>
            </div>

            <div id="5 điểm mới" class="tabcontent">
                <h1>Ngày 4/12, Bộ GD&ĐT đã công bố thông tin mới nhất về kỳ thi THPT quốc gia 2019. Theo đó, có 5 điểm
                    mới quan trọng thay đổi so với năm trước.</h1>
                <strong>Nội dung đề thi chủ yếu nằm trong chương trình lớp 12</strong>
                <p>
                    Nội dung đề thi nằm trong chương trình cấp THPT, chủ yếu là chương trình lớp 12; đảm bảo ngưỡng cơ
                    bản để xét tốt nghiệp THPT và có độ phân hóa phù hợp để các cơ sở giáo dục đại học, giáo dục nghề
                    nghiệp làm cơ sở cho tuyển sinh.
                </p>
                <p>
                    Bộ GDĐT sẽ sớm công bố đề thi tham khảo Kỳ thi THPT quốc gia 2019 để giúp giáo viên, học sinh tổ
                    chức dạy học, ôn tập chuẩn bị tham gia Kỳ thi.
                </p>
                <strong>Lắp camera giám sát</strong>
                <p>
                    Quy định chặt chẽ về sắp xếp phòng thi, nhất là đối với các thí sinh tự do; hướng dẫn chi tiết kỹ
                    thuật niêm phong, lưu trữ, bảo quản bài thi, đề thi để tăng cường bảo mật; đặt camera giám sát phòng
                    chứa tủ đựng đề thi, bài thi 24 giờ/ngày và tăng cường trách nhiệm của các đối tượng có liên quan
                    trong bảo quản đề thi, bài thi tại Điểm thi, Hội đồng thi.
                </p>
                <strong>Bộ GDĐT trực tiếp chỉ đạo tổ chức chấm bài thi trắc nghiệm</strong>
                <p>
                    Bộ GDĐT trực tiếp chỉ đạo tổ chức chấm bài thi trắc nghiệm, giao nhiệm vụ cho các trường ĐH chủ trì,
                    đặt camera giám sát phòng chấm thi 24/24 giờ.
                </p>
                <p>
                    Sửa đổi, nâng cấp, hoàn thiện phần mềm chấm thi trắc nghiệm theo hướng tăng cường tính bảo mật và
                    chức năng giám sát để ngăn ngừa các can thiệp trái phép.
                </p>
                <p>
                    Theo đó, mã hóa dữ liệu tạo ra trong quá trình xử lý bài thi trắc nghiệm để tránh người dùng can
                    thiệp và đảm bảo trong suốt quá trình xử lý bài thi, cán bộ xử lý bài thi không thể có được thông
                    tin về mối liên hệ giữa thông tin cá nhân của thí sinh với phần nội dung trả lời trắc nghiệm (đây là
                    một hình thức “đánh phách” điện tử Phiếu trả lời trắc nghiệm của thí sinh).
                </p>
                <p>
                    Đồng thời, tăng cường thanh tra, kiểm tra, giám sát của Bộ GDĐT và của các trường ĐH, CĐ đối với
                    việc chấm bài thi tự luận (môn Ngữ văn) do sở GDĐT chủ trì.
                </p>
                <strong>Điểm xét tốt nghiệp THPT gồm 70% điểm trung bình các bài thi THPT quốc gia</strong>
                <p>
                    Bộ GDĐT công bố công khai, rộng rãi thông tin tổng hợp phân tích kết quả thi trước khi công bố kết
                    quả thi.
                </p>
                <p>
                    Tăng tỷ lệ kết quả thi trong xét công nhận tốt nghiệp THPT. Dự kiến, điểm xét tốt nghiệp THPT gồm
                    70% điểm trung bình các bài thi THPT quốc gia dùng để xét tốt nghiệp THPT + 30% điểm trung bình cả
                    năm lớp 12 của thí sinh + điểm ưu tiên, khuyến khích (nếu có).
                </p>
                <p>
                    Tăng tính tự chủ của các cơ sở giáo dục đại học, giáo dục nghề nghiệp trong tuyển sinh ĐH, CĐ. Cụ
                    thể: các trường chủ động xây dựng và công bố đề án tuyển sinh đảm bảo nguyên tắc tự chủ; theo đó,
                    ngoài phương thức sử dụng kết quả Kỳ thi THPT quốc gia làm cơ sở tuyển sinh, có thể sử dụng các
                    phương thức khác để tuyển sinh.
                </p>
                <strong>Phối hợp với công an tập huấn kỹ về nghiệp vụ tổ chức thi</strong>
                <p>
                    Tăng cường công tác thanh tra, kiểm tra, giám sát thực hiện các khâu tổ chức Kỳ thi; yêu cầu các cơ
                    sở giáo dục thực hiện nghiêm túc việc lựa chọn, phân công cán bộ tham gia tổ chức thi theo đúng quy
                    định của quy chế đảm bảo có đội ngũ cán bộ đủ năng lực và có ý thức trách nhiệm cao tham gia Kỳ thi;
                </p>
                <p>
                    Phối hợp với các cơ quan, nhất là cơ quan Công an để tập huấn kỹ về nghiệp vụ tổ chức thi cũng như
                    kỹ năng phòng chống, phát hiện gian lận, nhất là gian lận sử dụng công nghệ cao.
                </p>
                <p id="nguon">Nguồn: flss.vnu.edu.vn</p>
            </div>
        </div>

        <script type="text/javascript">
            var buttons = document.getElementsByClassName('tablinks');
            var contents = document.getElementsByClassName('tabcontent');

            function showContent(id) {
                for (var i = 0; i < contents.length; i++) {
                    contents[i].style.display = 'none';
                }
                var content = document.getElementById(id);
                content.style.display = 'block';
            }

            for (var i = 0; i < buttons.length; i++) {
                buttons[i].addEventListener("click", function () {
                    var id = this.textContent;
                    for (var i = 0; i < buttons.length; i++) {
                        buttons[i].classList.remove("active");
                    }
                    this.className += " active";
                    showContent(id);
                });
            }
            showContent('Lịch thi các môn');
        </script>


    </div>
    <div id="footer">
        <div>
            <div id="connect">
                <a href="https://www.facebook.com/groups/644703785954707/?multi_permalinks=645737145851371&notif_id=1555690841219669&notif_t=group_activity"
                   target="_blank"><img src="images/icon-facebook.gif" alt="Facebook"/></a>
            </div>
            <div id="contact">
                <p>0333 879 987 || nhan.tran1245@hcmut.edu.vn</p>
            </div>
        </div>
    </div>
</div>


</body>
</html>