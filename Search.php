﻿<!DOCTYPE html>
<?php
include_once 'router.php';
include_once 'UserID.php';
$id = new UserID();
$r = new router();
if ($id->isLogin()) {
    if ($id->getSESSION("username") !== "admin") header('Location:home.php');
} else "Page not found";
$conn = new PDO('mysql:host=' . "localhost" . ';dbname=' . "1162803", "1162803", "Bien20031999");
$text = trim($r->getPOST('text'));
$stmt = $conn->prepare('SELECT ID, username,firstname, name, email from users WHERE name = :name');
$stmt->setFetchMode(PDO::FETCH_ASSOC);
?>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>Tìm kiếm users</title>
    <link rel="stylesheet" type="text/css" href="style.css"/>

</head>
<body>

<div id="page">
    <div id="header">
        <div id="section">
            <p> Hello ADMIN <a href="logout.php">Logout</a></p>
        </div>
        <div id="section">
            <p></p>
        </div>
	<ul>
        <li><a href="home.php">Trang chủ</a></li>
        <li><a href="testExam.php">THI THỬ</a></li>
        <li><a href="documents.php">TÀI LIỆU</a></li>
        <li><a href="news.php">TIN TỨC</a></li>
		<li class="current"><a  href="Search.php">TÌM KIẾM USERS</a></li>
		<li><a href="upFile.php">TẢI LÊN TÀI LIỆU</a></li>
				
	</ul>	
    </div>
    <div id="content">
        <div class="container">
            <form class="searchform" acction="Search.php" method="POST">
                <input type="text" placeholder="Nhập tên cần tìm kiếm" name="text">
                <input type="submit" name="submit" value="Search" >
            </form>
            <?php if ($r->getPOST("submit") && $text) {
            $id->name = $text;
            $stmt->execute(array('name' => $text));
            if ($id->isName()) {
            ?>
            <div id="result">
                <table cellspacing="15px" border="0">
                    <tr>
                        <td>ID</td>
                        <td>Username</td>
                        <td>Họ và chữ lót</td>
                        <td>Tên</td>
                        <td>Email</td>
                        <td>Sửa</td>
                        <td>Xóa</td>
                    </tr>


                    <?php
                    while ($row = $stmt->fetch()) {
                        ?>
                        <tr>
                            <td><?php echo $row['ID']; ?></td>
                            <td><?php echo $row['username']; ?></td>
                            <td><?php echo $row['firstname']; ?></td>
                            <td><?php echo $row['name']; ?></td>
                            <td><?php echo $row['email'] . "<br>"; ?></td>
                            <td><a href="Edit.php?id=<?php echo $row['ID']?>"><img src="images/edit.png" height="20px" width="20px"></a></td>
                            <td><a onclick="return confirmDelete()" href="Delete.php?id=<?php echo $row['ID']?>"><img src="images/delete.png" height="20px" width="20px"></a></td>
                        </tr>
                        <?php
                    }
                    } else echo "Not find " . $text;
                    }
                    ?>
                </table>
                <script type="text/javascript">
                    function confirmDelete() {
                        var result = confirm("Xác nhận xóa User ?");
                        if (result == true) {
                            return  true;
                        } else {
                            return false;
                        }


                    }
                </script>
            </div>
        </div>


    </div>
    <div id="footer">
        <div>
            <div id="connect">
                <a href="https://www.facebook.com/groups/644703785954707/?multi_permalinks=645737145851371&notif_id=1555690841219669&notif_t=group_activity"
                   target="_blank"><img src="images/icon-facebook.gif" alt="Facebook"/></a>
            </div>
            <div id="contact">
                <p>0333 879 987 || nhan.tran1245@hcmut.edu.vn</p>
            </div>
        </div>
    </div>
</div>

</body>
</html>