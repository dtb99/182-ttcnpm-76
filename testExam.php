<!DOCTYPE html>
<?php
include 'UserID.php';
$user = new UserID();
if(!$user->isLogin()) header('Location:login.php');
?>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Luyện Thi Đại Học</title>
	<link rel="stylesheet" type="text/css" href="style.css" />
	
</head>
<body>
	<div id="page">
		<div id="header">
            <div id="section">
                <p> Hello <?= $user->getSESSION("firstname")." ".$user->getSESSION("name") ?> <a href="logout.php">Logout</a></p>
            </div>
			<ul>
				<li><a href="home.php">TRANG CHỦ</a></li>
				<li class="current"><a href="testExam.php">THI THỬ</a></li>
				<li><a href="documents.php">TÀI LIỆU</a><li>
				<li><a href="news.php">TIN TỨC</a></li>
                <?php if ($user->isLogin()) {
                    if ($user->getSESSION("username") == "admin") {
                        ?>
                        <li><a href="Search.php">TÌM KIẾM USERS</a></li>
                        <li><a href="upFile.php">TẢI LÊN DỮ LIỆU</a></li>
                    <?php } } ?>
			</ul>	
		</div>
		<div id="content">
			<div class="container">
				<div class="tab">
					<button class="tablinks active" style="margin-left:215px">Toán</button>
					<button class="tablinks">Lý</button>
					<button class="tablinks">Hóa</button>
					<button class="tablinks">Sinh</button>
					<button class="tablinks">Ngữ Văn</button>
					<button class="tablinks">Tiếng Anh</button>
					<button class="tablinks">Sử</button>
					<button class="tablinks">Địa</button>
					<button class="tablinks">GDCD</button>
				</div>
				<div id="Toán" class="tabcontent">
					<p id="note">Click vào đề để thi thử</p>
					<a href="Toan_1.php" target="_blank">
						<div id="file">
							<img src="images/icon-exam.png">
							<p>Đề 1</p>
						</div>
					</a>
					<a href="Toan_2.php" target="_blank">
						<div id="file">
							<img src="images/icon-exam.png">
							<p>Đề 2</p>
						</div>
					</a>
					<a href="Toan_3.php" target="_blank">
						<div id="file">
							<img src="images/icon-exam.png">
							<p>Đề 3</p>
						</div>
					</a>
					
				</div>
				<div id="Lý" class="tabcontent">
					<p id="note">Click vào đề để thi thử</p>
					<a href="Vatly_1.php" target="_blank">
						<div id="file">
							<img src="images/icon-exam.png">
							<p>Đề 1</p>
						</div>
					</a>
					<a href="Vatly_2.php" target="_blank">
						<div id="file">
							<img src="images/icon-exam.png">
							<p>Đề 2</p>
						</div>
					</a>
					<a href="Vatly_3.php" target="_blank">
						<div id="file">
							<img src="images/icon-exam.png">
							<p>Đề 3</p>
						</div>
					</a>
				</div>
				<div id="Hóa" class="tabcontent">
					<p id="note">Click vào đề để thi thử</p>
					<a href="Hoa_1.php" target="_blank">
						<div id="file">
							<img src="images/icon-exam.png">
							<p>Đề 1</p>
						</div>
					</a>
				</div>
				<div id="Sinh" class="tabcontent">
					<p id="note">Click vào đề để thi thử</p>
					<a href="Sinh_1.php" target="_blank">
						<div id="file">
							<img src="images/icon-exam.png">
							<p>Đề 1</p>
						</div>
					</a>
				</div>
				<div id="Ngữ Văn" class="tabcontent">
					<p id="note">Click vào đề để thi thử</p>
				</div>
				<div id="Tiếng Anh" class="tabcontent">
					<p id="note">Click vào đề để thi thử</p>
					<a href="AnhVan_1.php" target="_blank">
						<div id="file">
							<img src="images/icon-exam.png">
							<p>Đề 1</p>
						</div>
					</a>
				</div>
				<div id="Sử" class="tabcontent">
					<p id="note">Click vào đề để thi thử</p>
					<a href="Su_1.php" target="_blank">
						<div id="file">
							<img src="images/icon-exam.png">
							<p>Đề 1</p>
						</div>
					</a>
				</div>
				
				<div id="Địa" class="tabcontent">
					<p id="note">Click vào đề để thi thử</p>
					<a href="DiaLy_1.php" target="_blank">
						<div id="file">
							<img src="images/icon-exam.png">
							<p>Đề 1</p>
						</div>
					</a>
				</div>
				<div id="GDCD" class="tabcontent">
					<p id="note">Click vào đề để thi thử</p>
					<a href="GDCD_1.php" target="_blank">
						<div id="file">
							<img src="images/icon-exam.png">
							<p>Đề 1</p>
						</div>
					</a>
					<a href="GDCD_2.php" target="_blank">
						<div id="file">
							<img src="images/icon-exam.png">
							<p>Đề 2</p>
						</div>
					</a>
				</div>
				
				<script type="text/javascript">
					var buttons = document.getElementsByClassName('tablinks');
					var contents = document.getElementsByClassName('tabcontent');
					function showContent(id){
						for (var i = 0; i < contents.length; i++) {
							contents[i].style.display = 'none';
						}
						var content = document.getElementById(id);
						content.style.display = 'block';
					}
					for (var i = 0; i < buttons.length; i++) {
						buttons[i].addEventListener("click", function(){
						var id = this.textContent;
						for (var i = 0; i < buttons.length; i++) {
						buttons[i].classList.remove("active");
						}
						this.className += " active";
						showContent(id);
						});
					}
					showContent('Toán');
				</script>
			</div>
		</div>
		<div id="footer">
			<div>
				<div id="connect">
					<a href="https://www.facebook.com/groups/644703785954707/?multi_permalinks=645737145851371&notif_id=1555690841219669&notif_t=group_activity" target="_blank"><img src="images/icon-facebook.gif" alt="Facebook"/></a>
				</div>
				<div id="contact">
					<p>0333 879 987 || nhan.tran1245@hcmut.edu.vn</p>
				</div>
			</div>
		</div>	
	</div>

</body>
</html>