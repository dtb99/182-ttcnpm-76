﻿<?php
include 'UserID.php';
$user = new UserID();
$i = 0;
(int)$i;
$filelist = array();
$handle = opendir("Doc");
while (($entry = readdir($handle)) !== false) {
    $filelist[] = $entry;
}
closedir($handle);
?>
<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8"/>
    <title>Luyện Thi Đại Học</title>
    <link rel="stylesheet" type="text/css" href="style.css"/>

</head>
<body>
<div id="page">
    <div id="header">
        <div id="section">
            <?php if ($user->isLogin()) {
                ?>
                <p> Hello <?= $user->getSESSION("firstname")." ".$user->getSESSION("name") ?> <a href="logout.php">Logout</a></p>
            <?php }
            ?>
        </div>
        <ul>
            <li><a href="home.php">Trang chủ</a></li>
            <li><a href="testExam.php">THI THỬ</a></li>
            <li class="current"><a href="documents.php">TÀI LIỆU</a></li>
            <li><a href="news.php">TIN TỨC</a></li>
            <?php if ($user->isLogin()) {
                if ($user->getSESSION("username") == "admin") {
                    ?>
                    <li><a href="Search.php">TÌM KIẾM USERS</a></li>
                    <li><a href="upFile.php">TẢI LÊN DỮ LIỆU</a></li>
                <?php } } ?>
        </ul>
    </div>
    <div id="content">
        <div class="container">
            <div id="result">
                <table cellspacing="15px" border="0">
                    <tr>
                        <td><p style="font-weight:bold">Filename</p></td>
                        <td><p style="font-weight:bold">Link</p></td>
                    </tr>
                    <?php
                    while ($i < count($filelist)) {
                        if($filelist[$i] !== "." && $filelist[$i] !== "..") {
                            ?>
                            <tr>
                                <td><?php echo $filelist[$i]; ?></td>
                                <td><a href="download.php?file=<?php echo $filelist[$i]; ?>"><img
                                                src="images/download.jpg"
                                                height="20px"
                                                width="20px"></a></td>

                            </tr>
                            <?php
                        }
                        $i++;
                    }
                    ?>
                </table>
            </div>

        </div>
    </div>
    <div id="footer">
        <div>
            <div id="connect">
                <a href="https://www.facebook.com/groups/644703785954707/?multi_permalinks=645737145851371&notif_id=1555690841219669&notif_t=group_activity"
                   target="_blank"><img src="images/icon-facebook.gif" alt="Facebook"/></a>
            </div>
            <div id="contact">
                <p>0333 879 987 || nhan.tran1245@hcmut.edu.vn</p>
            </div>
        </div>
    </div>
</div>

</body>
</html>