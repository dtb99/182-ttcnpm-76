<!DOCTYPE html>
<?php
include 'UserID.php';
$user = new UserID();
if(!$user->isLogin()) header('Location:login.php');
?>
<html>
	<head>
		<meta charset="UTF-8" />
		<title>Đề THPTQG môn Sinh học</title>
		<link rel="stylesheet" type="text/css" href="examstyle.css" />
		
		
	</head>
	<body>
		<h1>Đề thi thử THPT Quốc Gia 2019</h1>
		<p align="center"> <b> Môn:Sinh học </b> </p>
		<p id="time">Thời gian làm bài: 50 phút</p>		
		<script src='examform.js'></script>
		<div id="content">
			<div id="noi_dung_de" style='display:none;'>
				
			<!--Câu 1--> Ở ruồi giấm, xét 3 gen A, B, D quy định 3 tính trạng khác nhau và alen trội là trội hoàn toàn. Phép lai
P: ♀ <img src="https://latex.codecogs.com/gif.latex?\frac{AB}{ab}Dd" title="\frac{AB}{ab}Dd" /> × ♂ <img src="https://latex.codecogs.com/gif.latex?\frac{AB}{ab}Dd" title="\frac{AB}{ab}Dd" /> thu được F1 có tỉ lệ kiểu hình lặn về cả 3 tính trạng chiếm tỉ lệ 4%. Có bao nhiêu
dự đoán sau đây là đúng với kết quả ở F1? 
			<br>(1). Có 21 loại kiểu gen và 8 loại kiểu hình.
			<br>(2). Kiểu hình có 2 trong 3 tính trạng trội chiếm tỉ lệ 30%.
			<br>(3). Tần số hoán vị gen là 36%.
			<br>(4). Tỉ lệ kiểu hình mang 1 trong 3 tính trạng trội chiếm 16,5%.
			<br>(5). Kiểu gen dị hợp về 3 cặp gen chiếm tỉ lệ 16%.
			<br>(6). Xác suất để 1 cá thể A-B-D- có kiểu gen thuần chủng là 8/99.
			$
				5$
				4$
				3$
				~6@
			<!--Câu 2-->Giả sử trong quần thể của một loài động vật phát sinh một đột biến lặn, trường hợp nào sau đây đột
biến sẽ nhanh chóng trở thành nguyên liệu cho chọn lọc tự nhiên?$
				 Đột biến xuất hiện ở loài sinh sản hữu tính, các cá thể giao phối có lựa chọn.$
				 Đột biến xuất hiện ở loài sinh sản hữu tính, các cá thể giao phối cận huyết.$
				 ~Đột biến xuất hiện ở quần thể của loài sinh sản hữu tính, các cá thể tự thụ tinh.$
				Đột biến xuất hiện ở loài sinh sản vô tính, cá thể con được sinh ra từ cá thể mẹ.@									
			<!--Câu 3-->Có bao nhiêu kết luận sau đây đúng?
			<br>(1). Liên kết gen làm hạn chế sự xuất hiện biến dị tổ hợp.
			<br>(2). Các cặp gen càng nằm ở vị trí gần nhau thì tần số hoán vị gen càng cao.
			<br>(3). Số lượng gen nhiều hơn số lượng NST nên liên kết gen là phổ biến.
			<br>(4). Hai cặp gen nằm trên 2 cặp NST khác nhau thì không liên kết với nhau.
			<br>(5). Số nhóm gen liên kết bằng số NST đơn có trong tế bào sinh dưỡng.
			$
				5$
				2$
				~3$
				4@
			<!--Câu 4--> Ở gà, gen quy định màu sắc lông nằm trên vùng không tương đồng của nhiễm sắc thể giới tính X có
hai alen: alen A quy định lông vàng trội hoàn toàn so với alen a quy định lông đen. Cho gà trống lông vàng
thuần chủng giao phối với gà mái lông đen thu được F1. Cho F1 giao phối với nhau thu được F2. Xét các kết
luận sau đây về kiểu gen và kiểu hình ở F2.
			<br>(1). Gà trống lông vàng có tỉ lệ gấp đôi gà mái lông đen.
			<br>(2). Gà trống lông vàng có tỉ lệ gấp đôi gà mái lông vàng.
			<br>(3). Tất cả các gà lông đen đều là gàmái.
			<br>(4). Gà lông vàng và gà lông đen có tỉ lệ bằng nhau.
			<br>(5). Có 2 kiểu gen quy định gà trống lông vàng.
			<br>(6) Ở F2 có 4 loại kiểu gen khác nhau.
			<br>Có bao nhiêu kết luận đúng?
			$
				~5$
				4$
				3$
				6@
			<!--Câu 5--> Ở một loài thực vật, tính trạng khối lượng quả do nhiều cặp gen nằm trên các cặp NST khác nhau
di truyền theo kiểu tương tác cộng gộp. Cho cây có quả nặng nhất lai với cây có quả nhẹ nhất được F1.
Cho F1 giao phấn tự do được F2 có 15 loại kiểu hình về tính trạng khối lượng quả. Tính trạng khối lượng
quả do bao nhiêu cặp gen quy định?$
				~Do 7 cặp genquy định.$
				Do 5 cặp gen quy định.$
				Do 8 cặp gen quy định$
				Do 6 cặp gen quy định.@
			<!--Câu 6-->Giả sử thế hệ thứ nhất của một quần thể thực vật ở trạng thái cân bằng di truyền có q(a) =0,2.
p(A)=0,8 . Thế hệ thứ hai của quần thể có cấu trúc 0,72AA : 0,16Aa : 0,12aa. Cấu trúc di truyền của quần
thể ở thể hệ thứ ba sẽ như thế nào? Biết rằng cách thức sinh sản tạo ra thế hệ thứ ba cũng giống như cách
thức sinh sản tạo ra thế hệ thứ hai.$
				0,64AA + 0,32Aa + 0,04aa$
				0,72AA + 0,16Aa + 0,12aa$
				0,78AA + 0,04Aa + 0,18aa$
				~0,76AA + 0,08Aa + 0,16aa@
			<!--Câu 7--> Phương pháp nghiên cứu di truyền người nào dưới đây cho phép phát hiện hội chứng Claiphentơ?$
				 Nghiên cứu trẻ đồng sinh.$
				~Nghiên cứu tế bào.$
				Di truyền hoá sinh.$
				Nghiên cứu phả hệ.@
			<!--Câu 8-->Một loài thực vật, mỗi cặp gen quy định một cặp tính trạng, alen trội là trội hoàn toàn. Cho cây
thân cao, hoa đỏ giao phấn với cây thân thấp, hoa trắng (P), thu được F1 có 100% cây thân cao, hoa đỏ. Cho
F1 giao phấn với nhau, thu được F2 có 4 loại kiểu hình, trong đó cây thân cao, hoa trắng chiếm 16%. Biết
không xảy ra đột biến nhưng có hoán vị gen ở cả đực và cái với tần số bằng nhau. Theo lí thuyết, có bao
nhiêu phát biểu sau đây đúng?
			<br>(1). Nếu cho F1 lai phân tích thì sẽ thu được Fa có 4 kiểu hình, trong đó cây thân cao, hoa trắng chiếm 20%.
			<br>(2).Trong quá trình phát sinh giao tử của cơ thể F1 đã xảy ra hoán vị gen với tần số 40%.
			<br>(3) Lấy ngẫu nhiên một cây thân thấp, hoa đỏ ở F2, xác suất thu được cây thuần chủng là 1/3.
			<br>(4). Lấy ngẫu nhiên một cây thân cao, hoa đỏ ở F2, xác suất thu được cây thuần chủng là 2/7.$
				~2$
				3$
				4$
				1@
			<!--Câu 9-->Trong quá trình phát sinh sự sống trên Trái Đất$
				khi tế bào nguyên thủy được hình thành thì tiến hóa sinh học sẽ kết thúc.$
				các đại phân tử hữu cơ đã được hình thành trong giai đoạn tiến hóa sinh học.$
				các tế bào sơ khai là khởi đầu của giai đoạn tiến hóa tiền sinh học.$
				~các chất hữu cơ đơn giản đã được hình thành trong giai đoạn tiến hóa hóa học.@
			<!--Câu 10-->Phát biểu nào sau đây là không đúng khi nói về quá trình phiên mã của gen trong nhân ở tế bào
nhân thực?$
				 Diễn ra theo nguyên tắc bổ sung: A - U, T – A, X – G, G – X.$
				~mARN được tổng hợp xong tham gia ngay vào quá trình dịch mã tổng hợp protein.$
				Enzim ARN pôlimeraza tổng hợp mARN theo chiều 5’ → 3’.$
				 Chỉ có một mạch của gen tham gia vào quá trình phiên mã tổng hợp mARN.@
			<!--Câu 11-->Giả sử 5 tế bào sinh tinh của cơ thể có kiểu gen <img src="https://latex.codecogs.com/gif.latex?\frac{AB}{ab}" title="\frac{AB}{ab}" />
tiến hành giảm phân bình thường. <br>Theo lí thuyết, có bao nhiêu phát biểu sau đây đúng?
			<br>(1). Nếu cả 5 tế bào đều xảy ra hoán vị gen thì loại giao tử aB chiếm 25%
			<br>(2). Nếu chỉ 2 tế bào xảy ra hoán vị gen thì loại giao tử Ab chiếm 10%.
			<br>(3). Nếu chỉ có 3 tế bào xảy ra hoán vị gen thì sẽ tạo ra 4 loại giao tử với tỉ lệ 7:7:3:3
			<br>(4). Nếu chỉ có 1 tế bào xảy ra hoán vị gen thì sẽ tạo ra 4 loại giao tử với tỉ lệ 4:4:1:1.$
				2$
				1$
				~3$
				4@
			<!--Câu 12--> Khi nói về các yếu tố ngẫu nhiên, kết luận nào sau đây <b>không</b> đúng?$
				~Với quần thể có kích thước càng lớn thì các yếu tố ngẫu nhiên càng dễ làm thay đổi tần số alen của quần thể
và ngược lại.$
				Khi không xảy ra đột biến, không có CLTN, không có di - nhập gen, nếu thành phần kiểu gen và tần số alen
của quần thể có biến đổi thì đó là do tác động của các yếu tố ngẫu nhiên.$
				 Một quần thể đang có kích thước lớn nhưng do các yếu tố thiên tai hoặc bất kì các yếu tố nào khác làm giảm
kích thước của quần thể một cách đáng kể thì những cá thể sống sót có thể có vốn gen khác biệt hẳn với vốn
gen của quần thể ban đầu.$
				Kết quả tác động của các yếu tố ngẫu nhiên thường dẫn tới làm nghèo vốn gen của quần thể, giảm sự đa
dạng di truyền và có thể dẫn tới làm suy thoái quần thể.@
			<!--Câu 13-->Khi cho cây cao, hoa đỏ thuần chủng lai với cây thấp, hoa trắng thuần chủng thu được F1 có 100%
cây cao, hoa đỏ. Các cây F1 giao phấn ngẫu nhiên thu được F2 có tỉ lệ kiểu hình 75% cây cao, hoa đỏ :
25% cây thấp, hoa trắng. Có bao nhiêu dự đoán sau đây là phù hợp với kết quả của phép lai nói trên?
			<br>(1). Có hiện tượng 1 gen quy định 2 tính trạng, trong đó thân cao, hoa đỏ là trội so với thân thấp, hoa trắng.
			<br>(2). Đời F2 chỉ có 3 kiểu gen.
			<br>(3) Nếu cho F1 lai phân tích thì đời con sẽ có tỉ lệ kiểu hình 50% cây cao, hoa đỏ : 50% cây thấp, hoa trắng.
			<br>(4). Có hiện tượng mỗi tính trạng do một cặp gen quy định và di truyền liên kết hoàn toàn.$
				2$
				3$
				1$
				~4@
			<!--Câu 14-->Xét các quá trình sau:
			<b>(1). Tạo cừu Dolly.
			<b>(2).Tạo giống dâu tằm tam bội.
			<b>(3).Tạo giống bông kháng sâu hại.
			<b>(4).Tạo chuột bạch có gen của chuột cống.
			<b>Những quá trình nào thuộc ứng dụng của công nghệ gen?$
				~3,4$
				1,2$
				1,3,4$
				2,3,4@
			<!--Câu 15-->Để tìm hiểu hiện tượng kháng thuốc ở sâu bọ, người ta đã làm thí nghiệm dùng DDT để xử lí các
dòng ruồi giấm được tạo ra trong phòng thí nghiệm. Ngay từ lần xử lí đầu tiên, tỉ lệ sống sót của các dòng đã
rất khác nhau (thay đổi từ 0% đến 100% tuỳ dòng). Kết quả thí nghiệm chứng tỏ khả năng kháng DDT$
				 không liên quan đến đột biến hoặc tổ hợp đột biến đã phát sinh trong quần thể$
				~liên quan đến những đột biến và tổ hợp đột biến phát sinh ngẫu nhiên từ trước.$
				chỉ xuất hiện tạm thời do tác động trực tiếp của DDT.$
				là sự biến đổi đồng loạt để thích ứng trực tiếp với môi trường có DDT.@
			<!--Câu 16-->Bệnh do gen trội trên nhiễm sắc thể X ở người gây ra có đặc điểm di truyền nào sau đây?$
				Mẹ mắc bệnh thì tất cả các con trai đều mắc bệnh.$
				~Bố mắc bệnh thì tất cả các con gái đều mắc bệnh.$
				Bố mẹ không mắc bệnh có thể sinh ra con mắc bệnh$
				Bệnh thường biểu hiện ở nam nhiều hơn nữ.@
			<!--Câu 17--> Hiện nay, một trong những biện pháp ứng dụng liệu pháp gen đang được các nhà khoa học nghiên
cứu nhằm tìm cách chữa trị các bệnh di truyền ở người là$
				loại bỏ ra khỏi cơ thể người bệnh các sản phẩm dịch mã của gen gây bệnh.$
				đưa các prôtêin ức chế vào trong cơ thể người để ức chế hoạt động của gen gây bệnh.$
				làm biến đổi các gen gây bệnh trong cơ thể thành các gen lành.$
				~bổ sung gen lành vào cơ thể người bệnh.@
			<!--Câu 18-->Chất cônxixin thường được dùng để gây đột biến đa bội ở thực vật, do cônxixin có khả năng$
				kích thích cơ quan sinh dưỡng phát triển.$
				tăng cường sự trao đổi chất ở tế bào.$
				tăng cường quá trình sinh tổng hợp chất hữu cơ$
				~cản trở sự hình thành thoi phân bào làm cho nhiễm sắc thể không phân li.@
			<!--Câu 19-->Sự kiện nào sau đây sau đây có nội dung không đúng với quá trình nhân đôi ADN ở tế bào nhân
thực?$
				 Trong mỗi phân tử ADN được tạo thành thì một mạch là mới được tổng hợp, còn mạch kia là của ADN ban
đầu (nguyên tắc bán bảo toàn).$
				~Vì enzim ADN–pôlimeraza chỉ tổng hợp mạch mới theo chiều 5’–3’, nên trên mạch khuôn 5’-3’ mạch mới
được tổng hợp liên tục, còn trên mạch khuôn 3’– 5’ mạch mới được tổng hợp ngắt quãng tạo nên các đoạn ngắn
rồi được nối lại nhờ enzim nối.$
				Nhờ các enzim tháo xoắn, hai mạch đơn của phân tử ADN tách dần tạo nên chạc 3 tái bản và để lộ ra hai
mạch khuôn.$
				Enzim ADN – pôlimeraza sử dụng một mạch làm khuôn tổng hợp nên mạch mới theo nguyên tắc bổ sung,
trong đó A liên kết với T và ngược lại, G luôn liên kết với X và ngược lại.@
			<!--Câu 20-->Ở kì đầu của giảm phân 1, sự tiếp hợp và trao đổi chéo không cân giữa các đoạn crômatit cùng
nguồn gốc trong cặp NST tương đồng sẽ dẫn tới dạng đột biến$
				mất cặp và thêm cặp nuclêôtit$
				đảo đoạn NST$
				chuyển đoạn NST$
				~mất đoạn và lặp đoạn NST.@
			<!--Câu 21-->Ở một loài thực vật lưỡng bội sinh sản bằng tự thụ phấn, gen A quy định hoa đỏ trội hoàn toàn so
với a quy định hoa trắng. Thế hệ xuất phát của một quần thể có tỉ lệ kiểu hình là 9 cây hoa đỏ : 1 cây hoa
trắng. Ở thế hệ F2, tỉ lệ cây hoa trắng là 40%. Nếu ở F2, các cá thể giao phấn ngẫu nhiên thì theo lí thuyết, tỉ lệ kiểu hình ở F3 sẽ là$
				35 cây hoa đỏ : 1 cây hoa trắng$
				~3 cây hoa đỏ : 1 cây hoa trắng.$
				99 cây hoa đỏ : 1 cây hoa trắng.$
				 21 cây hoa đỏ : 4 cây hoa trắng.@
			<!--Câu 22-->Khi nói về nhiễm sắc thể giới tính ở người, phát biểu nào sau đây là đúng?$
				~Trên vùng tương đồng của nhiễm sắc thể giới tính X và Y, gen tồn tại thành từng cặp alen.$
				Trên vùng tương đồng của nhiễm sắc thể giới tính, gen nằm trên nhiễm sắc thể X không có alen tương ứng
trên nhiễm sắc thể Y.$
				Trên vùng không tương đồng của nhiễm sắc thể giới tính X và Y, các gen tồn tại thành từng cặp$
				Trên vùng không tương đồng của nhiễm sắc thể giới tính X và Y đều không mang gen.@
			<!--Câu 23-->Đột biến mất đoạn có bao nhiêu đặc điểm trong các đặc điểm sau đây?
			<br>(1). Làm thay đổi hàm lượng ADN ở trong nhân tế bào.
			<br>(2). Làm thay đổi chiều dài của phân tử ADN.
			<br>(3). Không phải là biến dị ditruyền.
			<br>(4). Làm xuất hiện các alen mới trong quần thể.$
				3$
				~2$
				1$
				4@
			<!--Câu 24-->Gen I có 3 alen, gen II có 4 alen, gen III có 5 alen. Biết gen I và II nằm trên X không có alen trên Y
và gen III nằm trên Y không có alen trên X. Số kiểu gen trong quần thể là?$
				154$
				214$
				~138$
				184@
			<!--Câu 25-->Một loài thú, cho con đực mắt trắng, đuôi dài giao phối với con cái mắt đỏ, đuôi ngắn (P), thu được
F1 có 100% con mắt đỏ, đuôi ngắn. Cho F1 giao phối với nhau, thu được F2 có kiểu hình gồm: Ở giới cái có
100% cá thể mắt đỏ, đuôi ngắn. Ở giới đực có 45% cá thể mắt đỏ, đuôi ngắn, 45% cá thể mắt trắng, đuôi
dài, 5% cá thể mắt trắng, đuôi ngắn, 5% cá thể mắt đỏ, đuôi dài. Biết mỗi cặp tính trạng do một cặp gen quy
định và không xảy ra đột biến. Theo lí thuyết, có bao nhiêu phát biểu sau đây đúng?
			<br>(1). Đời F1 có 8 loại kiểu gen.
			<br>(2). Đã xảy ra hoán vị gen ở giới đực với tần số 10%.
			<br>(3). Lấy ngẫu nhiên 1 cá thể cái ở F2, xác suất thu được cá thể thuần chủng là 45%.
			<br>(4). Nếu cho cá thể đực F1 lai phân tích thì sẽ thu được Fa có kiểu hình đực mắt đỏ, đuôi dài chiếm 2,5%$
				2$
				3$
				~1$
				4@
			<!--Câu 26-->Một quần thể sinh vật ngẫu phối đang chịu tác động của chọn lọc tự nhiên có cấu trúc di truyền ở
các thế hệ như sau:
			<br>Nhận xét nào sau đây là đúng về tác động của chọn lọc tự nhiên đối với quần thể này?$
			<br> <img src="https://scontent.fsgn5-2.fna.fbcdn.net/v/t1.15752-9/62264711_2213476802041217_4015887729924505600_n.png?_nc_cat=105&_nc_oc=AQlreLJIYktqxclptwiWFks1Uur8b91v6md17H1dK7d9HALN7NiO6cqhuFZIIUs_jsU&_nc_ht=scontent.fsgn5-2.fna&oh=70a9b03691f188d63b0a14ffec7da51d&oe=5D9564BC"/>$
			
				Chọn lọc tự nhiên đang loại bỏ những kiểu gen dị hợp và đồng hợp lặn.$
				Chọn lọc tự nhiên đang loại bỏ các kiểu gen đồng hợp và giữ lại những kiểu gen dị hợp.$
				Các cá thể mang kiểu hình lặn đang bị chọn lọc tự nhiên loại bỏ dần.$
				~Các cá thể mang kiểu hình trội đang bị chọn lọc tự nhiên loại bỏ dần.@
			<!--Câu 27-->Khi nói về vấn đề quản lí tài nguyên cho phát triển bền vững, phát biểu nào sau đây <b>không</b> đúng?$
				Con người phải tự nâng cao nhận thức và sự hiểu biết, thay đổi hành vi đối xử với thiên nhiên$
				Con người phải biết khai thác tài nguyên một cách hợp lí, bảo tồn đa dạng sinh học.$
				~Con người cần phải khai thác triệt để tài nguyên tái sinh, hạn chế khai thác tài nguyên không tái sinh.$
				Con người cần phải bảo vệ sự trong sạch của môi trường sống.@
			<!--Câu 28-->Khi nói về cạnh tranh cùng loài, có bao nhiêu phát biểu sau đây đúng?
			<br>(1). Khi môi trường đồng nhất và cạnh tranh cùng loài diễn ra khốc liệt thì các cá thể phân bố một cách đồng
đều trong khu vực sống của quần thể.
			<br>(2). Cạnh tranh cùng loài giúp duy trì ổn định số lượng cá thể của quần thể, cân bằng với sức chứa của môi
trường.
			<br>(3). Về mặt sinh thái, sự phân bố các cá thể cùng loài một cách đồng đều trong môi trường có ý nghĩa giảm sự
cạnh tranh gay gắt giữa các cá thể trong quần thể.
			<br>(4). Trong cùng một quần thể, cạnh tranh diễn ra thường xuyên giữa các cá thể để tranh giành nhau về thức ăn,
nơi sinh sản,...$
				2$
				1$
				3$
				~4@
			<!--Câu 29-->Cho các bước tạo động vật chuyển gen:
			<br>(1). Lấy trứng ra khỏi con vật
			<br>(2). Cấy phôi đã được chuyển gen vào tử cung con vật khác để nó mang thai và sinh đẻ bình thường
			<br>(3). Cho trứng thụ tinh trong ống nghiệm.
			<br>(4). Tiêm gen cần chuyển vào hợp tử và hợp tử phát triển thành phôi.
			<br>Trình tự đúng trong quy trình tạo động vật chuyển gen là$
				(2) (3) (4) (2).$
				~(1) (3) (4) (2).$
				(3) (4) (2) (1).$
				(1) (4) (3) (2).@
			<!--Câu 30-->Trong một quần xã sinh vật xét các loài sinh vật: Cây gỗ lớn, cây bụi, cây cỏ, hươu, sâu, thú nhỏ, đại
bàng, bọ ngựa và hổ. Đại bàng và hổ ăn thú nhỏ, Bọ ngựa và thú nhỏ ăn sâu ăn lá, Hổ có thể bắt hươu làm
thức ăn, Cây gỗ, cây bụi, cây cỏ là thức ăn của hươu, sâu, bọ ngựa. Trong các phát biểu sau đây về quần xã
này, có bao nhiêu phát biểu đúng?
			<br>(1). Chuỗi thức ăn dài nhất có 4 mắt xích.
			<br>(2). Hươu và sâu là những loài thuộc sinh vật tiêu thụ bậc 1.
			<br>(3). Quan hệ giữa đại bàng và hổ là quan hệ hợp tác
			<br>(4). Nếu bọ ngựa bị tiêu diệt thì số lượng thú nhỏ sẽ tăng lê
			<br>(5). Nếu giảm số lượng hổ thì sẽ làm tăng số lượng sâu.$
				4$
				1$
				~3$
				2@
			<!--Câu 31-->Khi nói về hóa thạch phát biểu nào sau đây <b>không</b> đúng?$
				Tuổi của hóa thạch được xác định được nhờ phân tích các đồng vị phóng xạ có trong hóa thạch$
				~Hóa thạch cung cấp cho chúng ta những bằng chứng gián tiếp về lịch sử tiến hóa của sinh giới.$
				Căn cứ vào hóa thạch có thể biết loài nào xuất hiện trước, loài nào xuất hiện sau.$
				Hóa thạch là di tích của sinh vật để lại trong các lớp đất đá của vỏ trái đất.@
			<!--Câu 32--> Khi nói về quá trình hình thành loài mới, phát biểu nào sau đây là sai?$
				Quá trình hình thành loài mới có thể diễn ra trong khu vực địa lí hoặc khác khu vực địa lí.$
				Hình thành loài mới bằng cách sinh thái thường xảy ra đối với các loại động vật ít di chuyển.$
				Quá trình hình thành loài mới bằng con đường cách li thường xảy ra một cách chậm chạp qua nhiều giai
đoạn trung gian chuyển tiếp$
				~Hình thành loài mới nhờ cơ chế lai xa và đa bội hóa diễn ra phổ biến ở cả động vật và thực vật@
			<!--Câu 33-->Vào những năm 80 của thế kỉ XX, ốc bươu vàng du nhập vào Việt Nam phát triển mạnh gây thiệt
hại cho ngành nông nghiệp. Sự gia tăng nhanh số lượng ốc bươu vàng là do:
			<br>(1). Tốc độ sinh sản cao.
			<br>(2). Gần như chưa có thiên địch
			<br>(3). Nguồn số dồi dào nên tốc độ tăng trưởng nhanh.
			<br>(4). Giới hạn sinh thái rộng.
			<br>Số phương án đúng là:$
				3$
				2$
				~4$
				1@
			<!--Câu 34-->Sơ đồ dưới minh họa lưới thức ăn trong một hệ sinh thái gồm các loài sinh vật: A, B, C, D, E, F, H.
Cho các kết luận sau về lưới thức ăn này:
			<br> <img src="https://scontent.fsgn5-2.fna.fbcdn.net/v/t1.15752-9/62653771_434944130564627_4934612759598858240_n.png?_nc_cat=107&_nc_oc=AQlXb8WLDd0wthls7H6bNHtc4Fawkz4yQwl_CzcKp8Scx2CjnsJqXoX1OuFfA-NGUdE&_nc_ht=scontent.fsgn5-2.fna&oh=6457cc6608c23d3372c7ebaabdf9bca0&oe=5D9AB349"/>
			<br>(1). Lưới thức ăn này có tối đa 6 chuỗi thức ăn.
			<br>(2). Loài D tham gia vào 2 chuỗi thức ăn khác nhau
			<br>(3). Loài E tham gia vào ít chuỗi thức ăn hơn loài F
			<br>(4). Nếu loại bỏ loài B ra khỏi quần xã thì loài D sẽ không mất đi
			<br>(5). Có 3 loài thuộc bậc dinh dưỡng cấp 5.
			<br>(6). Nếu số lượng cá thể của loài C giảm thì số lượng cá thể của loài F giảm.
			<br>Số kết luận đúng là:$
				~3$
				5$
				2$
				4@
			<!--Câu 35-->Có mấy phát biểu sau đây đúng khi nói về quá trình hình thành loài mới?
			<br>(1). Hình thành loài bằng cách li sinh thái thường xảy ra với các loài động vật ít di chuyển xa.
			<br>(2). Cách li địa lí góp phần duy trì sự khác biệt về tần số alen và thành phần kiểu gen giữa các quần thể được
tạo ra bởi các nhân tố tiến hóa.
			<br>(3). Hình thành loài nhờ lai xa và đa bội hóa thường xảy ra trong quần xã gồm nhiều loài thực vật có quan hệ họ
hàng gần gũi.
			<br>(4). Sự hình thành loài mới không liên quan đến quá trình phát sinh các đột biến
			<br>Số phương án đúng là:$
				~3$
				1$
				2$
				4@
			<!--Câu 36--> Ở một loài thực vật lưỡng bội: gen A quy định hoa đơn trội hoàn toàn so với gen a quy định hoa
kép, gen B quy định hoa dài trội hoàn toàn so với gen b quy định hoa ngắn. Biết rằng 2 gen quy định 2 tính
trạng trên cùng nhóm gen liên kết và cách nhau 20 cM. Mọi diễn biến trong giảm phân và thụ tinh đều bình
thường và hoán vị gen xảy ra ở 2 bên. Phép lai P: (đơn, dài) × (kép, ngắn). F1: 100% đơn, dài. Đem F1 tự
thụ phấn thu được F2. Cho các kết luận sau, có bao nhiêu kết luận đúng về thông tin trên?
			<br>(1). F2 có kiểu gen Ab/aB chiếm tỉ lệ 2%
			<br>(2). F2 tỉ lệ đơn, dài dị hợp là 66% .
			<br>(3). F2 gồm 4 kiểu hình: 66% đơn, dài: 9% đơn, ngắn: 9% kép, dài: 16% kép, ngắn
			<br>(4). Tỉ lệ kiểu gen dị hợp tử ở F2 chiếm 50%.
			<br>(5) Khi lai phân tích F1 thì đời con (Fa) gồm 10% cây kép, ngắn.
			<br>(6). Số kiểu gen ở F2 bằng 7.$
				~2$
				5$
				3$
				4@
			<!--Câu 37-->Mối quan hệ nào sau đây <b>không</b> mang tính chất thường xuyên và bắt buộc?$
				Trùng roi sống trong ruột mối$
				~Cây phong lan sống trên thân cây gỗ.$
				Nấm sống chung với địa y$
				 Giun sán sống trong ruột người.@
			<!--Câu 38--> Cho các phát biểu sau đây :
			<br>(1). Chọn lọc tự nhiên chống lại alen lặn chậm hơn so với trường hợp chọn lọc chống lại alen trội.
			<br>(2). Chọn lọc tự nhiên chỉ tác động khi điều kiện môi trường sống thay đổi.
			<br>(3). Đột biến và di - nhập gen là nhân tố tiến hoá có thể làm xuất hiện các alen mới trong quần thể sinh vật.
			<br>(4). Các yếu tố ngẫu nhiên làm thay đổi tần số các alen không theo một hướng xác định.
			<br>(5) Chọn lọc tự nhiên phân hóa khả năng sống sót và khả năng sinh sản của các kiểu gen khác nhau trong
quần thể
			<br>(6).Chọn lọc tự nhiên sẽ đào thải hoàn toàn một alen trội có hại ra khỏi quần thể khi chọn lọc chống lại
alen trội.
			<br>Số phát biểu đúng theo quan điểm hiện đại về tiến hóa là:$
				4$
				3$
				~5$
				6@
			<!--Câu 39--> Điều nào sau đây không phải là nguyên nhân dẫn đến diễn thế sinh thái ?$
				~Do cạnh tranh và hợp tác giữa các loài trong quần xã$
				Do chính hoạt động khai thác tài nguyên của con người$
				 Do thay đổi của điều kiện tự nhiên, khí hậu$
				Do cạnh tranh gay gắt giữa các loài trong quần xã@
			<!--Câu 40-->Phát biểu nào sau đây không phải là quan niệm của Đacuyn?$
				 Toàn bộ sinh giới ngày nay là kết quả quá trình tiến hóa từ một nguồn gốc chung.$
				Chỉ có những biến dị phát sinh trong quá trình sinh sản mới là nguyên liệu của tiến hóa.$
				~Ngoại cảnh thay đổi mạnh là nguyên nhân gây ra những biến đổi trên cơ thể sinh vật.$
				Chọn lọc tự nhiên tác động thông qua đặc tính biến dị và di truyền của sinh vật	
			</div>
			<div id="countDown">	
				<p>Thời gian còn lại:</p>
				<p class="minute">Phút</p>
				<p class="second">Giây<p>
				
			<div>
			<script type="text/javascript">
				function getCookie(name){
					var cname = name + "=";
					var dc = document.cookie;
					if (dc.length > 0) {
						begin = dc.indexOf(cname);
						if (begin != -1) {
							begin += cname.length;
							end = dc.indexOf(";", begin);
						if (end == -1) end = dc.length;
						return unescape(dc.substring(begin, end));
						}
					}
					return null;
				}
				if(document.cookie && document.cookie.match('myClock_Sinh1')){
				  // get deadline value from cookie
				  var deadline = getCookie('myClock_Sinh1');
				}
				 
				
				else{
				  // create deadline 50 minutes from now
					var currentTime = Date.parse(new Date());
					var deadline = Date.parse(new Date(currentTime + 50*60*1000));
				 
				  // store deadline in cookie for future reference
				  document.cookie = 'myClock_Sinh1=' + deadline + '; path=/';
				}
							
			
			
				
				var currentTime = Date.parse(new Date());
				
				var t=deadline-currentTime;
				var second = Math.floor((t / 1000) % 60);
				var minute = Math.floor(t / 1000 / 60);
				
				
				
			
				var nd = $("#noi_dung_de").html();
				var cau_hoi = nd.split('@');
				var html = ""; var tra_loi_dung = 0;
				for(var i = 0;i<cau_hoi.length;i++)
				{
					var chi_tiet = cau_hoi[i].split('$');
					//alert(chi_tiet[i]);
					for(var j=0;j<chi_tiet.length;j++)
					{
			 
						if (j==0) html += "<tr><td><b>Câu " + (i+1) + ":</b></td><td> <b>" + chi_tiet[j].trim() + "</b></td></tr>";
						else 
						{
							html += "<tr><td></td><td id='" + (i + 1 + String.fromCharCode(64 + j))+"'><input type='radio' name='" + (i + 1 )+ "' id='" + (i + 1 + String.fromCharCode(64 + j))+"' value='" + chi_tiet[j].trim() +"'> " + String.fromCharCode(64 + j) + ". " + chi_tiet[j].replace("~","").trim() + "</td></tr>";
						}
					}
				}

				$("#noi_dung_de").empty().append("<table>" + html + "</table><input type='submit' value='Nộp bài' id='tra_loi_xong' style='margin-left:585px;'>").fadeIn();
				$("#noi_dung_de input").click(function(){   
				//Lấy id của radio
				var id = $(this).attr("id");
				for (var j=1;j<5;j++){
					var newid=id.substr(0,1)+String.fromCharCode(64 + j);
					
					$("td#" + newid).css("background-color","");
				}
				$("td#" + id).css("background-color","yellow");
				
				});   
				
2				
				
3				
4				
			
				
				/*jQuery(document).ready(function($) {
					
					setTimeout(function(){
						
						$('#tra_loi_xong').trigger( "click" ) ;
						}, time);
				}); */
				
				
				
			
			
				$("#tra_loi_xong").click(function(){
					$('#noi_dung_de input').each(function () {
					var id = $(this).attr("id");
					var ctl = $(this).val();
				 
					//Hiển thị câu đúng với nền là màu đỏ
					if (ctl[0] == '~') 
					{
						//alert($("td#" + id).css("background-color"));
						if ($("td#" + id).css("background-color") == "rgb(255, 255, 0)")
						{
							tra_loi_dung++;
						}
						else $("td#" + id).css("background-color","red");     
					}
				});
				$("#noi_dung_de").append("<p style='text-align:center'>Số câu đúng :" + (tra_loi_dung < 0 ? "0" : tra_loi_dung) + " câu. Điểm : " + tra_loi_dung*0.25+ "</p>");
				$("#tra_loi_xong").fadeOut();
				
				});
				
				var timer = setInterval(function() {
					
				   $('.minute').text(minute +" Phút")
				   $('.second').text(second-- +" Giây");
				   if (second == -1) {
						minute--;
						second=60;
				   }
				   if (minute ==-1)  {
						 alert("Đã hết giờ làm bài, kiểm tra kết quả");
					  $('#tra_loi_xong').trigger( "click" ) ;
					  $('.second').fadeOut("slow");
					  $('.minute').fadeOut("slow");
					  clearInterval(timer);
				   }
				   
				}, 1000);
				
			</script>
			
		</div>
	</body>
</html>