﻿<!DOCTYPE html>
<?php
include 'UserID.php';
$user = new UserID();
if(!$user->isLogin()) header('Location:login.php');
?>
<html>
	<head>
		<meta charset="UTF-8" />
		<title>Thi thử-SDCD 1</title>
		<link rel="stylesheet" type="text/css" href="examstyle.css" />
		
		
	</head>
	<body>
		<h1>Đề thi thử môn GDCD-Đề 1</h1>
		<p id="time">Thời gian: 50p</p>
		<script src='examform.js'></script>
		<div id="content">
			<div id="noi_dung_de" style='display:none;min-height:3000px'>
				
				Việc xử lý người chưa thành niên phạm tội cần phải tuân thủ nguyên tắc cơ bản nào sau đây?
					@Trừng trị thích đáng.
					@~Lấy giáo dục là chủ yếu.
					@Xử lý nghiêm minh.
					@Chỉ phạt tiền.`
				Anh A làm thủ tục và thoả thuận thuê 1 ô tô của anh B trong vòng 2 ngày. Nhưng sau thời hạn 5 ngày anh A mới đem xe đến trả và bị hư hỏng nặng, anh B đòi bồi thường thiệt hại, anh A không chịu nên anh B khởi kiện ra tòa án. Trong trường hợp này, hành vi của anh A thuộc loại vi phạm nào?
					@Kỷ luật.
					@Hình sự.
					@~Dân sự.
					@Hành chính`
				Hình thức nào sau đây áp dụng với người vi phạm kỉ luật?
					@Phê bình.
					@~Cảnh cáo.
					@Phạt tiền.
					@Cải tạo không giam giữ.`
				Do mâu thuẫn trên facebook giữa học sinh A với học sinh B và C nên B và C đã rủ H, N và M tìm gặp A để hỏi, lời qua tiếng lại thấy A ra vẻ thách thức nên H, B, C, M, N đã chủ động đợi lúc tan học đã chặn đường A. H và B lao vào A và đánh dằn mặt, còn  N thì đứng quay lại cảnh đánh nhau, rách áo và tung lên mạng. Qúa nhục nhã nên A rơi vào khủng hoảng và đã tìm đến tự tử. Hậu quả A bị ảnh hưởng đến tình trạng sức khỏe nghiêm trọng. Những ai dưới đây đã vi phạm pháp luật?
					@~Học sinh B, C, H, N.
					@Học sinh H, B, C, M, N.
					@Học sinh A, B, C, H, N.
					@Học sinh H, B và N.`
				Cả ba doanh nghiệp M, N và Q cùng sản xuất một loại hàng hóa X có chất lượng như nhau nhưng thời gian lao động cá biệt khác nhau: Doanh nghiệp M là 3 giờ, doanh nghiệp N là 2,5 giờ, doanh nghiệp Q là 3,5 giờ. Thời gian lao động xã hội cần thiết để làm ra mặt hàng này là 3 giờ. Doanh nghiệp nào dưới đây đã vi phạm yêu cầu của quy luật giá trị?
					@Doanh nghiệp Q và M.
					@Doanh nghiệp N.
					@Doanh nghiệp M.
					@Doanh nghiệp Q.`
				Dấu hiệu nào dưới đây là một trong những căn cứ để xác định một hành vi vi phạm pháp luật?
					@Hành vi do người có thẩm quyền trong cơ quan nhà nước thực hiện.
					@Hành vi do người trên 16 đến 18 tuổi thực hiện.
					@Hành vi do người trên 18 tuổi thực hiện.
					@~Hành vi do người có năng lực trách nhiệm pháp lí thực hiện.`
				Cá nhân, tổ chức sử dụng pháp luật tức là không làm những điều mà pháp luật
					@cấm.
					@~cho phép làm.
					@không cấm.
					@không đồng ý.`
				Giới hạn mà trong đó sự biến đổi về lượng chưa làm thay đổi về chất của sự vật và hiện tượng được gọi là
					@~độ.
					@điểm nút.
					@chất.
					@lượng.`
				Người phải chịu trách nhiệm hành chính về mọi vi phạm hành chính do mình gây ra có độ tuổi theo qui định của pháp luật là
					@từ đủ 18 tuổi trở lên.
					@từ 18 tuổi trở lên.
					@~từ đủ 16 tuổi trở lên.
					@từ đủ 14 tuổi trở lên.`
				Cá nhân, tổ chức không làm những việc phải làm theo quy định của pháp luật là hành vi trái pháp luật dưới dạng nào?
					@~Không hành động.
					@Hành động.
					@Có thể hành động.
					@Có thể là không hành động.`
				Ngoài việc bình đẳng về hưởng quyền, công dâncòn bình đẳng trong thực hiện
					@trách nhiệm.
					@~nghĩa vụ.
					@công việc chung.
					@nhu cầu riêng.`
				Công ty S do ông V làm giám đốc đã gây thất thoát hàng chục tỷ đồng của nhà nước, đồng thời ông V còn chỉ đạo kế toán công ty là chị T tiêu hủy các chứng từ có liên quan. Biết chuyện đó nên anh X là nhân viên công ty đã tố cáo ông V, thấy vậy con ông V là M đã nhờ S, Q và K hành hung anh X, đồng thời đưa ông V trốn đi xa. Còn chị T do được gia đình vận động đã ra đầu thú. Những ai dưới đây có hành vi vi phạm pháp luật?
					@~Ông V, chị T, anh M, anh S, anh Q, anh K.
					@Ông V, anh M, anh S, anh Q, anh K, anh X.
					@Anh S, anh Q, anh K.
					@Ông V, anh M, anh S, anh Q, anh K.`
				Theo nội dung quy luật cung-cầu, giá cả thị trường thường cao hơn giá trị hàng hóa trong sản xuất khi
					@~cung nhỏ hơn cầu.
					@cung lớn hơn cầu.
					@cung bằng cầu.
					@cầu giảm, cung tăng.`
				Do mâu thuẫn trên Facebook nên A và M hẹn gặp C và H để hòa giải. Biết chuyện này, anh trai của A đã rủ N chặn đường gây gổ với H và C. Do bị gây gổ nên anh C đã dùng dao đâm anh N bị thương nặng. Những ai dưới đây phải chịu trách nhiệm hình sự?
					@Anh trai A, C, M, A.
					@Anh trai A, N, C.
					@~Anh C.
					@Anh trai A, M, N, H, A.`
				Bồi thường thiệt hại về vật chất khi có hành vi xâm phạm tới các quan hệ tài sản và nhân thân được áp dụng với người có hành vi vi phạm
					@hình sự.
					@kỉ luật.
					@hành chính.
					@~dân sự.`
				Ông H cho ông G vay một khoản tiền, việc vay trên đã được ông G viết giấy biên nhận, trong đó có ngày hẹn sẽ trả. Đúng đến ngày hẹn, ông H đến nhà ông G đề nghị trả số tiền này, nhưng ông G không trả với lí do chưa có và hẹn ngày khác, hai ông đã cự cãi và dẫn đến xô xát. Thấy thế T (21 tuổi) và Q (19 tuổi) là con trai của ông G đã xông vào đánh ông H bị trọng thương trên 11%. Ông H phải nằm viện điều trị hết 25 triệu đồng. Những ai dưới đây phải chịu trách nhiệm dân sự?
					@Ông G.
					@Ông H.
					@~Ông G, anh T và anh Q.
					@Ông H, ông B, anh T và anh Q.`
				Pháp luật không quy định về những việc nào dưới đây?
					@Không được làm.
					@Được làm.
					@Phải làm.
					@~Nên làm.`
				Chị M trả nợ hết 800 ngìn đồng. Trong trường hợp này tiền đã thực hiện chức năng
					@thước đo giá trị.
					@phương tiện cất trữ.
					@phương tiện lưu thông.
					@~phương tiện thanh toán.`
				Nam thanh niên đủ điều kiện theo qui định của pháp luật mà trốn nghĩa vụ quân sự là không thực hiện pháp luật theo hình thức nào dưới đây?
					@~Thi hành pháp luật.
					@Sử dụng pháp luật.
					@Tuân thủ pháp luật.
					@Áp dụng pháp luật.`
				Công ty  X ở tỉnh Y do ông A làm giám đốc đã có hành vi trốn thuế. Biết được việc đó, anh C cùng với anh D và anh E đã làm đơn tố cáo ông A. Nhận được đơn tố cáo, cơ quan chức năng Z đã vào cuộc kiểm tra công ty X và buộc công ty X phải nộp lại đầy đủ số tiền thuế đã trốn và nộp thêm tiền phạt theo quy định của pháp luật. Những ai dưới đây đã áp dụng pháp luật?
					@~Cơ quan chức năng Z.
					@Cơ quan chức năng Z, anh C, anh D, anh E.
					@Anh C,anh D, anh E.
					@Công ty X và ông A.`
				Bất kỳ công dân nào vi phạm pháp luật đều phải chịu trách nhiệm về hành vi vi phạm của mình và bị xử lí theo quy định của pháp luật. Điều này thể hiện công dân bình đẳng về
					@~trách nhiệm pháp lí.
					@trách nhiệm kinh tế.
					@trách nhiệm xã hội.
					@trách nhiệm chính trị.`
				Hành vi không nhường ghế trên xe buýt cho người già, trẻ em, phụ nữ mang thai là hành vi vi phạm
					@pháp luật hành chính.
					@pháp luật dân sự.
					@pháp luật hình sự.
					@~chuẩn mực đạo đức.`
				Sau khi hoàn thành cuộc cách mạng dân tộc dân chủ nhân dân, Đảng và nhân dân ta đã lựa chọn con đường lên chủ nghĩa xã hội bỏ qua chế độ
					@chiếm hữu nô lệ.
					@~tư bản chủ nghĩa.
					@tư bản độc quyền.
					@phong kiến.`
				Một hòn đá lăn từ độ cao 20m trên mặt phẳng nghiêng là thuộc hình thức vận động cơ bản nào sau đây của thế giới vật chất?
					@~Cơ học.
					@Vật lí.
					@Sinh học.
					@Hoá học.`
				Câu tục ngữ nào sau đây mang yếu tố biện chứng?
					@Giàu sang do trời.
					@~Nước chảy đá mòn.
					@Sống chết có mệnh.
					@Trời cho hơn lo làm.`
				Bản chất của mỗi trường phái triết học là trả lời các câu hỏi về
					@ý thức.
					@~mối quan hệ giữa ý thức và vật chất.
					@vật chất.
					@mối quan hệ giữa ý thức và tư duy.`
				Do bị chị N thường xuyên bịa đặt nói xấu mình trên facebook nên chị V cùng em gái là G đã đến tận nhà chị N để làm rõ mọi chuyện. Trong lúc chị V  và chị N đang nói chuyện, cô G đã sử dụng điện thoại di động quay và phát trực tiếp lên facebook, chồng chị N tức giận đã mắng chửi và đánh chị G đánh trọng thương. Những ai dưới đây phải chịu trách nhiệm pháp lí?
					@Chồng chị N.
					@Vợ chồng chị N và cô G.
					@Chị N.
					@~Vợ chồng chị N.`
				Trong lời kêu gọi toàn quốc kháng chiến (19/12/1946), Chủ tịch Hồ Chí Minh đã viết: “Hỡi đồng bào toàn quốc! Chúng ta muốn hòa bình, chúng ta phải nhân nhượng. Nhưng chúng ta càng nhân nhượng, thực dân Pháp càng lấn tới, vì chúng muốn cướp nước ta lần nữa! Không! Chúng ta thà hy sinh tất cả, chứ nhất định không chịu mất nước, nhất định không chịu làm nô lệ. Hỡi đồng bào, Chúng ta phải đứng lên!”. Trong đoạn trích trên, Chủ tịch Hồ Chí Minh đã giải quyết mâu thuẫn bằng
					@thương lượng.
					@hoà bình.
					@thoả hiệp.
					@~đấu tranh.`
				Hệ thống quy tắc xử sự chung do nhà nước xây dựng, ban hành và bảo đảm thực hiện bằng quyền lực nhà nước là nội dung của khái niệm nào dưới đây?
					@Quy định.
					@Quy tắc.
					@~Pháp luật.
					@Quy chế.`
				Anh B đi xe máy trên đường phố bị một cành cây rơi xuống làm anh B không tự chủ được tay lái, nên cả người và xe văng trên đường. Anh A đi sau một đoạn đâm vào xe máy của anh B làm xe máy của B hư hại một số bộ phận và bản thân B bị thương nhẹ. Anh B đòi anh A bồi thường thiệt hại về sức khỏe và tài sản. A không chịu bồi thường vì cho rằng mình không có lỗi. Anh B đã gọi anh D và anh Q đến đánh anh A và lấy xe máy của anh A về nhà, rồi yêu cầu anh A mang tiền đến đền bù mới trả xe. Những ai dưới đây đã vi phạm pháp luật?
					@Anh A.
					@Anh D và anh Q.
					@~Anh B, anh D và anh Q.
					@Anh A, anh B, anh D và anh Q.`
				Phủ định biện chứng tạo điều kiện, làm tiền đề cho sự
					@đấu tranh.
					@~phát triển.
					@tồn tại.
					@diệt vong.`
				Hành vi nào sau đây không phải chịu trách nhiệm pháp lý?
					@Vượt đèn đỏ.
					@Kinh doanh không đóng thuế.
					@~Không tụ tập đua xe trái phép.
					@Kinh doanh không đúng theo giấy phép kinh doanh.`
				Thông qua thị trường, giá trị sử dụng và giá trị của hàng hoá được
					@phản ánh.
					@thông qua.
					@biểu hiện.
					@~thực hiện.`
				Anh A vừa lái xe máy vừa sử dụng điện thoại. Em B đi xe đạp điện bất ngờ sang đường, anh A không kịp xử lý nên đã va vào em B. Hậu quả là anh A và em B đều bị thương, xe máy của anh A và xe đạp điện của em B bị hư hỏng. Cảnh sát giao thông đã lập biên bản và xử lý theo đúng quy định của pháp luật. Trong trường hợp trên, ai thực hiện đúng pháp luật?
					@Anh A.
					@~Cảnh sát giao thông.
					@Anh A và cảnh sát giao thông.
					@Em B và cảnh sát giao thông.`
				Ý nghĩa nhân văn sâu sắc của sự nghiệp giáo dục nước ta được thể hiện qua phương hướng nào sau đây?
					@Ưu tiên đầu tư cho giáo dục.
					@~Thực hiện công bằng xã hội trong giáo dục.
					@Nâng cao chất lượng, hiệu quả giáo dục.
					@Mở rộng quy mô giáo dục.`
				Chỉ có đem những tri thức mà con người thu nhận được kiểm nghiệm qua thực tiễnmới đánh giá được tính đúng đắn hay sai lầm của chúng. Điều này thể hiện thực tiễn là
					@mục đích của nhận thức.
					@động lực của nhận thức.
					@~tiêu chuẩn của chân lí.
					@cơ sở của nhận thức.`
				Trong các văn bản dưới đây, văn bản nào có giá trị pháp lí cao nhất?
					@~Hiến pháp.
					@Bộ luật Hình sự.
					@Bộ luật Dân sự.
					@Luật Đất đai.`
				Ông A rủ ông B và ông C cùng đột nhập vào tiệm vàng X để ăn trộm nhưng ông C từ chối không đi. Biết chuyện này, vợ ông A đã ngăn cản chồng nhưng không được. Khi ông A và ông B đang phá khóa tủ thì bị phát hiện nên đã bỏ chạy. Những ai dưới đây không phải chịu trách nhiệm pháp lí?
					@Ông A, ông B.
					@Ông A, vợ ông A, ông B, ông C.
					@~Ông C, vợ ông A.
					@Ông A, ông B, ông C.`
				Sức lao động, đối tượng lao động và tư liệu lao động là những yếu tố cơ bản của
					@mọi quá trình trao đổi, mua bán.
					@mọi tư liệu sản xuất.
					@~mọi quá trình sản xuất.
					@mọi xã hội.`
				Loại cạnh tranh nào giữ vai trò là động lực kinh tế của sản xuất và lưu thông hàng hóa?
					@~Cạnh tranh lành mạnh.
					@Cạnh tranh không lành mạnh.
					@Cạnh tranh giữa các doanh nghiệp lớn.
					@Mọi loại canh tranh.
			</div>
			<div id="countDown">	
				<p>Thời gian còn lại:</p>
				<p class="minute">Phút</p>
				<p class="second">Giây<p>
				
			<div>
			<script type="text/javascript">
				function getCookie(name){
					var cname = name + "=";
					var dc = document.cookie;
					if (dc.length > 0) {
						begin = dc.indexOf(cname);
						if (begin != -1) {
							begin += cname.length;
							end = dc.indexOf(";", begin);
						if (end == -1) end = dc.length;
						return unescape(dc.substring(begin, end));
						}
					}
					return null;
				}
				if(document.cookie && document.cookie.match('myClock_GDCD1')){
				  // get deadline value from cookie
				  var deadline = getCookie('myClock_GDCD1');
				}
				 
				
				else{
				  // create deadline 50 minutes from now
					var currentTime = Date.parse(new Date());
					var deadline = Date.parse(new Date(currentTime + 50*60*1000));
				 
				  // store deadline in cookie for future reference
				  document.cookie = 'myClock_GDCD1=' + deadline + '; path=/';
				}
							
			
			
				
				var currentTime = Date.parse(new Date());
				
				var t=deadline-currentTime;
				var second = Math.floor((t / 1000) % 60);
				var minute = Math.floor(t / 1000 / 60);
				
				
				
			
				var nd = $("#noi_dung_de").html();
				var cau_hoi = nd.split('`');
				var html = ""; var tra_loi_dung = 0;
				for(var i = 0;i<cau_hoi.length;i++)
				{
					var chi_tiet = cau_hoi[i].split('@');
					//alert(chi_tiet[i]);
					for(var j=0;j<chi_tiet.length;j++)
					{
			 
						if (j==0) html += "<tr><td><b>Câu " + (i+1) + ":</b></td><td> <b>" + chi_tiet[j].trim() + "</b></td></tr>";
						else 
						{
							html += "<tr><td></td><td id='" + (i + 1 + String.fromCharCode(64 + j))+"'><input type='radio' name='" + (i + 1 )+ "' id='" + (i + 1 + String.fromCharCode(64 + j))+"' value='" + chi_tiet[j].trim() +"'> " + String.fromCharCode(64 + j) + ". " + chi_tiet[j].replace("~","").trim() + "</td></tr>";
						}
					}
				}

				$("#noi_dung_de").empty().append("<table>" + html + "</table><input type='submit' value='Nộp bài' id='tra_loi_xong' style='margin-left:585px;'>").fadeIn();
				$("#noi_dung_de input").click(function(){   
				//Lấy id của radio
				var id = $(this).attr("id");
				for (var j=1;j<5;j++){
					var newid=id.substr(0,1)+String.fromCharCode(64 + j);
					
					$("td#" + newid).css("background-color","");
				}
				$("td#" + id).css("background-color","yellow");
				
				});   
				
2				
				
3				
4				
			
				
				/*jQuery(document).ready(function($) {
					
					setTimeout(function(){
						
						$('#tra_loi_xong').trigger( "click" ) ;
						}, time);
				}); */
				
				
				
			
			
				$("#tra_loi_xong").click(function(){
					$('#noi_dung_de input').each(function () {
					var id = $(this).attr("id");
					var ctl = $(this).val();
				 
					//Hiển thị câu đúng với nền là màu đỏ
					if (ctl[0] == '~') 
					{
						//alert($("td#" + id).css("background-color"));
						if ($("td#" + id).css("background-color") == "rgb(255, 255, 0)")
						{
							tra_loi_dung++;
						}
						else $("td#" + id).css("background-color","red");     
					}
				});
				$("#noi_dung_de").append("<p style='text-align:center'>Số câu đúng :" + (tra_loi_dung < 0 ? "0" : tra_loi_dung) + " câu. Điểm : " + tra_loi_dung*0.25+ "</p>");
				$("#tra_loi_xong").fadeOut();
				
				});
				
				var timer = setInterval(function() {
					
				   $('.minute').text(minute +" Phút")
				   $('.second').text(second-- +" Giây");
				   if (second == -1) {
						minute--;
						second=60;
				   }
				   if (minute ==-1)  {
						 alert("Đã hết giờ làm bài, kiểm tra kết quả");
					  $('#tra_loi_xong').trigger( "click" ) ;
					  $('.second').fadeOut("slow");
					  $('.minute').fadeOut("slow");
					  clearInterval(timer);
				   }
				   
				}, 1000);
				
			</script>
			
		</div>
	</body>
</html>