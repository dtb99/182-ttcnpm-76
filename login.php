<?php
include_once 'router.php';
include_once 'UserID.php';
$r = new router();
$account = trim($r->getPOST('username'));
$password = trim($r->getPOST('password'));
$id = new UserID();
if ($id->isLogin()) header('Location:home.php');
if ($r->getPOST("submit") && $account && $password) {
    $id->username = $account;
    $id->password = $password;
    if ($id->login()) {
        if ($id->username == "admin") header('Location:Search.php');
        else header('Location:home.php');
    }
        else echo "Username or password is correct";
}
?>
<html>


<head>
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" href="loginstyle.css">
    <title>Sign in</title>
</head>

<body>
<div class="main">
    <p class="sign" align="center">Sign in</p>
    <form class="form1" action="login.php" method="POST">
        <input class="un " type="text" align="center" placeholder="Username" name="username">
        <input class="pass" type="password" align="center" placeholder="Password" name="password">
        <input type="submit" class="submit" align="center" name="submit" value="Sign in">
        <p class="havenot" align="center"><a href="register.php">Bạn chưa có tài khoản?</p>


</div>

</body>


</html>