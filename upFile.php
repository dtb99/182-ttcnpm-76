﻿<!DOCTYPE html>
<?php
include_once 'UserID.php';
$id = new UserID();
if ($id->isLogin()) {
    if ($id->getSESSION("username") !== "admin") header('Location:home.php');
} else echo "Page not found";
if(isset($_POST["upload"])){
    if(isset($_FILES["file"])&&!$_FILES["file"]["error"]){
        move_uploaded_file($_FILES["file"]["tmp_name"],'./Doc/'.$_FILES["file"]["name"]);
        echo "File uploaded <br>";
    }
    else echo "Error";
}
?> 
<html>
<head>
	<meta charset="UTF-8" />
	<title>Up tài liệu </title>
	<link rel="stylesheet" type="text/css" href="style.css" />
	
</head>
<body>
	<div id="page">
		<div id="header">
            <div id="section">
                <p> Hello ADMIN <a href="logout.php">Logout</a></p>
            </div>
			<div id="section">
				<p></p>
			</div>
			<ul>
                <li><a href="home.php">Trang chủ</a></li>
                <li><a href="testExam.php">THI THỬ</a></li>
                <li><a href="documents.php">TÀI LIỆU</a></li>
                <li><a href="news.php">TIN TỨC</a></li>
				<li><a href="Search.php">TÌM KIẾM USERS</a></li>
				<li class="current" ><a href="upFile.php">TẢI LÊN TÀI LIỆU</a></li>
			</ul>
		</div>
		<div id="content">
			<div class="container">
				<form method="post" action="upFile.php" enctype="multipart/form-data" class="searchform" style="padding:50px 10px">
					<input type="file" name="file"/>
					<input type="submit" name="upload" value="Upload"/>
				</form>
					
			</div>	
		</div>	
		<div id="footer">
			<div>
				<div id="connect">
					<a href="https://www.facebook.com/groups/644703785954707/?multi_permalinks=645737145851371&notif_id=1555690841219669&notif_t=group_activity" target="_blank"><img src="images/icon-facebook.gif" alt="Facebook"/></a>
				</div>
				<div id="contact">
					<p>0333 879 987 || nhan.tran1245@hcmut.edu.vn</p>
				</div>
			</div>
		</div>	
	</div>

</body>
</html>
        
