<!DOCTYPE html>
<?php
include 'UserID.php';
$user = new UserID();
if(!$user->isLogin()) header('Location:login.php');
?>
<html>
	<head>
		<meta charset="UTF-8" />
		<title>Đề THPTQG môn Vật lý- Đề số 3</title>
		<link rel="stylesheet" type="text/css" href="examstyle.css" />
		
		
	</head>
	<body>
		<h1>Đề thi thử THPTQG 2019-Đề số 3</h1>
		<p align="center"> <b> Môn: Vật lý </b> </p>
		<p id="time">Thời gian làm bài: 50 phút</p>		
		<script src='examform.js'></script>
		<div id="content">
			<div id="noi_dung_de" style='display:none;'>
			<!--Câu 31-->Một chất điểm tham gia đồng thời hai dao động
điều hòa cùng phương có đồ thị như hình vẽ. Phương trình
vận tốc của chất điểm là:<br><img src="https://scontent.fsgn5-2.fna.fbcdn.net/v/t1.15752-9/64238802_351854565519046_3745455900110880768_n.png?_nc_cat=105&_nc_oc=AQmNqfhBoz7n5QgZiSyiiZqKarUC5F0mGv8XmamRODpUjGAWIH4N-xPp-ABx_V6stL4&_nc_ht=scontent.fsgn5-2.fna&oh=fcf31ad86b6e1c8784c3bdf2ffb1c360&oe=5D87E17F"/>$
				<img src="https://latex.codecogs.com/gif.latex?\small&space;v=\frac{5\pi&space;}{2}\cos&space;(\frac{\pi&space;}{2}t&plus;\frac{\pi&space;}{2})cm/s" title="\small v=\frac{5\pi }{2}\cos (\frac{\pi }{2}t+\frac{\pi }{2})cm/s" />$
				~<img src="https://latex.codecogs.com/gif.latex?\small&space;v=\frac{\pi&space;}{2}\cos&space;\frac{\pi&space;}{2}t&space;cm/s" title="\small v=\frac{\pi }{2}\cos \frac{\pi }{2}t cm/s" />$
				<img src="https://latex.codecogs.com/gif.latex?\small&space;v=\frac{\pi&space;}{2}\cos&space;(\frac{\pi&space;}{2}t-\frac{\pi&space;}{2})cm/s" title="\small v=\frac{\pi }{2}\cos (\frac{\pi }{2}t-\frac{\pi }{2})cm/s" />$
				<img src="https://latex.codecogs.com/gif.latex?\small&space;v=\frac{5\pi&space;}{2}\cos&space;(\frac{\pi&space;}{2}t-\frac{\pi&space;}{2})cm/s" title="\small v=\frac{5\pi }{2}\cos (\frac{\pi }{2}t-\frac{\pi }{2})cm/s" />@
			<!--Câu 32-->Biên độ dao động cưỡng bức của hệ không phụ thuộc vào$
				biên độ của ngoại lực$
				tần số riêng của hệ$
				~pha của ngoại lực$
				tần số của ngoại lực@
			<!--Câu 33-->Hình vẽ bên là đồ thị biểu diễn sự phụ, thuộc của động năng W<sub>đh</sub> của
một con lắc lò xo vào thời gian t. Tần số dao động của con lắc bằng<img src="https://scontent.fsgn5-6.fna.fbcdn.net/v/t1.15752-9/64222870_825586334481303_4501813255982809088_n.png?_nc_cat=106&_nc_oc=AQm1UxZGuwFykYUrISpHtYKQyug0qNq18RECWPsMmbT8yWl-7TNaVBwO4pVtFaPsDtM&_nc_ht=scontent.fsgn5-6.fna&oh=c320ea559624eac95abc33f4262ae7c6&oe=5D93CB57"/>$
				~37,5 Hz$
				10 Hz$
				18,75 Hz$
				20 Hz@
			<!--Câu 34-->Đặt điện áp xoay chiều có giá trị hiệu dụng 120V, tần số không đổi
vào hai đầu đoạn mạch AB gồm đoạn mạch AM ghép nối tiếp với đoạn mạch MB. Đoạn mạch AM chỉ có
biến trở R; đoạn mạch MB gồm cuộn dây không thuần cảm ghép nối tiếp với tụ C. Điều chỉnh R đến giá trị
R<sub>o</sub> sao cho công suất tiêu thụ trên biến trở đạt cực đại thì thấy điện áp hiệu dụng đoạn mạch MB bằng
<img src="https://latex.codecogs.com/gif.latex?\small&space;40\sqrt{3}" title="\small 40\sqrt{3}" /> V và công suất tiêu thụ trên đoạn mạch AB bằng 90W. Công suất tiêu thụ trên đoạn mạch MB bằng$
				~30 W$
				22,5 W$
				40 W$
				45 W@
			<!--Câu 35--> 35. Đặt điện áp xoay chiều <img src="https://latex.codecogs.com/gif.latex?\small&space;u=U\sqrt{2}\cos&space;(100\pi&space;t)(V)" title="\small u=U\sqrt{2}\cos (100\pi t)(V)" /> vào hai đầu đoạn mạch mắc nối tiếp gồm điện trở thuần
R, tụ điện có điện dung C và cuộn cảm thuần có độ tự cảm L thay đổi được. Điều chỉnh L để điện áp hiệu
dụng ở hai đầu cuộn cảm đạt giá trị cực đại thì thấy giá trị cực đại đó bằng 125 V và điện áp hiệu dụng ở hai
đầu tụ điện bằng 80 V. Giá trị của U là$
				48 V$
				~75 V$
				64 V$
				80 V@
			<!--Câu 36-->Một con lắc lò xo treo thẳng đứng. Kích thích cho con lắc dao động điều hòa theo phương thẳng
đứng. Chu kì và biên độ dao động của con lắc lần lượt là 0,4 s và 8 cm. Chọn trục x’x thẳng đứng chiều dương
hướng xuống, gốc tọa độ tại vị trí cân bằng, gốc thời gian t = 0 khi vật qua vị trí cân bằng theo chiều dương.
Lấy gia tốc rơi tự do g = 10 m/s<sup>2</sup> và  π<sup>2</sup> = 10. Thời gian ngắn nhất kể từ khi t = 0 đến khi lực đàn hồi của lò xo
có độ lớn cực tiểu là$
				~7/30s$
				4/15s$
				3/10s$
				1/30s@
			<!--Câu 37-->Cho con lắc đơn dài l =100cm, vật nặng m có khối lượng 100g, dao động tại nơi có gia tốc trọng
trường g = 10m/s<sup>2</sup>. Kéo con lắc lệch khỏi vị trí cân bằng một góc <img src="https://latex.codecogs.com/gif.latex?\small&space;\alpha&space;_{o}=60^{0}" title="\small \alpha _{o}=60^{0}" /> rồi thả nhẹ. Bỏ qua ma sát. Chọn
đáp án đúng$
				Lực căng của dây treo có độ lớn cực đại khi vật ở vị trí biên và bằng 0,5N$
				Tốc độ của vật khi qua vị trí có li độ góc <img src="https://latex.codecogs.com/gif.latex?\small&space;\alpha=30^{0}" title="\small \alpha=30^{0}" /> xấp xỉ bằng 2,7(m/s).$
				Lực căng của dây treo khi vật qua vị trí có li độ góc <img src="https://latex.codecogs.com/gif.latex?\small&space;\alpha=30^{0}" title="\small \alpha=30^{0}" /> xấp xỉ bằng 1,598 (N).$
				~Khi qua vị trí cân bằng tốc độ của vật lớn nhất là <img src="https://latex.codecogs.com/gif.latex?\small&space;\sqrt{10}" title="\small \sqrt{10}" /> m/s@
			<!--Câu 38--> Đoạn mạch AB gồm hai đoạn mạch AM và MB mắc nối tiếp. Đoạn mạch AM gồm điện trở thuần R<sub>1</sub>
= 40Ω  mắc nối tiếp với tụ điện có diện dụng 10<sup>-3</sup>/4π F, đoạn mạch MB gồm điện trở thuần R<sub>2</sub> mắc nối tiếp
với cuộn cảm thuần. Đặt vào A, B điện áp xoay chiều có giá trị hiệu dụng và tần số không đổi thì điện áp tức
thời ở hai đầu đoạn mạch AM và MB lần lượt là :<img src="https://latex.codecogs.com/gif.latex?\small&space;u_{AM}=50\sqrt{2}\cos&space;(100\pi&space;t-\frac{7\pi&space;}{12})(V)" title="\small u_{AM}=50\sqrt{2}\cos (100\pi t-\frac{7\pi }{12})(V)" /> và <img src="https://latex.codecogs.com/gif.latex?\small&space;u_{MB}=150\cos&space;(100\pi&space;t)(V)" title="\small u_{MB}=150\cos (100\pi t)(V)" />. Hệ
số công suất của đoạn mạch AB là$
				0,86$
				0,71$
				~0,84$
				0,91@
			<!--Câu 39-->Lăng kính có thiết diện là tam giác có góc chiết quang A đặt trong không khí. Biết chiết suất của lăng
kính là <img src="https://latex.codecogs.com/gif.latex?\small&space;n=\sqrt{3}" title="\small n=\sqrt{3}" /> . Chiếu một tia sáng đơn sắc tới mặt bên thứ nhất và cho tia ló ra khỏi mặt bên thứ hai. Biết góc
lệch cực tiểu của tia sáng qua lăng kính bằng góc chiết quang. Tìm góc chiết quang.$
				~60<sup>0</sup>$
				90<sup>0</sup>$
				45<sup>0</sup>$
				30<sup>0</sup>@
			<!--Câu 40-->Đặt điện áp xoay chiều u = U<sub>0</sub>cosωt vào hai đầu đoạn mạch mắc nối tiếp gồm điện trở, cuộn cảm
thuần và tụ điện có điện dung C thay đổi được. Ban đầu, khi C = C<sub>0</sub> thì điện áp hiệu dụng ở hai đầu tụ điện đạt
giá trị cực đại 100V. Tăng giá trị điện dung C đến khi điện áp hiệu dụng ở hai đầu tụ điện bằng 50V thì cường
độ dòng điện trong mạch trễ pha so với hiệu điện thế hai đầu đoạn mạch là 15<sup>0</sup>. Tiếp tục tăng giá trị điện dung
C đến khi điện áp hiệu dụng ở hai đầu tụ điện bằng 40V. Khi đó, điện áp hiệu dụng ở hai đầu cuộn cảm thuần
có giá trị gần nhất với giá trị nào sau đây?$
				~66 V$
				62 V$
				70 V$
				54 V
			</div>	
			<!--Câu 1-->Một kính lúp có tiêu cự f = 5 cm. Người quan sát mắt không có tật, có khoảng nhìn rõ ngắn nhất Đ =
25cm. Số bội giác của kính lúp khi người đó ngắm chừng ở vô cực bằng:$
				~5$
				30$
				125$
				25@
			<!--Câu 2--> Một từ trường đều có phương thẳng đứng, hướng xuống. Hạt α là hạt nhân nguyên tử He chuyển động
theo hướng Bắc địa lý bay vào từ trường trên. Lực Lorenxơ tác dụng lên α có hướng$
				Đông$
				~Tây$
				Đông – Bắc$
				Nam@								
			<!--Câu 3-->Trên sợi dây đàn hai đầu cố định, dài l = 100 cm, đang xảy ra sóng dừng. Cho tốc độ truyền sóng trên
dây đàn là 450 m/s. Tần số âm cơ bản do dây đàn phát ra bằng$
				200 Hz$
				250 Hz$
				~225 Hz$
				275 Hz@
			<!--Câu 4-->Một con lắc lò xo treo thẳng đứng, độ cứng k = 100 N/m, vật nặng khối lượng m = 500g. Khi vật cân
bằng lò xo dãn:$
				2 cm$
				2,5 cm$
				4 cm$
				~5 cm@
			<!--Câu 5-->Tốc độ cực đại của dao động điều hòa có biên độ A và tần số góc <img src="https://latex.codecogs.com/gif.latex?\small&space;\omega" title="\small \omega" /> là$
				<img src="https://latex.codecogs.com/gif.latex?\small&space;\omega&space;A^{2}" title="\small \omega A^{2}" />$
				<img src="https://latex.codecogs.com/gif.latex?\small&space;\omega^{2}&space;A" title="\small \omega^{2} A" />$
				<img src="https://latex.codecogs.com/gif.latex?\small&space;(\omega&space;A)^{2}" title="\small (\omega A)^{2}" />$
				~<img src="https://latex.codecogs.com/gif.latex?\small&space;\omega&space;A" title="\small \omega A" />@
			<!--Câu 6-->Trong thí nghiệm giao thoa sóng trên mặt nước với hai nguồn cùng pha S<sub>1</sub>, S<sub>2</sub>. O là trung điểm của
S<sub>1</sub>S<sub>2</sub>. Xét trên đoạn S<sub>1</sub>S<sub>2</sub>: tính từ trung trực của S<sub>1</sub>S<sub>2</sub> (không kể O) thì M là cực đại thứ 5, N là cực tiểu thứ 5.
Nhận định nào sau đây là đúng?$
				NO > MO$
				NO MO$
				~NO < MO$
				NO = MO@
			<!--Câu 7-->Mắt không có tật là mắt$
				khi không điều tiết có tiêu điểm nằm trước màng lưới$
				~khi không điều tiết có tiêu điểm nằm trên màng lưới$
				khi quan sát ở điểm cực cận mắt không phải điều tiết$
				khi quan sát ở điểm cực viễn mắt phải điều tiết@
			<!--Câu 8-->Một vật dao động điều hòa có chu kỳ T. Thời gian ngắn nhất vật chuyển động từ vị trí biên về vị trí gia
tốc có độ lớn bằng một nửa độ lớn cực đại là:$
				T/8$
				T/4$
				T/12$
				~T/6@
			<!--Câu 9-->Cho đoạn mạch gồm điện trở thuần R, tụ điện C và cuộn dây thuần cảm L mắc nối tiếp. Điện áp hai
đầu mạch <img src="https://latex.codecogs.com/gif.latex?\small&space;u=U\sqrt{2}\cos&space;(\omega&space;t&plus;\varphi&space;)" title="\small u=U\sqrt{2}\cos (\omega t+\varphi )" /> và dòng điện trong mạch <img src="https://latex.codecogs.com/gif.latex?\small&space;i=I\sqrt{2}\cos&space;(\omega&space;t)" title="\small i=I\sqrt{2}\cos (\omega t)" />. Biểu thức nào sau đây về tính công
suất tiêu thụ của đoạn mạch là KHÔNG đúng?$
				<img src="https://latex.codecogs.com/gif.latex?\small&space;P=\frac{U^{2}}{R}\cos&space;^{2}&space;\varphi" title="\small P=\frac{U^{2}}{R}\cos ^{2} \varphi" />$
				~<img src="https://latex.codecogs.com/gif.latex?\small&space;P=\frac{U^{2}\cos&space;\varphi&space;}{R}" title="\small P=\frac{U^{2}\cos \varphi }{R}" />$
				<img src="https://latex.codecogs.com/gif.latex?\small&space;P=RI^{2}" title="\small P=RI^{2}" />$
				<img src="https://latex.codecogs.com/gif.latex?\small&space;P=UI\cos&space;\varphi" title="\small P=UI\cos \varphi" />@
			<!--Câu 10--> Dòng điện Phu-cô là$
				~dòng điện cảm ứng sinh ra trong khối vật dẫn khi khối vật dẫn chuyển động cắt các đường sức từ.$
				 dòng điện chạy trong khối vật dẫn$
				dòng điện cảm ứng sinh ra trong mạch kín khi từ thông qua mạch biến thiên$
				dòng điện xuất hiện trong tấm kim loại khi nối tấm kim loại với hai cực của nguồn điện@
			<!--Câu 11-->Một vật dao động điều hòa chuyển động từ biên về vị trí cân bằng. Nhận định nào là đúng?$
				Vật chuyển động nhanh dần đều.$
				~Vận tốc và lực kéo về cùng dấu$
				Tốc độ của vật giảm dần$
				 Gia tốc có độ lớn tăng dần.@
			<!--Câu 12-->Cho đoạn mạch gồm điện trở thuần R = 40 Ω, tụ điện có C = 10<sup>-3</sup>/6π F và cuộn dây thuần cảm có L =
1/π H mắc nối tiếp. Điện áp hai đầu mạch <img src="https://latex.codecogs.com/gif.latex?\small&space;u=120\cos&space;(100\pi&space;t&plus;\frac{\pi&space;}{3})" title="\small u=120\cos (100\pi t+\frac{\pi }{3})" />(V). Biểu thức cường độ dòng điện trong
mạch:$
				~<img src="https://latex.codecogs.com/gif.latex?\small&space;i=1,5\sqrt{2}\cos&space;(100\pi&space;t&plus;\frac{\pi&space;}{12})(A)" title="\small i=1,5\sqrt{2}\cos (100\pi t+\frac{\pi }{12})(A)" />$
				<img src="https://latex.codecogs.com/gif.latex?\small&space;i=3\cos&space;(100\pi&space;t&plus;\frac{\pi&space;}{12})(A)" title="\small i=3\cos (100\pi t+\frac{\pi }{12})(A)" />$
				<img src="https://latex.codecogs.com/gif.latex?\small&space;i=3\sqrt{2}\cos&space;(100\pi&space;t&plus;\frac{\pi&space;}{4})(A)" title="\small i=3\sqrt{2}\cos (100\pi t+\frac{\pi }{4})(A)" />$
				<img src="https://latex.codecogs.com/gif.latex?\small&space;i=1,5\sqrt{2}\cos&space;(100\pi&space;t&plus;\frac{\pi&space;}{4})(A)" title="\small i=1,5\sqrt{2}\cos (100\pi t+\frac{\pi }{4})(A)" />@
			<!--Câu 13-->Một sóng truyền trên mặt nước có bước sóng <img src="https://latex.codecogs.com/gif.latex?\small&space;\lambda" title="\small \lambda" />. M và N là hai đỉnh sóng nơi sóng truyền qua. Giữa
M, N có 1 đỉnh sóng khác. Khoảng cách từ vị trí cân bằng của M đến vị trí cân bằng của N bằng:$
				~<img src="https://latex.codecogs.com/gif.latex?\small&space;2\lambda" title="\small 2\lambda" />$
				<img src="https://latex.codecogs.com/gif.latex?\small&space;3\lambda" title="\small 3\lambda" />$
				<img src="https://latex.codecogs.com/gif.latex?\small&space;\lambda" title="\small \lambda" />$
				<img src="https://latex.codecogs.com/gif.latex?\small&space;\lambda/2" title="\small \lambda/2" />@
			<!--Câu 14-->Đặt điện áp xoay chiều <img src="https://latex.codecogs.com/gif.latex?\small&space;u=120\sqrt{2}\cos&space;(100\pi&space;t&plus;\frac{\pi&space;}{6})" title="\small u=120\sqrt{2}\cos (100\pi t+\frac{\pi }{6})" />(V) vào hai đầu đoạn mạch chỉ có tụ điện C = 10<sup>-4</sup>/π F. Dòng điện qua tụ có biểu thức:$
				~<img src="https://latex.codecogs.com/gif.latex?\small&space;i=1,2\sqrt{2}\cos&space;(100\pi&space;t&plus;\frac{2\pi&space;}{3})(A)" title="\small i=1,2\sqrt{2}\cos (100\pi t+\frac{2\pi }{3})(A)" />$
				<img src="https://latex.codecogs.com/gif.latex?\small&space;i=1,2\cos&space;(100\pi&space;t-\frac{2\pi&space;}{3})(A)" title="\small i=1,2\cos (100\pi t-\frac{2\pi }{3})(A)" />$
				<img src="https://latex.codecogs.com/gif.latex?\small&space;i=1,2\sqrt{2}\cos&space;(100\pi&space;t&plus;\frac{\pi&space;}{2})(A)" title="\small i=1,2\sqrt{2}\cos (100\pi t+\frac{\pi }{2})(A)" />$
				<img src="https://latex.codecogs.com/gif.latex?\small&space;i=1,2\cos&space;(100\pi&space;t-\frac{\pi&space;}{2})(A)" title="\small i=1,2\cos (100\pi t-\frac{\pi }{2})(A)" />@
			<!--Câu 15-->Một vật chịu tác dụng của một ngoại lực cưỡng bức điều hòa F = 5cos4πt (N). Biên độ dao động của
vật đạt cực đại khi vật có tần số dao động riêng bằng:$
				2π Hz$
				4 Hz$
				4π Hz$
				~2 Hz@
			<!--Câu 16-->Cho 3 loại đoạn mạch: chỉ có điện trở thuần, chỉ có tụ điện, chỉ có cuộn dây thuần cảm. Đoạn mạch
nào tiêu thụ công suất khi có dòng điện xoay chiều chạy qua?$
				chỉ có tụ điện và chỉ có cuộn dây thuần cảm$
				~chỉ có điện trở thuần$
				 chỉ có tụ điện$
				 chỉ có cuộn dây thuần cảm@
			<!--Câu 17-->Một khung dây có diện tích S đặt trong từ trường đều có cảm ứng từ B sao cho mặt phẳng khung dây
vuông góc với đường sức từ. Gọi <img src="https://latex.codecogs.com/gif.latex?\small&space;\phi" title="\small \phi" /> là từ thông gửi qua khung dây. Độ lớn của <img src="https://latex.codecogs.com/gif.latex?\small&space;\phi" title="\small \phi" /> bằng:$
				 0,5.B.S$
				2B.S$
				~B.S$
				-B.S@
			<!--Câu 18-->Độ cao của âm là đặc trưng sinh lý được quyết định bởi đặc trưng vật lý của âm là$
				Biên độ âm$
				Mức cường độ âm$
				~Tần số âm$
				Cường độ âm@
			<!--Câu 19--> Cho đoạn mạch gồm điện trở thuần R, tụ điện C và cuộn dây thuần cảm L mắc nối tiếp. Z là tổng trở
của mạch. Điện áp hai đầu mạch <img src="https://latex.codecogs.com/gif.latex?\small&space;u=U_{o}\cos&space;(\omega&space;t&plus;\varphi&space;)" title="\small u=U_{o}\cos (\omega t+\varphi )" /> và dòng điện trong mạch <img src="https://latex.codecogs.com/gif.latex?\small&space;i=I_{o}\cos&space;(\omega&space;t)" title="\small i=I_{o}\cos (\omega t)" />. Điện áp tức thời và
biên độ hai đầu R, L, C lần lượt là u<sub>R</sub>, u<sub>L</sub>, u<sub>C</sub> và U<sub>0R</sub>, U<sub>0L</sub>, U<sub>0C</sub>. Biểu thức nào là đúng?$
				<img src="https://latex.codecogs.com/gif.latex?\small&space;\frac{u_{C}^{2}}{U_{OC}^{2}}&plus;\frac{u_{L}^{2}}{U_{OL}^{2}}=1" title="\small \frac{u_{C}^{2}}{U_{OC}^{2}}+\frac{u_{L}^{2}}{U_{OL}^{2}}=1" />$
				<img src="https://latex.codecogs.com/gif.latex?\small&space;\frac{u^{2}}{U_{O}^{2}}&plus;\frac{u_{L}^{2}}{U_{OL}^{2}}=1" title="\small \frac{u^{2}}{U_{O}^{2}}+\frac{u_{L}^{2}}{U_{OL}^{2}}=1" />$
				~<img src="https://latex.codecogs.com/gif.latex?\small&space;\frac{u_{R}^{2}}{U_{OR}^{2}}&plus;\frac{u_{C}^{2}}{U_{OC}^{2}}=1" title="\small \frac{u_{R}^{2}}{U_{OR}^{2}}+\frac{u_{C}^{2}}{U_{OC}^{2}}=1" />$
				<img src="https://latex.codecogs.com/gif.latex?\small&space;\frac{u_{R}^{2}}{U_{OR}^{2}}&plus;\frac{u^{2}}{U_{o}^{2}}=1" title="\small \frac{u_{R}^{2}}{U_{OR}^{2}}+\frac{u^{2}}{U_{o}^{2}}=1" />@
			<!--Câu 20-->Một đoạn dây dài l = 50cm mang dòng điện cường độ I = 5A được đặt trong từ trường đều có cảm
ứng từ B = 0,2T, sao cho đoạn dây dẫn vuông góc với đường sức từ. Độ lớn lớn từ tác dụng lên đoạn dây dẫn
bằng:$
				0,2 N$
				0,4 N$
				0,3 N$
				~0,5 N@
			<!--Câu 21-->Kẻ trộm giấu viên kim cương ở dưới đáy một bể bơi. Anh ta
đặt chiếc bè mỏng đồng chất hình tròn bán kính R trên mặt nước, tâm
của bè nằm trên đường thẳng đứng đi qua viên kim cương. Mặt nước
yên lặng và mức nước là h = 2,0 m. Cho chiết suất của nước là n= 4/3.
Giá trị nhỏ nhất của R để người ở ngoài bể bơi không nhìn thấy viên
kim cương gần đúng bằng:
			<br><img src="https://scontent.fsgn5-4.fna.fbcdn.net/v/t1.15752-9/64565420_684840291958786_7781028637679026176_n.png?_nc_cat=104&_nc_oc=AQm-miNYvs-cEE2zteRUw_pJVaQuZVeXkIMYPSwes_lIYm0t6sCftPAXPabXR0qdHVY&_nc_ht=scontent.fsgn5-4.fna&oh=3299980d038d1baf679299679b523e85&oe=5D910BDA"/>$
				3,40 m$
				~2,27 m$
				2,83 m$
				2,58 m@
			<!--Câu 22-->Một vật nhỏ dao động điều hòa dọc theo trục Ox. Khi vật cách vị trí cân bằng một đoạn 2 cm thì
động năng của vật là 0,48 J. Khi vật cách vị trí cân bằng một đoạn 6 cm thì động năng của vật là 0,32 J. Biên
độ dao động của vật bằng$
				12 cm$
				~10 cm$
				14 cm$
				8 cm@
			<!--Câu 23-->Sóng dừng hình thành trên một sợi dây đàn hồi OB, với
đầu phản xạ B cố định và tốc độ lan truyền v = 400cm/s. Hình ảnh
sóng dừng như hình vẽ. Sóng tới tại B có biên độ A = 2cm, thời
điểm ban đầu hình ảnh sợi dây là đường (1), sau đó các khoảng
thời gian là 0,005 s và 0,015 s thì hình ảnh sợi dây lần lượt là (2)
và (3). Biết x<sub>M</sub> là vị trí phần tử M của sợi dây lúc sợi dây duỗi
thẳng. Khoảng cách xa nhất giữa M tới phần tử sợi dây có cùng
biên độ với M là<br><img src="https://scontent.fsgn5-2.fna.fbcdn.net/v/t1.15752-9/62349956_303303067278301_240408445046489088_n.png?_nc_cat=105&_nc_oc=AQm5UToo0zT1pnyvbT_aEK_n4NyS7tpcnCvuu7ZUeSTbeycwpM74ecAL8zz5d9yXUn0&_nc_ht=scontent.fsgn5-2.fna&oh=d219387ad098bbcc5e0754c4c1fd3b63&oe=5D81E17A"/>$
				24 cm$
				28 cm$
				~24,66 cm$
				28,56 cm@
			<!--Câu 24-->Đặt điện áp <img src="https://latex.codecogs.com/gif.latex?\small&space;u=180\sqrt{2}\cos&space;\omega&space;t" title="\small u=180\sqrt{2}\cos \omega t" />(V) (với <img src="https://latex.codecogs.com/gif.latex?\small&space;\omega" title="\small \omega" /> không đổi) vào hai đầu đoạn mạch AB gồm đoạn mạch AM
nối tiếp đoạn mạch MB. Đoạn mạch AM có điện trở thuần R, đoạn mạch MB có cuộn cảm thuần có độ tự cảm
L thay đổi được và tụ điện có điện dung C mắc nối tiếp. Điện áp hiệu dụng ở hai đầu đoạn mạch AM và độ
lớn góc lệch pha của cường độ dòng điện so với điện áp u khi L=L<sub>1</sub> là U và <img src="https://latex.codecogs.com/gif.latex?\small&space;\varphi&space;_{1}" title="\small \varphi _{1}" />, còn khi L = L<sub>2</sub> thì tương ứng
là 3 U và <img src="https://latex.codecogs.com/gif.latex?\small&space;\varphi&space;_{2}" title="\small \varphi _{2}" />.Biết <img src="https://latex.codecogs.com/gif.latex?\small&space;\varphi&space;_{1}&plus;\varphi&space;_{2}=90^{0}" title="\small \varphi _{1}+\varphi _{2}=90^{0}" />. Giá trị U bằng$
				60V$
				180V$
				~90V$
				135V@
			<!--Câu 25--> Hình vẽ nào sau đây xác định đúng chiều dòng điện cảm ứng khi cho vòng dây dịch chuyển lại gần
hoặc ra xa nam châm:$
				<img src="https://scontent.fsgn5-5.fna.fbcdn.net/v/t1.15752-9/64409394_341369730092607_8966297362058182656_n.png?_nc_cat=100&_nc_oc=AQlaoXoe7tQBPj17bVqZ11kvRJceBDlUSlHHbUfKvXivAw8ALam-6HTiy96vZyz5yDc&_nc_ht=scontent.fsgn5-5.fna&oh=1ec0b01567269d7f33d63f5ea60d57ba&oe=5D8AFEC2"/>$
				<img src="https://scontent.fsgn5-5.fna.fbcdn.net/v/t1.15752-9/62652184_886669328346231_644499820320391168_n.png?_nc_cat=100&_nc_oc=AQkKpVqKuBYmlCT9Pe0PsMAGqcrB6SFqtT3JyFv0Yx3PxvSZGp96ls5QcRroDSjzl4w&_nc_ht=scontent.fsgn5-5.fna&oh=3d8c04e91e8fbf8ba553c98add044c2d&oe=5D918773"/>$
				<img src="https://scontent.fsgn5-3.fna.fbcdn.net/v/t1.15752-9/62264649_313240506218044_211126079576866816_n.png?_nc_cat=111&_nc_oc=AQmBmWq9KBcz4DPQJR7ShHvbMLiKQ9XLoZ_m7lDZzSVr2l9p328-r54rCZ-BS7HjQ1s&_nc_ht=scontent.fsgn5-3.fna&oh=ced3a082c8aa4b7832dd2ff1ecbe0421&oe=5D83BA91"/>$
				~<img src="https://scontent.fsgn5-1.fna.fbcdn.net/v/t1.15752-9/64486568_658336537961498_4833709649804918784_n.png?_nc_cat=101&_nc_oc=AQl-DMFgiwJPWihlQv538ytHD3UsQYqr9Xdov6MhGT_OMk9WFbRn7dQWBmpjYHL2dl0&_nc_ht=scontent.fsgn5-1.fna&oh=dd5d3f4492ed3d1f52c63ed85105b094&oe=5D9F0EC2"/>@
			<!--Câu 26-->Ở mặt nước, một nguồn sóng đặt tại O dao động điều hòa theo phương thẳng đứng. Sóng truyền trên
mặt nước với bước sóng λ. M và N là hai điểm ở mặt nước sao cho OM = 6λ, ON = 8λ và OM vuông góc với
ON. Trên đoạn thẳng MN, số điểm mà tại đó các phần tử nước dao động ngược pha với dao động của nguồn
O là$
				~4$
				5$
				3$
				6@
			<!--Câu 27-->Đặt điện áp <img src="https://latex.codecogs.com/gif.latex?\small&space;u=U_{o}\cos&space;(\omega&space;t&plus;\frac{\pi&space;}{3})" title="\small u=U_{o}\cos (\omega t+\frac{\pi }{3})" /> vào hai đầu đoạn mạch gồm điện trở thuần, cuộn cảm thuần và tụ
điện mắc nối tiếp. Biết cường độ dòng điện trong mạch có biểu thức <img src="https://latex.codecogs.com/gif.latex?\small&space;i=\sqrt{6}\cos&space;(\omega&space;t&plus;\frac{\pi&space;}{6})" title="\small i=\sqrt{6}\cos (\omega t+\frac{\pi }{6})" />(A) và công suất tiêu
thụ của đoạn mạch bằng 150 W. Giá trị U<sub>0</sub> bằng$
				120 V$
				<img src="https://latex.codecogs.com/gif.latex?\small&space;100\sqrt{3}V" title="\small 100\sqrt{3}V" />$
				100 V$
				~<img src="https://latex.codecogs.com/gif.latex?\small&space;100\sqrt{2}V" title="\small 100\sqrt{2}V" />@
			<!--Câu 28-->Một sóng âm truyền trong không khí. Mức cường độ âm tại điểm M và tại điểm N lần lượt là 20 dB
và 60 dB. Cường độ âm tại N lớn hơn cường độ âm tại M$
				1000 lần$
				 ~10000 lần$
				3 lần$
				40 lần@
			<!--Câu 29-->Một con lắc đơn gồm dây treo có chiều dài 1m và vật nhỏ có khối lượng 100g mang điện tích 2.10<sup>-5</sup>
C. Treo con lắc đơn này trong điện trường đều với vectơ cường độ điện trường hướng theo phương ngang và
có độ lớn 5.10<sup>4</sup> V/m. Trong mặt phẳng thẳng đứng đi qua điểm treo và song song với vectơ cường độ điện
trường, kéo vật nhỏ theo chiều của vectơ cường độ điện trường sao cho dây treo hợp với vectơ gia tốc trong
trường <img src="https://latex.codecogs.com/gif.latex?\small&space;\bar{g}" title="\small \bar{g}" /> một góc 55<sup>o</sup> rồi buông nhẹ cho con lắc dao động điều hòa. Lấy g = 10 m/s<sup>2</sup>. Trong quá trình dao
động, tốc độ cực đại của vật nhỏ là$
				~0,66 m/s$
				0,50 m/s$
				2,87 m/s$
				3,41 m/s@
			<!--Câu 30-->Ở mặt chất lỏng có 2 nguồn kết hợp đặt tại A và B dao động điều hòa, cùng pha theo phương thẳng
đứng. Ax là nửa đường thẳng nằm ở mặt chất lỏng và vuông góc với AB. Trên Ax có những điểm mà các
phần tử ở đó dao động với biên độ cực đại, trong đó M là điểm xa A nhất, N là điểm kế tiếp với M, P là điểm
kế tiếp với N và Q là điểm gần A nhất. Biết MN = 22,25 cm; NP = 8,75 cm. Độ dài đoạn QA <b>gần nhất</b> với giá
trị nào sau đây?$
				3,1 cm$
				4,2 cm$
				~2,1 cm$
				1,2 cm@
			
			<div id="countDown">	
				<p>Thời gian còn lại:</p>
				<p class="minute">Phút</p>
				<p class="second">Giây<p>
				
			<div>
			<script type="text/javascript">
				function getCookie(name){
					var cname = name + "=";
					var dc = document.cookie;
					if (dc.length > 0) {
						begin = dc.indexOf(cname);
						if (begin != -1) {
							begin += cname.length;
							end = dc.indexOf(";", begin);
						if (end == -1) end = dc.length;
						return unescape(dc.substring(begin, end));
						}
					}
					return null;
				}
				if(document.cookie && document.cookie.match('myClock_Ly3')){
				  // get deadline value from cookie
				  var deadline = getCookie('myClock_Ly3');
				}
				 
				
				else{
				  // create deadline 50 minutes from now
					var currentTime = Date.parse(new Date());
					var deadline = Date.parse(new Date(currentTime + 50*60*1000));
				 
				  // store deadline in cookie for future reference
				  document.cookie = 'myClock_Ly3=' + deadline + '; path=/';
				}
							
			
			
				
				var currentTime = Date.parse(new Date());
				
				var t=deadline-currentTime;
				var second = Math.floor((t / 1000) % 60);
				var minute = Math.floor(t / 1000 / 60);
				
				
				
			
				var nd = $("#noi_dung_de").html();
				var cau_hoi = nd.split('@');
				var html = ""; var tra_loi_dung = 0;
				for(var i = 0;i<cau_hoi.length;i++)
				{
					var chi_tiet = cau_hoi[i].split('$');
					//alert(chi_tiet[i]);
					for(var j=0;j<chi_tiet.length;j++)
					{
			 
						if (j==0) html += "<tr><td><b>Câu " + (i+1) + ":</b></td><td> <b>" + chi_tiet[j].trim() + "</b></td></tr>";
						else 
						{
							html += "<tr><td></td><td id='" + (i + 1 + String.fromCharCode(64 + j))+"'><input type='radio' name='" + (i + 1 )+ "' id='" + (i + 1 + String.fromCharCode(64 + j))+"' value='" + chi_tiet[j].trim() +"'> " + String.fromCharCode(64 + j) + ". " + chi_tiet[j].replace("~","").trim() + "</td></tr>";
						}
					}
				}

				$("#noi_dung_de").empty().append("<table>" + html + "</table><input type='submit' value='Nộp bài' id='tra_loi_xong' style='margin-left:585px;'>").fadeIn();
				$("#noi_dung_de input").click(function(){   
				//Lấy id của radio
				var id = $(this).attr("id");
				for (var j=1;j<5;j++){
					var newid=id.substr(0,1)+String.fromCharCode(64 + j);
					
					$("td#" + newid).css("background-color","");
				}
				$("td#" + id).css("background-color","yellow");
				
				});   
				
2				
				
3				
4				
			
				
				/*jQuery(document).ready(function($) {
					
					setTimeout(function(){
						
						$('#tra_loi_xong').trigger( "click" ) ;
						}, time);
				}); */
				
				
				
			
			
				$("#tra_loi_xong").click(function(){
					$('#noi_dung_de input').each(function () {
					var id = $(this).attr("id");
					var ctl = $(this).val();
				 
					//Hiển thị câu đúng với nền là màu đỏ
					if (ctl[0] == '~') 
					{
						//alert($("td#" + id).css("background-color"));
						if ($("td#" + id).css("background-color") == "rgb(255, 255, 0)")
						{
							tra_loi_dung++;
						}
						else $("td#" + id).css("background-color","red");     
					}
				});
				$("#noi_dung_de").append("<p style='text-align:center'>Số câu đúng :" + (tra_loi_dung < 0 ? "0" : tra_loi_dung) + " câu. Điểm : " + tra_loi_dung*0.25+ "</p>");
				$("#tra_loi_xong").fadeOut();
				
				});
				
				var timer = setInterval(function() {
					
				   $('.minute').text(minute +" Phút")
				   $('.second').text(second-- +" Giây");
				   if (second == -1) {
						minute--;
						second=60;
				   }
				   if (minute ==-1)  {
						 alert("Đã hết giờ làm bài, kiểm tra kết quả");
					  $('#tra_loi_xong').trigger( "click" ) ;
					  $('.second').fadeOut("slow");
					  $('.minute').fadeOut("slow");
					  clearInterval(timer);
				   }
				   
				}, 1000);
				
			</script>
			
		</div>
	</body>
</html>