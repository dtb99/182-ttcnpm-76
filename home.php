<?php
include 'UserID.php';
$user = new UserID();
?>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>Luyện Thi Đại Học</title>
    <link rel="stylesheet" type="text/css" href="style.css"/>

</head>
<body>
<div id="page">
    <div id="header">
        <div id="section">
            <?php if($user->isLogin()){
                ?>
                <p> Hello <?= $user->getSESSION("firstname")." ".$user->getSESSION("name") ?> <a href="logout.php">Logout</a></p>

            <?php }
            ?>
        </div>
        <?php if(!$user->isLogin()){ ?>
            <div id="section">
                <a id="login" href="login.php"><button>ĐĂNG NHẬP</button></a>
                <a id="creat" href="register.php"><button>ĐĂNG KÍ</button></a>
            </div>
        <?php } ?>
        <ul>
            <li class="current"><a href="home.php">Trang chủ</a></li>
            <li><a href="testExam.php">THI THỬ</a></li>
            <li><a href="documents.php">TÀI LIỆU</a></li>
            <li><a href="news.php">TIN TỨC</a></li>
            <?php if ($user->isLogin()) {
            if ($user->getSESSION("username") == "admin") {
            ?>
                <li><a href="Search.php">TÌM KIẾM USERS</a></li>
                <li><a href="upFile.php">TẢI LÊN DỮ LIỆU</a></li>
            <?php } } ?>
        </ul>
    </div>
    <div id="content">

        <div id="countdown">
            <h1>Đếm ngược đến lúc thi</h1>
            <div id="clockdiv">
                <div>
                    <span class="days"></span>
                    <div class="smalltext">Ngày</div>
                </div>
                <div>
                    <span class="hours"></span>
                    <div class="smalltext">Giờ</div>
                </div>
                <div>
                    <span class="minutes"></span>
                    <div class="smalltext">Phút</div>
                </div>
                <div>
                    <span class="seconds"></span>
                    <div class="smalltext">Giây</div>
                </div>
            </div>


            <script src="js/index.js"></script>

        </div>
        <div id="examinfo">
            <span style="color:#ffd400ed;font-size:35px;font-weight:bold;text-shadow: 4px 4px 4px #cc0000;">______Thông tin </span>
            <span style="color:#ff00eb;font-size:35px;font-weight:bold;text-shadow: 4px 4px 4px #1f00cc">kì thi______</span>
            <div id="info">
                <div class="box time">
                    <h1>Thời gian thi</h1>
                    <p>Theo hướng dẫn tổ chức kỳ thi THPT quốc gia và xét công nhận tốt nghiệp THPT năm 2019 được Bộ
                        Giáo dục và Đào tạo công bố ngày 28/3, kỳ thi THPT quốc gia năm nay sẽ diễn ra từ ngày 24 đến
                        27/6, như năm 2018.</p>
                    <a href="https://vnexpress.net/giao-duc/bo-giao-duc-cong-bo-lich-thi-thpt-quoc-gia-nam-2019-3901565.html"
                       target="_blank">Xem thông tin về lịch thi cụ thể</a>
                </div>
                <div class="box score">
                    <h1>Các môn thi</h1>
                    <p>Kỳ thi THPT Quốc gia 2019 gồm 5 bài thi, bao gồm: Toán, Ngữ Văn, Ngoại Ngữ và 2 bài thi tổ hợp là
                        Khoa học Tự nhiên(Vật Lý, Hóa học, Sinh Học) và Khoa học xã hội(Lịch sử, địa lý, Giáo dục công
                        dân).</p>
                    <a href="https://news.zing.vn/cach-tinh-diem-xet-tot-nghiep-thpt-quoc-gia-2019-post914387.html"
                       ;target="_blank">Xem thêm thông tin về cách tính điểm tốt nghiệp</a>
                </div>
            </div>
        </div>
        <div id="subject">
            <a style="text-decoration:none;margin-bottom:20px" href=""><span>Các môn học</span></a>
            <div id="subject-list">
                <div class="sbox math">
                    <img src="images/toan.jpg">
                    <p>Toán</p>
                </div>
                <div class="sbox physic">
                    <img src="images/ly.jpg">
                    <p>Lý</p>
                </div>
                <div class="sbox chemical">
                    <img src="images/hoa.jpg">
                    <p>Hóa</p>
                </div>
                <div class="sbox biology">
                    <img src="images/sinh.jpg">
                    <p>Sinh</p>
                </div>
                <div class="sbox english">
                    <img src="images/anh.jpg">
                    <p>Tiếng Anh</p>
                </div>
                <div class="sbox history">
                    <img src="images/su.jpg">
                    <p>Sử</p>
                </div>
                <div class="sbox geography">
                    <img src="images/dia.jpg">
                    <p>Địa</p>
                </div>
                <div class="sbox gdcd">
                    <img src="images/gdcd.jpg">
                    <p>Giáo dục công dân</p>
                </div>
            </div>
        </div>

    </div>

    <div id="footer">
        <div>
            <div id="connect">
                <a href="https://www.facebook.com/groups/644703785954707/?multi_permalinks=645737145851371&notif_id=1555690841219669&notif_t=group_activity"
                   target="_blank"><img src="images/icon-facebook.gif" alt="Facebook"/></a>
            </div>
            <div id="contact">
                <p>0333 879 987 || nhan.tran1245@hcmut.edu.vn</p>
            </div>
        </div>
    </div>
</div>

</body>
</html>
