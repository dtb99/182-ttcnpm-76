<!DOCTYPE html>
<?php
include 'UserID.php';
$user = new UserID();
if(!$user->isLogin()) header('Location:login.php');
?>
<html>
	<head>
		<meta charset="UTF-8" />
		<title>Đề THPTQG môn Sử</title>
		<link rel="stylesheet" type="text/css" href="examstyle.css" />
		
		
	</head>
	<body>
		<h1>Đề thi thử THPTQG 2019</h1>
		<p align="center"> <b> Môn: Lịch sử </b> </p>
		<p id="time">Thời gian: 50 phút</p>		
		<script src='examform.js'></script>
		<div id="content">
			<div id="noi_dung_de" style='display:none;'>
				
			<!--Câu 1-->Nguyên nhân khách quan dẫn đến thắng lợi của cách mạng Tháng Tám năm 1945 là do:;
				sự lãnh đạo của Đảng cộng sản Đông Dương;
				[thắng lợi của quân đồng minh với chủ nghĩa phát xít;
				sức mạnh của khối đại đoàn kết dân tộc;
				nghệ thuật khởi nghĩa linh hoạt, sáng tạo.|
			<!--Câu 2-->Ngày 12/4/1944, Hồ Chí Minh viết: <i>“Cuộc kháng chiến của ta là một cuộc kháng chiến toàn dân nên
phải động viên toàn dân, vũ trang toàn dân”</i>. <br>Cuộc kháng chiến trên đây diễn ra trong bối cảnh nào?</br>;
				[Khi nhân dân Việt Nam chưa có chính quyền cách mạng;
				Khi nhân dân Việt Nam đã có chính quyền cách mạng;
				Khi nước Việt Nam mới đang dần hình thành;
				Khi các lực lượng đồng minh đang chuẩn bị vào Việt Nam|					
				
			<!--Câu 3-->Ý nghĩa quan trọng nhất của phong trào yêu nước dân chủ tư sản ở Việt Nam trong những năm 20 của
thế kỉ XX là;
				đào tạo và rèn luyện đội ngũ cán bộ cho phong trào yêu nước dân tộc dân chủ Việt Nam;
				góp phần cổ vũ mạnh mẽ tinh thần yêu nước của nhân dân Việt Nam, bồi đắp truyền thống yêu nước;
				góp phần khảo sát và thử nghiệm một con đường cứu nước mới theo khuynh hướng dân chủ tư sản;
				[chứng tỏ sự bất lực của hệ tư tưởng dân chủ tư sản, độc lập dân tộc không gắn liền với con đường tư sản|
			<!--Câu 4-->Nguyên nhân quyết định sự bùng nổ của phong trào cách mạng 1930- 1931 là;
				hậu quả của cuộc khủng hoảng kinh tế 1929- 1933;
				mâu thuẫn giữa dân tộc Việt Nam với thực dân Pháp và tay sai phát triển gay gắt;
				[Đảng cộng sản Việt Nam ra đời và lãnh đạo đấu tranh;
				những tác động của tình hình thế giới|
			<!--Câu 5-->Yếu tố nào giữ vai trò quyết định đến việc tìm đường cứu nước của Nguyễn Tất Thành?;
				[Do tinh thần yêu nước thương dân, ý chí đánh đuổi giặc Pháp của Nguyễn Tất Thành;
				Những hoạt động cứu nước của các vị tiền bối diễn ra sôi nổi nhưng đều thất bại;
				Phong trào cách mạng thế giới diễn ra mạnh mẽ cổ vũ cách mạng Việt Nam;
				Do yêu cầu của sự nghiệp giải phóng dân tộc cần tìm ra con đường cứu nước phù hợp|
			<!--Câu 6--> Đại hội đại biểu lần thứ II của Đảng Cộng sản Đông Dương (tháng 2 - 1951) là mốc đánh dấu bước phát
triển mới trong quá trình lãnh đạo và trưởng thành của Đảng và là;
				Đại hội xây dựng chủ nghĩa xã hội.;
				Đại hội kháng chiến toàn dân;
				[Đại hội kháng chiến thắng lợi;
				Đại hội xây dựng và bảo vệ Tổ quốc.|
			<!--Câu 7-->Ý nào dưới đây không phải là tính chất của cuộc kháng chiến chống Pháp của nhân dân Việt Nam
(1945- 1954)?;
				Là một cuộc chiến tranh nhân dân, chính nghĩa;
				Là một cuộc chiến tranh yêu nước, bảo vệ Tổ quốc;
				Là một cuộc cách mạng giải phóng dân tộc;
				[Là một cuộc cách mạng dân tộc dân chủ nhân dân điển hình|
			<!--Câu 8-->Ý nào dưới đây không phải là yếu tố chủ quan đảm bảo cho khởi nghĩa Yên Thế tồn tại trong thời gian
dài;
				Nghĩa quân biết dựa vào dân vừa chiến đấu vừa sản xuất;
				Nghĩa quân biết khai thác tốt địa hình, địa vật để chiến đấu lâu dài.;
				Nghĩa quân biết vận dụng chiến lược hòa hoãn để chuẩn bị cho cuộc chiến đấu lâu dài.;
				[Thực dân Pháp muốn chấm dứt xung đột để tiến hành khai thác thuộc địa ở Bắc Kì.|
			<!--Câu 9--> Cuộc vận động dân chủ 1936 – 1939 là một phong trào;
				[có tính chất dân chủ;
				chỉ mang tính dân tộc;
				không mang tính cách mạng;
				mang tính chất cải lương|
			<!--Câu 10-->Kết quả cuộc đấu tranh giành độc lập của các nước Đông Nam Á trong năm 1945 chứng tỏ;
				lực lượng vũ trang giữ vai trò quyết định;
				điều kiện khách quan giữ vai trò quyết định.;
				tầng lớp trung gian đóng vai trò nòng cốt;
				[điều kiện chủ quan giữ vai trò quyết định.|
			<!--Câu 11-->Điểm khác nhau của chiến dịch Việt Bắc thu - đông năm 1947 so với chiến dịch Biên giới thu
-đông năm 1950 của quân dân Việt Nam về;
				địa hình tác chiến;
				[loại hình chiến dịch;
				đối tượng tác chiến.;
				lực lượng chủ yếu.|
			<!--Câu 12-->Ý nghĩa then chốt của cuộc cách mạng khoa học công nghệ hiện đại là;
				làm thay đổi cơ bản các nhân tố sản xuất;
				[đưa loài người sang nền văn minh mới.;
				 thay đổi to lớn về cơ cấu dân cư, chất lượng nhân lực;
				nâng cao mức sống và chất lượng cuộc sống|
			<!--Câu 13-->Từ bài học sụp đổ của chế độ xã hội chủ nghĩa ở Liên Xô và các nước Đông Âu, cần rút ra bài học gì
trong công cuộc xây dựng chủ nghĩa xã hội ở Việt Nam?;
				Thực hiện chính sách “đóng cửa” nhằm hạn chế những ảnh hưởng từ bên ngoài.;
				Cải tổ, đổi mới về kinh tế- xã hội trước tiên, sau đó mới đến cải tổ, đổi mới về chính trị.;
				[Duy trì sự lãnh đạo của Đảng cộng sản, không chấp nhận đa nguyên chính trị.;
				Xây dựng nền kinh tế thị trường tư bản chủ nghĩa để phát triển nền kinh tế.|
			<!--Câu 14-->Một trong những tác động của phong trào giải phóng dân tộc đối với quan hệ quốc tế sau Chiến tranh
thế giới thứ hai là;
				thúc đẩy Mỹ phải chấm dứt tình trạng Chiến tranh lạnh với Liên Xô.;
				thúc đẩy các nước tư bản hòa hoãn với các nước xã hội chủ nghĩa.;
				[góp phần làm xói mòn và tan rã trật tự thế giới hai cực Ianta.;
				góp phần hình thành các liên minh kinh tế - quân sự khu vực.|
			<!--Câu 15-->Cơ hội tiêu diệt giặc sau chiến thắng Cầu Giấy (21 – 12 – 1873) của quân dân ta bị bỏ lỡ vì;
				Thực dân Pháp ngày càng củng cố dã tâm xâm chiếm toàn bộ Việt Nam.;
				[Triều đình Huế chủ động thương thuyết rồi kí với thực dân Pháp Hiệp ước 1874.;
				Triều đình Huế đàn áp các cuộc đấu tranh chống Pháp của nhân dân ta.;
				Thực dân Pháp hoang mang lo sợ và tìm mọi cách thương lượng.|
			<!--Câu 16-->Trong Cách mạng tháng Tám (1945), khởi nghĩa tại các đô thị thắng lợi có ý nghĩa quyết định nhất vì
đây là nơi;
				đặt cơ quan đầu não chỉ huy của lực lượng cách mạng.;
				[tập trung các trung tâm chính trị, kinh tế của kẻ thù.;
				 có nhiều thực dân, đế quốc.;
				có đông đảo quần chúng được giác ngộ.|
			<!--Câu 17-->Cuộc chiến tranh Đông Dương 1945- 1954 cuộc chiến tranh quốc tế giữa hai phe là do;
				chiến dịch Biên giới thu đông 1950 thắng lợi;
				hội nghị Giơnevơ được triệu tập (1954);
				 nước Việt Nam dân chủ cộng hòa ra đời (1945);
				[có sự tham gia của các cường quốc (1950)|
			<!--Câu 18-->Nguyên nhân trực tiếp làm bùng nổ phong trào Cần Vương là;
				[cuộc phản công kinh thành Huế thất bại, Tôn Thất Thuyết lấy danh nghĩa vua Hàm Nghi xuống chiếu Cần
Vương.;
				tuy triều đình Huế đã kí với Pháp hiệp ước đầu hàng, tinh thần yêu nước chống Pháp vẫn sục sôi trong nhân
dân cả nước.;
				do mâu thuẫn của phe chủ chiến trong triều đình Huế đại diện là Tôn Thất Thuyết với thực dân Pháp;
				dựa vào phong trào kháng chiến của nhân dân, phái chủ chiến ra sức chuẩn bị và tổ chức phản công thực dân
Pháp|
			<!--Câu 19-->Bài học cách mạng Tháng Tám năm 1945 có thể vận dụng cho công cuộc xây dựng và phát triển đất
nước hiện nay là;
				 [kết hợp sức mạnh dân tộc với sức mạnh thời đại;
				 kiên trì sự lãnh đạo của Đảng;
				đoàn kết toàn dân trong mặt trận dân tộc thống nhất;
				độc lập dân tộc gắn liền với chủ nghĩa xã hội.|
			<!--Câu 20-->Điểm khác biệt của giai cấp công nhân Việt Nam so với giai cấp công nhân ở các nước tư bản Âu
-Mỹ đó là;
				[ra đời trước giai cấp tư sản Việt Nam.;
				ra đời sau giai cấp tiểu tư sản Việt Nam.;
				ra đời cùng giai cấp tư sản Việt Nam.;
				ra đời sau giai cấp tư sản Việt Nam.|
			<!--Câu 21--><i>“Tổ chức và lãnh đạo quần chúng đoàn kết, tranh đấu để đánh đổ đế quốc chủ nghĩa Pháp và tay sai
để tự cứu lấy mình”</i> là mục tiêu hoạt động của tổ chức nào?;
				[Hội Việt Nam Cách mạng Thanh niên.;
				 Hội Hưng Nam.;
				Việt Nam Quốc dân Đảng;
				Hội Phục Việt.|
			<!--Câu 22--><i>“Đảng ra đời chứng tỏ giai cấp vô sản ta đã trưởng thành và đủ sức lãnh đạo cách mạng”</i>(Nguyễn Ái
Quốc). Câu nói trên thể hiện điều gì?;
				[Đảng ra đời đánh dấu giai cấp công nhân đã trở thành một giai cấp độc lập;
				Đảng ra đời chứng tỏ phong trào công nhân đã có sự chuyển biến về chất;
				Đảng cộng sản là chính đảng của giai cấp công nhân đã giành quyền lãnh đạo cách mạng;
				Không có sự ra đời của Đảng thì không có sự lãnh đạo của giai cấp công nhân|
			<!--Câu 23-->Theo quy định của Hội nghị Ianta (2-1945), quốc gia nào dưới đây cần trở thành một quốc gia thống
nhất và dân chủ?;
				Triều Tiên.;
				 Mông Cổ.;
				[Trung Quốc;
				Nhật Bản.|
			<!--Câu 24-->Sự kiện nào đánh dấu chiến sự chấm dứt ở châu Âu trong chiến tranh thế giới thứ hai (1939- 1945)?;
				Các nước Đông Âu được giải phóng;
				Hồng quân Liên Xô cắm cờ trên nóc tòa nhà Quốc hội Đức.;
				[Đức kí hiệp ước đầu hàng không điều kiện;
				Nhật Bản đầu hàng không điều kiện|
			<!--Câu 25-->Ý nào sau đây không thuộc nội dung của Chính sách kinh tế mới (NEP) ở Nga năm 1921;
				Nhà nước nắm các ngành kinh tế chủ chốt;
				[Cho phép tư nhân thuê hoặc xây dựng các xí nghiệp không quá 50 công nhân;
				Khuyến khích tư bản nước ngoài đầu tư kinh doanh ở Nga;
				Cho phép thương nhân tự do buôn bán, trao đổi hàng hóa|
			<!--Câu 26-->Hình thái khởi nghĩa vũ trang trong cách mạng tháng Mười Nga năm 1917 là;
				đồng thời tiến hành khởi nghĩa ở thành thị và nông thôn;
				 [bắt đầu từ thành thị, lấy thành thị làm trung tâm;
				bắt đầu từ nông thôn, lấy nông thôn bao vây thành thị;
				nổi dậy của quần chúng là chủ yếu|
			<!--Câu 27-->Để khắc phục tình trạng khó khăn về tài chính sau Cách mạng tháng Tám năm 1945, Chính phủ nước
Việt Nam Dân chủ Cộng hòa kêu gọi;
				nhân dân thực hiện phong trào tăng gia sản xuất;
				[tinh thần tự nguyện đóng góp của nhân dân.;
				nhân dân cả nước thực hiện “Ngày đồng tâm”.;
				cải cách ruộng đất và thực hành tiết kiệm.|
			<!--Câu 28-->Cụm từ nào dưới đây phản ánh đầy đủ tính chất của Chiến tranh thế giới thứ nhất (1914- 1918)?;
				Chiến tranh phi nghĩa;
				Chiến tranh đế quốc;
				Chiến tranh chính nghĩa;
				[Chiến tranh đế quốc phi nghĩa|
			<!--Câu 29-->Điểm giống nhau cơ bản giữa <i>“Cương lĩnh chính trị”</i> đầu tiên (đầu năm 1930) với <i>“Luận cương chính trị”</i> (10/1930) là đều;
				xác định đúng đắn mâu thuẫn trong xã hội Đông Dương.;
				xác định đúng đắn khả năng của tiểu tư sản đối với cách mạng.;
				[xác định đúng đắn giai cấp lãnh đạo.;
				xác định đúng đắn khả năng tham gia cách mạng của các giai cấp.|
			<!--Câu 30-->Đâu là nguyên nhân trực tiếp của sự kiện Nhật Bản đảo chính Pháp ngày 09/3/1945 ở Đông Dương?;
				Nhật Bản tiến hành theo kế hoạch chung của phe phát xít.;
				[mâu thuẫn Pháp – Nhật Bản càng lúc càng gay gắt;
				thất bại gần kề của Nhật Bản trong Chiến tranh thế giới thứ hai.;
				 phong trào cách mạng dâng cao gây cho Nhật Bản nhiều khó khăn.|
			<!--Câu 31-->Lực lượng xã hội nào là điều kiện bên trong cho cuộc vận động giải phóng dân tộc theo khuynh hướng
dân chủ tư sản đầu thế kỉ XX?;
				Giai cấp công nhân;
				Tư sản dân tộc;
				[Sĩ phu yêu nước tư sản hóa;
				Tầng lớp tiểu tư sản|
			<!--Câu 32-->Tư tưởng chủ đạo của Chủ tịch Hồ Chí Minh trong việc giải quyết mối quan hệ Việt - Pháp (từ 6-3-
1946 đến trước 19-12-1946) là;
				sẵn sàng đánh Pháp khi cần thiết;
				nhân nhượng về kinh tế, độc lập về chính trị.;
				nhân nhượng cho Pháp về kinh tế và chính trị;
				[sử dụng phương pháp hòa bình.|
			<!--Câu 33-->Thực dân Pháp bắt đầu tiến hành công cuộc khai thác thuộc địa lần thứ nhất ở Việt Nam sau khi;
				đã hoàn thiện bộ máy thống trị ở Đông Dương.;
				đã dập tắt được cuộc khởi nghĩa Yên Thế.;
				[đã cơ bản bình định được Việt Nam về quân sự;
				đã đặt nền bảo hộ lên toàn bộ nước ta.|
			<!--Câu 34-->Trong cuộc kháng chiến chống thực dân Pháp (1945 - 1954), thắng lợi nào của quân dân Việt Nam
đã bước đầu làm phá sản kế hoạch Nava?;
				Chiến dịch Việt Bắc thu - đông năm 1947.;
				[Cuộc tiến công chiến lược Đông Xuân 1953- 1954;
				Chiến dịch Biên giới thu - đông năm 1950.;
				 Chiến dịch Điện Biên Phủ năm 1954.|
			<!--Câu 35-->So với Hiệp hội các quốc gia Đông Nam Á (ASEAN), sự phát triển của Liên minh châu Âu (EU) có
điểm khác biệt gì?;
				Hạn chế sự can thiệp và chi phối của các cường quốc.;
				Quá trình hợp tác, mở rộng thành viên diễn ra khá lâu dài.;
				[Diễn ra quá trình nhất thể hóa trong khuôn khổ khu vực.;
				Hợp tác, giúp đỡ các nước trong khu vực phát triển kinh tế.|
			<!--Câu 36-->Sự chuyển biến về kinh tế và sự chuyển biến về xã hội ở Việt Nam đầu thế kỉ XX có mối quan hệ như
thế nào?;
				Chuyển biến về kinh tế dẫn tới những tác động xấu về mặt xã hội.;
				Chuyển biến về kinh tế kéo theo những chuyển biến xã hội tích cực.;
				Chuyển biến về xã hội kéo theo sự biến đổi về mặt kinh tế.;
				[Chuyển biến về kinh tế kéo theo sự biến đổi về mặt xã hội.|
			<!--Câu 37--><i>“Hành lang Đông- Tây”</i> được Pháp thiết lập trong kế hoạch Rơve (13/5/1949)gồm;
				[Hải Phòng, Hà Nội, Hoà Bình, Sơn La;
				Hải Phòng, Hà Nội, Tuyên Quang, Lai Châu;
				Nam Định, Hà Nội, Hòa Bình, Lạng Sơn;
				Nam Định, Hà Nội, Lạng Sơn, Tuyên Quang|
			<!--Câu 38-->Đặc điểm nổi bật của quan hệ quốc tế từ sau Chiến tranh thế giới thứ hai đến đầu những năm 70 của thế
kỷ XX là gì?;
				Hai siêu cường Xô - Mỹ đối thoại, hợp tác.;
				Hòa bình, hợp tác trở thành xu thế chủ đạo.;
				Hợp tác chính trị - văn hóa là xu thế chủ đạo.;
				[Hai siêu cường Xô - Mỹ đối đầu gay gắt.|
			<!--Câu 39-->Sau khi kế hoạch <i>“đánh nhanh thắng nhanh”</i> thất bại ở Gia Định, thực dân Pháp chuyển sang kế hoạch;
				Đánh chiếm Bắc Kì.;
				Đánh chiếm các tỉnh miền Đông Nam Kì.;
				Đánh lâu dài;
				[“Chinh phục từng gói nhỏ”|
			<!--Câu 40-->Ngày 14/4/2018, Mĩ và đồng minh bắn hơn 100 quả tên lửa vào Siri với lí do quân đội của chính phủ
Siri sử dụng vũ khí hóa học <br> ở Đuma mặc dù chưa có bằng chứng xác thực. Hành động trên đây của Mĩ và đồng
minh Mĩ chứng tỏ</br>;
				[Sự thi hành chính sách áp đảo và cường quyền của Mĩ;
				Mĩ có trách nhiệm bảo vệ hòa bình thế giới;
				Mĩ thể hiện trách nhiệm chống sử dụng vũ khí hóa học;
				Chính sách “cây gậy và củ cà rốt” của Mĩ.	
			</div>
			<div id="countDown">	
				<p>Thời gian còn lại:</p>
				<p class="minute">Phút</p>
				<p class="second">Giây<p>
				
			<div>
			<script type="text/javascript">
				function getCookie(name){
					var cname = name + "=";
					var dc = document.cookie;
					if (dc.length > 0) {
						begin = dc.indexOf(cname);
						if (begin != -1) {
							begin += cname.length;
							end = dc.indexOf(";", begin);
						if (end == -1) end = dc.length;
						return unescape(dc.substring(begin, end));
						}
					}
					return null;
				}
				if(document.cookie && document.cookie.match('myClock_Su1')){
				  // get deadline value from cookie
				  var deadline = getCookie('myClock_Su1');
				}
				 
				
				else{
				  // create deadline 50 minutes from now
					var currentTime = Date.parse(new Date());
					var deadline = Date.parse(new Date(currentTime + 50*60*1000));
				 
				  // store deadline in cookie for future reference
				  document.cookie = 'myClock_Su1=' + deadline + '; path=/';
				}
							
			
			
				
				var currentTime = Date.parse(new Date());
				
				var t=deadline-currentTime;
				var second = Math.floor((t / 1000) % 60);
				var minute = Math.floor(t / 1000 / 60);
				
				
				
			
				var nd = $("#noi_dung_de").html();
				var cau_hoi = nd.split('|');
				var html = ""; var tra_loi_dung = 0;
				for(var i = 0;i<cau_hoi.length;i++)
				{
					var chi_tiet = cau_hoi[i].split(';');
					//alert(chi_tiet[i]);
					for(var j=0;j<chi_tiet.length;j++)
					{
			 
						if (j==0) html += "<tr><td><b>Câu " + (i+1) + ":</b></td><td> <b>" + chi_tiet[j].trim() + "</b></td></tr>";
						else 
						{
							html += "<tr><td></td><td id='" + (i + 1 + String.fromCharCode(64 + j))+"'><input type='radio' name='" + (i + 1 )+ "' id='" + (i + 1 + String.fromCharCode(64 + j))+"' value='" + chi_tiet[j].trim() +"'> " + String.fromCharCode(64 + j) + ". " + chi_tiet[j].replace("~","").trim() + "</td></tr>";
						}
					}
				}

				$("#noi_dung_de").empty().append("<table>" + html + "</table><input type='submit' value='Nộp bài' id='tra_loi_xong' style='margin-left:585px;'>").fadeIn();
				$("#noi_dung_de input").click(function(){   
				//Lấy id của radio
				var id = $(this).attr("id");
				for (var j=1;j<5;j++){
					var newid=id.substr(0,1)+String.fromCharCode(64 + j);
					
					$("td#" + newid).css("background-color","");
				}
				$("td#" + id).css("background-color","yellow");
				
				});   
				
2				
				
3				
4				
			
				
				/*jQuery(document).ready(function($) {
					
					setTimeout(function(){
						
						$('#tra_loi_xong').trigger( "click" ) ;
						}, time);
				}); */
				
				
				
			
			
				$("#tra_loi_xong").click(function(){
					$('#noi_dung_de input').each(function () {
					var id = $(this).attr("id");
					var ctl = $(this).val();
				 
					//Hiển thị câu đúng với nền là màu đỏ
					if (ctl[0] == '[') 
					{
						//alert($("td#" + id).css("background-color"));
						if ($("td#" + id).css("background-color") == "rgb(255, 255, 0)")
						{
							tra_loi_dung++;
						}
						else $("td#" + id).css("background-color","red");     
					}
				});
				$("#noi_dung_de").append("<p style='text-align:center'>Số câu đúng :" + (tra_loi_dung < 0 ? "0" : tra_loi_dung) + " câu. Điểm : " + tra_loi_dung*0.25+ "</p>");
				$("#tra_loi_xong").fadeOut();
				
				});
				
				var timer = setInterval(function() {
					
				   $('.minute').text(minute +" Phút")
				   $('.second').text(second-- +" Giây");
				   if (second == -1) {
						minute--;
						second=60;
				   }
				   if (minute ==-1)  {
						 alert("Đã hết giờ làm bài, kiểm tra kết quả");
					  $('#tra_loi_xong').trigger( "click" ) ;
					  $('.second').fadeOut("slow");
					  $('.minute').fadeOut("slow");
					  clearInterval(timer);
				   }
				   
				}, 1000);
				
			</script>
			
		</div>
	</body>
</html>