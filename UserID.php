<?php
session_start();
include_once 'users.php';
class UserID
{
    public $username;
    public $password;
    public $name;
    protected $id;

    public function __construct($username = "", $password = "")
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function encryptPassword()
    {
        return md5($this->password);
    }

    public function login()
    {
        $db = new users();
        $query = $db->buildQueryParams([
            "where" => "username =:username AND password =:password",
            "params" => [
                ":username" => trim($this->username),
                ":password" => $this->encryptPassword()
            ]
        ])->selectOne();
        if ($query) {
            $_SESSION["userId"] = $query["ID"];
            $_SESSION["username"] = $query["username"];
            $_SESSION["firstname"]=$query["firstname"];
            $_SESSION["name"]=$query["name"];
            return true;
        }
        return false;
    }
    public function isUsername()
    {
        $db = new users();
        $query = $db->buildQueryParams([
            "where" => "username =:username",
            "params" => [
                ":username" => trim($this->username),
            ]
        ])->selectOne();
        if ($query) {
            return true;
        }
        return false;
    }
    public function isName()
    {
        $db = new users();
        $query = $db->buildQueryParams([
            "where" => "name =:name",
            "params" => [
                ":name" => trim($this->name),
            ]
        ])->selectOne();
        if ($query) {
            return true;
        }
        return false;
    }
    public function logout()
    {
        unset($_SESSION["userId"]);
        unset($_SESSION["username"]);
    }

    public function getSESSION($name)
    {
        if ($name !== null) {
            return isset($_SESSION[$name]) ? $_SESSION[$name] : NULL;
        }
        return $_SESSION;
    }

    public function isLogin()
    {
        if ($this->getSESSION("userId")) {return true;}
        return false;
    }

    public function getID()
    {
        return $this->getSESSION("userId");
    }
}